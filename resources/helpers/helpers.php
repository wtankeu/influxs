<?php

if(!function_exists('get_client_ip'))
{
    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}

if(!function_exists('get_ip_detail'))
{
    function get_ip_detail($ip)
    {
        $ch =curl_init();
        curl_setopt($ch, CURLOPT_URL,'http://www.ip-api.com/json/'.$ip.'?fields=258047&lang=fr' );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        $localisationInfoJson = curl_exec($ch);
        curl_close($ch);
        $localisationInfoPhp = json_decode($localisationInfoJson, TRUE);
        if(!empty($localisationInfoPhp['status']) && $localisationInfoPhp['status']=='success')
        {
            $data['ville']= $localisationInfoPhp['country'].'/'.$localisationInfoPhp['city'];
            $data['localistion']=$localisationInfoJson;
            $data['FAI']=$localisationInfoPhp['org'];
            $data['lat']=$localisationInfoPhp['lat'];
            $data['long']=$localisationInfoPhp['lon'];
            return $data;
        }else return false;
    }
}
