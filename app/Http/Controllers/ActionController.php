<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class ActionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        if (!auth()->check())
         return response()->json(array('status'=>false,'message'=>'Unauthorized  users','error' => 'Unauthorized'), 401) ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    // Liste des action d'un contrat
    public function lastActions($contract)
    {
        DB::enableQueryLog();

        $data=Action::where('contract_id','=',$contract)
        ->where('actions.status','=',1)
        ->select('actions.id','user_id','campaigns.name AS campaign','platforms.name AS platform','actions_types.name AS type','text','link','actions.status','tags','actions.created_at AS date_j','actions.contract_id','products.name AS product',
               // DB::raw('IF(products.images IS NULL,IF(campaigns.images IS NULL,NULL,CONCAT("https://infl.fieldlive.pro/BO/_lib/file/img/",campaigns.images)), CONCAT("https://infl.fieldlive.pro/BO/_lib/file/img/",products.images)) AS image'),
                DB::raw('CONCAT("https://bo.influxs.live/BO/_lib/file/img/",actions_types.icon) AS image')
                )
        ->join('campaigns','campaign_id','=','campaigns.id')
        ->join('platforms','platform_id','=','platforms.id')
        ->join('actions_types','reaction_type_id','=','actions_types.id')
        ->join('products','product_id','=','products.id')
        ->orderBy('actions.id','DESC')->get();
        //->paginate(5);
        return response()->json($data);
    }

    // Detail d'une actions
    public function actionDetail($action)
    {


        $dataAction=Action::where('actions.id','=',$action)
        ->where('actions.status','=',1)
        ->select('actions.id','user_id','campaigns.name AS campaign','platforms.name AS platform','actions_types.name AS type','text','link','actions.status','tags','actions.publication_date AS date_j','actions.contract_id','products.name AS product')
        ->join('campaigns','campaign_id','=','campaigns.id')
        ->join('platforms','platform_id','=','platforms.id')
        ->join('actions_types','reaction_type_id','=','actions_types.id')
        ->join('products','product_id','=','products.id')
        ->orderBy('actions.id','DESC')->first();

        return response()->json(array("action_detail"=>$dataAction,
                                     "action_kpis"=>$this->getActionKPI($action),
                                     "action_files"=>$this->getActionFiles($action),
                                    ));
    }

    // Liste des fichiers d'une action
    private function getActionFiles($action)
    {
        $data=DB::table('actions_files')->where(['action_id'=>$action,'status'=>1])->select('type', 'link AS lien', 'created_at', DB::raw('CONCAT("https://bo.influxs.live/BO/_lib/file/img/",link) AS link'))->get();
        return $data;
    }


    //Liste des KPIs d'une action
    private function getActionKPI($action)
    {
        $data=DB::table('vue_max_actions_details')
        ->where(['action_id'=>$action])
        ->select('kpi_id', 'date_j', 'value','code','label', DB::raw('CONCAT("https://bo.influxs.live/BO/_lib/file/img/",kpis.logo) AS logo'))
        ->join('kpis','kpi_id','=','kpis.id')
        ->get();
        return $data;
    }

    // repartition des action par Plate formes
    public function actionsPlateForms($contract)
    {
        $data=DB::table('vue_max_actions_details')->where('contract_id','=',$contract)
        ->where('action_status','=',1)
        ->select('platform_id', 'platforms.name', DB::raw('SUM(vue_max_actions_details.value) as value'))
        ->join('platforms','platform_id','=','platforms.id')
        ->groupBy('platform_id','platforms.name')
        ->get();
        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    // Sauvegarde des actions
    public function actionSave(Request $request)
    {
        $user=auth()->user();
        $rules = [
                    'campaign' => 'required|integer',
                    'platform' => 'required|integer',
                    'product' => 'required|integer',
                    'contract' => 'required|integer',
                    'action_type' => 'required|integer',
                    'date_pub' => 'required',
                    'text' => 'required|string',
                    'link' => 'required|string'
        ];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        else
        {

            try{
                $action = new Action;
                $action->user_id =$user->id;
                $action->campaign_id =  $request->campaign;
                $action->platform_id = $request->platform;
                $action->reaction_type_id = $request->action_type;
                $action->text =  $request->text;
                $action->link =  $request->link;
                $action->contract_id =  $request->contract;
                $action->product_id =  $request->product;
                $action->publication_date = date('Y-m-d', strtotime($request->date_pub));
                $action->status =  1;
                $action->tags = $request->tags;
                $action->save();
                return response()->json(array('status'=>true,'message'=>'Action registered successfully','error' => ''), 201) ;
            }
            catch(Exception $e){

                return response()->json(array('status'=>false,'message'=>'Error When registered ','error' => $e.getMessage()), 422) ;
            }
        }

    }

    public function activiteUpdate(Request $request)
    {
        $user=auth()->user();
        $rules =[

            'idAction'=>'required|integer'];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails())
        {
            return response()->json($validator->errors(), 422);
        }

        $dataUpdate=array();

        if(isset($request->campaign) and !empty($request->campaign))
            $dataUpdate['campaign_id']=$request->campaign;

        if(isset($request->platform) and !empty($request->platform))
            $dataUpdate['platform_id']=$request->platform;

        if(isset($request->type) and !empty($request->type))
            $dataUpdate['reaction_type_id']=$request->type;

        if(isset($request->text) and !empty($request->text))
            $dataUpdate['text']=$request->text;

        if(isset($request->link) and !empty($request->link))
            $dataUpdate['link']=$request->link;

        if(isset($request->product) and !empty($request->product))
            $dataUpdate['product_id']=$request->product;

        if(isset($request->date_j) and !empty($request->date_j))
            $dataUpdate['publication_date']=date('Y-m-d',strtotime($request->date_j));

        $dataUpdate['updated_by']=$user->id;
        try{
            Action::where('id', $request->idAction)->update($dataUpdate);
            return response()->json(array('status'=>true,'message'=>'Action Update ','error' => ''), 201);
        }
        catch(Exception $e){

            return response()->json(array('status'=>false,'message'=>'Error Action Update ','error' => $e.getMessage()), 422) ;
        }

    }


    // Sauvegarde des KPIs sur les actions
    public function actionKpiSave(Request $request)
    {
        $user=auth()->user();
        $rules = [
            'action' => 'required|integer',
            'kpi' => 'required|integer',
            'value' => 'required|integer'
        ];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        else
        {

            try{
                $actionDetail = ['action_id'=>$request->action,
                'kpi_id'=>$request->kpi,
                'value'=>$request->value,
                'status'=>1,
                'date_j'=>date('Y-m-d'),
                'user_id'=>$user->id ];
                DB::table('actions_details')->insert($actionDetail);
                return response()->json(array('status'=>true,'message'=>'Action KPI registered successfully','error' => ''), 201);
            }
            catch(Exception $e){

                return response()->json(array('status'=>false,'message'=>'Error When registered ','error' => $e.getMessage()), 422) ;
            }
        }
    }



    public function actionFileSave(Request $request){

        $tabExtenion=array('csv'=>'Document','txt'=>'Document','pdf'=>'Document','doc'=>'Document', 'docx'=>'Document',
                            'mp4'=>'Video','flv'=>'Video','3gp'=>'Video','avi'=>'Video',
                            'jpg'=>'Images','png'=>'Images','jpeg'=>'Images','gif'=>'Images','svg'=>'Images');

        $rules =[

            'action'=>'required|integer'];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails())
        {
            return response()->json($validator->errors(), 422);
        }
        else
        {

            try
            {
                $file= $request->file('file');
               // $fileName = $file->getClientOriginalName();
                $path = $file->store('');
                $extension=$file->getClientOriginalExtension();
                $tabPath=explode('/',$path);
                $insertData=array('action_id'=>$request->action, 'type'=> $tabExtenion[$extension],'status'=>1,'link'=>end($tabPath));

                DB::table('actions_files')->insert($insertData);
                return response()->json(array('status'=>true,'message'=>'Fichier registered successfully','error' => ''), 201);
            }
            catch(Exception $e)
            {
                return response()->json(array('status'=>false,'message'=>'Error When registered ','error' => $e.getMessage()), 422) ;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function show(Action $action)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Action $action)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function destroy(Action $action)
    {
        //
    }
}
