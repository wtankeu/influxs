<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ParamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        if (!auth()->check())
         return response()->json(array('status'=>false,'message'=>'Unauthorized  users','error' => 'Unauthorized'), 401) ;
    }

    public function getParam($contract)
    {
        $data= DB::table('contracts')->where('id','=',$contract)->select('company_id')->first();
        $params=array(
            'plateforms'=>$this->getPlaleforms(),
            'products'=>$this->getProducts($data->company_id),
            'campagns'=>$this->getCampagns($data->company_id),
            'types'=>$this->getactionsTypes(),
            'kpis'=>$this->getKpis($data->company_id)
        );
        return response()->json($params,200);
    }

    protected function getPlaleforms()
    {
       $data= DB::table('platforms')->where('status','=',1)->select('*')->get();
       return  $data;
    }

    protected function getProducts($company)
    {
        $data= DB::table('products')
        ->where('status','=',1)
        ->where('company_id','=',$company)
        ->select('id', 'name')
        ->orderBy('name','ASC')
        ->get();
        return  $data;
    }
    protected function getCampagns($company)
    {
        $data= DB::table('campaigns')
        ->where('status','=',1)
        ->where('start_date','<=',date('Y-m-d'))
        ->where('end_date','>=',date('Y-m-d'))
        ->where('company_id','=',$company)
        ->select('id', 'name')
         ->orderBy('name','ASC')
         ->get();
        return  $data;
    }

    protected function getactionsTypes()
    {
        $data= DB::table('actions_types')
        ->where('status','=',1)
        ->orderBy('name','ASC')
        ->select('id', 'name')->get();
        return  $data;
    }
    protected function getKpis($company)
    {
        $data= DB::table('kpis')
        ->where('status','=',1)
        ->where('companies_id','=',$company)
        ->select('id','code','label')->get();
        return  $data;
    }

    public function set_log($module, $status, $user='',$login='')
    {
        $ip=get_client_ip();
        $dataIp=get_ip_detail($ip);
        $data=array('id_user'=>$user,
        'login'=>$login,
        'module'=>$module,
        'ip'=>$ip,
        'status'=>$status,
        'FAI'=>$dataIp['FAI'],
        'ville'=>$dataIp['ville'],
        'localistion_info' => $dataIp['localistion'],
        '_lat' =>$dataIp['lat'],
        '_long'=>$dataIp['long']);
        DB::table('global_logs')->insert($data);
    }

}
