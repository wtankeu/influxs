<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ParamController;
class UserAuthController extends Controller
{

    /**
     * Create an instace of UserAuthController
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'versionning', 'refresh']]);
    }

    /**
     * Register a User
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:2',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        return response()->json([
            'message' => 'User registered successfully',
            'user' => $user
        ], 201);
    }

    /**
     * Get a JWT token after successful login
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $param=new ParamController();
        $validator = Validator::make($request->all(), [
            'login' => 'required|string',
            'password' => 'required|string|min:2',
        ]);

        if ($validator->fails()) {
            $param->set_log('Login', $validator->errors(),0, $request->login);
            return response()->json($validator->errors(), 422);
        }

        if (!$token = JWTAuth::attempt($validator->validated(), true)) {
            $param->set_log('Login', $validator->errors(),0, $request->login);
            return response()->json(['status' => 'failed', 'message' => 'Invalid email and password.', 'error' => 'Unauthorized'], 401);
        }

        $param->set_log('Login', 'Success', $user=auth()->user()->id, $request->login);
        return $this->createNewToken($token);
    }

    public function versionning()
    {
        //DB::enableQueryLog();
        $data = DB::table('versionning')->select('code', 'label', 'actif', 'link')->get();
        //print_r(DB::getQueryLog());
        return response()->json($data);
    }
    /**
     * Get the token array structure.
     * @param  string $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    public function set_notification_token(Request $request)
    {

        try
        {
            DB::table('user_notification_tokens')->updateOrInsert(
                ['user_id' =>$request->userID, 'token' =>$request->fbToken ]
            );
            return response()->json(array('status'=>true,'message'=>'Token Firebese enregistré !!','error' => ''), 201);
        }
        catch(Exception $e){

            return response()->json(array('status'=>false,'message'=>'Erreur d\'enregistrement du token Firebase ','error' => $e.getMessage()), 422) ;
        }
    }

    /**
     * Refresh a JWT token
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {

        $token = JWTAuth::getToken();
        if (!$token) {
            throw new BadRequestHtttpException('Token not provided');
        }
        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            throw new AccessDeniedHttpException('The token is invalid');
        }
        return response()->json(['token' => $token])->header('Cache-Control', 'no-cache, no-store, must-revalidate');

        //return $this->createNewToken(auth()->refresh());
    }


    /**
     * Get the Auth user using token.
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        return response()->json(auth()->user());
    }


    /**
     * Logout user (Invalidate the token).
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['status' => 'success', 'message' => 'User logged out successfully']);
    }
}
