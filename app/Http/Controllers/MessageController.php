<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        if (!auth()->check())
         return response()->json(array('status'=>false,'message'=>'Unauthorized  users','error' => 'Unauthorized'), 401) ;
    }

    public function get_msgNonLu()
    {
        $user=auth()->user();
        $data= DB::table('message_user')->select(DB::raw('COUNT(id) nbr_msg'))
        ->whereNull('read_at')
        ->where('user_id','=',$user->id)
        ->first();
        return response()->json($data);
    }

    public function get_msgsList()
    {
        $user=auth()->user();
        $data= DB::table('message_user')->select('id','title', 'message', 'read_at', 'created_at')
        ->where('user_id','=',$user->id)
        ->get();
        return response()->json($data);
    }
    public function get_msg($id)
    {
        $data= DB::table('message_user')->select('title', 'message', 'read_at', 'message_user.created_at','influencers.name AS influenseur_name','messages.image')
        ->join('influencers','influencers.id','influenceur_id')
        ->join('messages','messages.id','message_id')
        ->where('message_user.id','=',$id)
        ->first();
        if(isset($data->read_at) and $data->read_at==null)
            DB::table('message_user')->where('id', $id)->update(['read_at'=>date('Y-m-d H:m:i')]);
        return response()->json($data);
    }

}
