<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Contract;
use Illuminate\Http\Request;
use App\Models\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        if (!auth()->check())
         return response()->json(array('status'=>false,'message'=>'Unauthorized  users','error' => 'Unauthorized'), 401) ;
    }

    // Liste des contrats d'un utilisateur
    public function index()
    {

        $user=auth()->user();
        $profil=$this->get_profile($user->login);

        if(isset($profil->code) and $profil->code=='ADMIN')
        {
            DB::enableQueryLog();
            $contractList=Contract::select('contracts.*','companies.name', DB::raw('CONCAT("https://infl.fieldlive.pro/BO/_lib/file/img/",companies.logo) AS logo'))
            //->join('influencer_managers','contracts.influenceur_id','=','influencer_managers.influenceur_id' )
            ->join('influencers','contracts.influenceur_id','=','influencers.id')
            ->join('companies','influencers.company_id','=','companies.id')
            ->get();
            //$queries = DB::getQueryLog();

        }else if(isset($profil->code) and $profil->code=='COMPANYAD')
        {
            DB::enableQueryLog();
            $contractList=Contract::select('contracts.*','companies.name','companies.logo' )
            ->where('contracts.company_id','=',$user->company_id)
            ->join('influencers','contracts.influenceur_id','=','influencers.id')
            ->join('companies','influencers.company_id','=','companies.id')
            ->get();
        }else
        {
            $contractList=Contract::where(['user_id'=>$user->id])
            ->join('influencer_managers','contracts.influenceur_id','=','influencer_managers.influenceur_id' )
            ->join('influencers','contracts.influenceur_id','=','influencers.id')
            ->join('companies','influencers.company_id','=','companies.id')
            ->select('contracts.*','companies.name','companies.logo' )
            ->get();
        }

        //
        return response()->json( $contractList);
    }


    private function get_profile($login)
    {
        $data=DB::table('sec_users_groups')
        ->select('login', 'description', 'code')
        ->join('sec_groups', 'sec_groups.group_id','=','sec_users_groups.group_id')
        ->where('login','=',$login)
        ->first();
        return  $data;
    }
    // Informations sur un contrat
    public function contractInfo($contract)
    {
        $data=Contract::where('contracts.id','=',$contract)
        ->join('influencers','contracts.influenceur_id','=','influencers.id')
        ->join('companies','contracts.company_id','=','companies.id')
        ->join('categories','influencers.category_id','=','categories.id')
        ->select('contracts.*','companies.name AS company_name',
        DB::raw('CONCAT("https://infl.fieldlive.pro/BO/_lib/file/img/",companies.logo) AS logo'),
        'companies.email AS company_email','companies.phone AS company_phone','influencers.name AS name_influencer', 'influencers.full_name', 'influencers.phone AS influencer_phone',
        DB::raw('IF(influencers.images IS NULL, NULL, CONCAT("https://infl.fieldlive.pro/BO/_lib/file/img/",influencers.images)) AS images'),
        'categories.name AS category_label')
        ->first();
        //
        return response()->json($data);
    }

    // Liste des objectifs  (KPIs) à atteindre à la signature d'un contrat
    public function contractKPI($contract)
    {
        $tab=array();
        $contractKPI=Contract::where(['contracts.id'=>$contract, 'contract_kpis.status'=>1, 'kpis.status'=>1])
        ->join('contract_kpis','contracts.id','=','contract_kpis.contract_id' )
        ->join('kpis','contract_kpis.kpi_id','=','kpis.id')
        ->select('contracts.label AS contract_label','kpis.label AS kpi_label','kpis.code','kpis.short_label', 'values as value' )
        ->get();

        foreach($contractKPI as $val)
        {
            $kpi=$val['code'];
            $tab[$kpi]=$val['value'];
        }
        return $tab;



    }


    // Liste des objectifs (KPIs) atteints par une actions
    public function contractActionKPI($contract)
    {
        $tab=array();
        $tabval=array();
        $tabcontratKpi=$this->contractKPI($contract);

        $contractKPI=Contract::where(['contracts.id'=>$contract, 'kpis.status'=>1])
        ->join('vue_max_actions_details','contracts.id','=','vue_max_actions_details.contract_id' )
        ->join('kpis','vue_max_actions_details.kpi_id','=','kpis.id')
        ->select('contracts.label AS contract_label','kpis.label AS kpi_label','kpis.code','kpis.short_label','default_val', DB::raw('SUM(vue_max_actions_details.value) as value'))
        ->groupBy('kpis.code','kpis.label', 'contracts.label','kpis.short_label','default_val')->get();
        //->sum('vue_max_actions_details.value');
        foreach($contractKPI as $val)
        {
            $kpi=$val['code'];
            $tabval=array(
                'contract_label'=> $val['contract_label'],
                'kpi_label'=> $val['kpi_label'],
                'short_label'=> $val['short_label'],
                'code'=> $val['code'],
                'kpi_actions'=> $val['value'],
                'contract_kpi'=>$val['default_val']
            );

            if(isset($tabcontratKpi[$kpi]))
                $tabval['contract_kpi']=$tabcontratKpi[$kpi];

            $tab[]= $tabval;
        }

        return response()->json($tab);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        //
    }
}
