<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserAuthController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\ActionController;
use App\Http\Controllers\ParamController;
use App\Http\Controllers\MessageController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api'], function ($router) {
    Route::post('register', [UserAuthController::class, 'register']);
    Route::post('login', [UserAuthController::class, 'login']);
    Route::get('user', [UserAuthController::class, 'user']);
    Route::post('refresh', [UserAuthController::class, 'refresh']);
    Route::post('logout', [UserAuthController::class, 'logout']);
    Route::get('contract_list', [ContractController::class, 'index']);
    Route::get('contract_kpi/{contract}', [ContractController::class, 'contractActionKPI']);
    Route::get('contract_actionKPI/{contract}', [ContractController::class, 'contractActionKPI']);
    Route::get('contract_detail/{contract}', [ContractController::class, 'contractInfo']);
    Route::get('last_actions/{contract}', [ActionController::class, 'lastActions']);
    Route::get('action_detail/{action}', [ActionController::class, 'actionDetail']);
    Route::get('actionsby_plateforms/{contract}', [ActionController::class, 'actionsPlateForms']);
    Route::post('new_action', [ActionController::class, 'actionSave']);
    Route::put('action', [ActionController::class, 'activiteUpdate']);
    Route::get('actions_params/{contract}', [ParamController::class, 'getParam']);
    Route::post('new_actionkpi', [ActionController::class, 'actionKpiSave']);
    Route::post('new_actionfile', [ActionController::class, 'actionFileSave']);
    Route::get('version', [UserAuthController::class, 'versionning']);
    Route::post('notification_token', [UserAuthController::class, 'set_notification_token']);
    Route::get('message/{id}', [MessageController::class, 'get_msg']);
    Route::get('messages_list', [MessageController::class, 'get_msgsList']);
    Route::get('message_non_lu', [MessageController::class, 'get_msgNonLu']);


});
