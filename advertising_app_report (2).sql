-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 23 juil. 2021 à 10:13
-- Version du serveur :  5.7.24
-- Version de PHP : 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `advertising_app_report`
--

-- --------------------------------------------------------

--
-- Structure de la table `actions`
--

CREATE TABLE `actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED DEFAULT NULL,
  `platform_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'La plateforme de réaction (facebook, twitter, instagram, ect...)',
  `reaction_type_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Le type de la réaction (commentaire, photo, like, vidéo, etc..)',
  `text` longtext,
  `link` varchar(255) DEFAULT NULL,
  `publication_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `tags` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `contract_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `actions`
--

INSERT INTO `actions` (`id`, `user_id`, `campaign_id`, `platform_id`, `reaction_type_id`, `text`, `link`, `publication_date`, `status`, `tags`, `created_at`, `updated_at`, `contract_id`, `product_id`) VALUES
(1, 1, 1, 3, 3, 'Je viens de publier sur Twitter un texte subtile sur Imbattable ', 'https://twitter.com/', NULL, 1, 'Test', '2021-06-30 09:11:43', '2021-06-30 09:11:43', 1, 2),
(2, 1, 1, 1, 1, 'réalisation d\'un commentaire suite à une publication visant à décrédibiliser MTN dans le cadre de l\'affaire MBIM', 'https://facebook.com/', NULL, 1, 'MBIM, MTN', '2021-06-30 09:17:18', '2021-06-30 09:17:18', 1, 2),
(3, 7, 4, 1, 3, 'Je reste comme ça les messages tombent seulement en cascades ! Que merci mumu ????! Je dis que eh! Y a quoi même ????? c’est comme ça que un me dit qu’il a regardé mes dernières story sur Ayoba Instant Messaging, ça ne lui a pas coupé ses datas et qu’en plus de ça, il a reçu 100 mégas gratuits ????????!!!\r\nVoici le lien mes braves\r\n\r\nhttps://i.ayo.ba/dQjW/murielblanche allez chercher vos mégas ????????.\r\n\r\nJ’ai dit que ça va aller vite le tour ci!!❤️❤️????', 'https://www.facebook.com/photo/?fbid=364675345016081&set=a.275267190623564', NULL, 1, '#ayoba #LEKATALOG #mtncameroun', '2021-07-07 15:11:08', '2021-07-07 15:11:08', 7, 2),
(4, 7, 4, 4, 3, 'Je reste comme ça les messages tombent seulement en cascades ! Que merci mumu ????! Je dis que eh! Y a quoi même ????? c’est comme ça que un me dit qu’il a regardé mes dernières story sur Ayoba Instant Messaging, ça ne lui a pas coupé ses datas et qu’en plus de ça, il a reçu 100 mégas gratuits ????????!!!\nVoici le lien mes braves\n\nhttps://i.ayo.ba/dQjW/murielblanche allez chercher vos mégas ????????.\n\nJ’ai dit que ça va aller vite le tour ci!!❤️❤️????', 'https://www.facebook.com/photo/?fbid=364675345016081&set=a.275267190623564', NULL, 1, '\n#ayoba #LEKATALOG #mtncameroun #imbattable', '2021-07-07 15:47:13', '2021-07-07 15:47:13', 7, 2),
(5, 7, 4, 1, 2, 'L’Internet IMBATTABLE avec Mtn Home pour toute la famille à partir de 15,000frs.\nC’est la magie ohh \nQui veut les data???\nNdedi Eyango KingmouannkumOffiel\nAlene Menget', 'https://fb.watch/v/1sQC-ayD1/', NULL, 1, '#mtnhome \n#brandambassador  #imbattable #alphafemale #mommaB', '2021-07-07 16:24:40', '2021-07-07 16:24:40', 2, 4),
(6, 7, 4, 4, 2, 'L’Internet IMBATTABLE avec Mtn Home pour toute la famille à partir de 15,000frs.\nC’est la magie ohh \nQui veut les data???\nNdedi Eyango KingmouannkumOffiel\nAlene Menget', 'https://fb.watch/v/1sQC-ayD1/', NULL, 1, '#mtnhome\n#brandambassador #imbattable #alphafemale #mommaB', '2021-07-07 16:45:26', '2021-07-07 16:45:26', 2, 4),
(7, 7, 4, 1, 2, 'Ma République \nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre... \nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout! \nPardon allez vous-même un peu télécharger le way derrière le lien si ????????: https://i.ayo.ba/dQjW/yamo', 'https://fb.watch/6BCZ8KYAHG/', NULL, 1, '#LeMaillotJaune????????', '2021-07-07 17:20:39', '2021-07-07 17:20:39', 11, 2),
(8, 7, 4, 4, 2, 'Ma République\nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre...\n\nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout!\n\nPardon allez vous-même un peu télécharger le way derrière lien si ????????: https://i.ayo.ba/dQjW/yamo\n\net venez vous même me dire ce que vous en pensez.', 'https://www.instagram.com/p/CQyKZUDNUok/?utm_source=ig_web_copy_link', NULL, 1, '#LeMaillotJaune', '2021-07-07 17:27:05', '2021-07-07 17:27:05', 11, 2),
(9, 7, 2, 2, 9, '\nAmis A Vie. ( épisode 01 ) - Le Noir Coeur\n', 'https://www.youtube.com/watch?v=XulfZm0n3EA', NULL, 1, '', '2021-07-07 18:09:37', '2021-07-07 18:09:37', 21, 8),
(10, 7, 2, 2, 9, 'Amis A Vie. ( épisode 02 ) - La mal bouche', 'https://www.youtube.com/watch?v=E0OIxZm2T7o', NULL, 1, '', '2021-07-07 18:14:55', '2021-07-07 18:14:55', 21, 8),
(11, 7, 2, 2, 9, '\nAmis à Vie. (épisode 03) , la boule zéro', 'https://www.youtube.com/watch?v=XsKZTjZnkGA', NULL, 1, '', '2021-07-07 18:20:24', '2021-07-07 18:20:24', 21, 8),
(12, 7, 2, 2, 9, 'Amis A Vie ( épisode 04 ) la conseillère', 'https://www.youtube.com/watch?v=-yS9_38EZno', NULL, 1, '', '2021-07-07 18:26:25', '2021-07-07 18:26:25', 21, 8),
(13, 7, 2, 2, 9, 'Amis A Vie , ( épisode 05) , Viviane', 'https://www.youtube.com/watch?v=GlFYqLn7jB0', NULL, 1, '', '2021-07-07 18:29:26', '2021-07-07 18:29:26', 21, 8),
(14, 7, 4, 5, 2, 'Le njoh vous fait aussi ca? code: *220*2#', 'https://www.tiktok.com/@filatrio/video/6982569258576252166?lang=fr&is_copy_url=0&is_from_webapp=v1&sender_device=pc&sender_web_id=6960579859606455813', NULL, 1, '#filatrio', '2021-07-08 15:18:59', '2021-07-08 15:18:59', 17, 1),
(15, 7, 4, 5, 2, 'Avec MTN tous change ????', 'https://www.tiktok.com/@dr___selfie/video/6982596182509915397?lang=fr&is_copy_url=0&is_from_webapp=v1&sender_device=pc&sender_web_id=6960579859606455813', NULL, 1, '', '2021-07-08 16:48:59', '2021-07-08 16:48:59', 20, 1),
(18, 7, 4, 1, 3, 'Nouvelle collaboration avec MTN Cameroon \nDisons merci à Dieu\nIgwesu!', 'https://scontent-lhr8-1.xx.fbcdn.net/v/t39.30808-6/216026872_352934103079259_5280819255147809554_n.jpg?_nc_cat=106&ccb=1-3&_nc_sid=8bfeb9&_nc_eui2=AeEVfpRsRJ1ODTP7fIWKdvM0woYmfit0CnTChiZ-K3QKdDUbR6LtQiVrvXB3Nndu-hhGXMrlj5EboTMkEkPL42V0&_nc_ohc=JgHXElLC0ic', NULL, 1, '', '2021-07-12 11:23:05', '2021-07-12 11:23:05', 22, 1),
(19, 7, 4, 4, 3, 'Nouveau contrat avec @mtncameroon', 'https://www.instagram.com/p/CRHvd9uFpXb/?utm_source=ig_web_copy_link', NULL, 1, '', '2021-07-12 12:18:50', '2021-07-12 12:18:50', 22, 1),
(20, 7, 4, 1, 2, 'Y\'ello!\nIt\'s holiday time! For the 4th YaMo Class, we\'re going to talk about \"how to make money with agriculture\". Stop everything, because Flavien Kouatcha, our expert, is going to meet you this saturday, July 10th from 11am LIVE on our Facebook page. Come and get practical tips on how to get started and do business.\n', 'https://fb.watch/v/18slEMMTI/', NULL, 1, '', '2021-07-12 13:38:48', '2021-07-12 13:38:48', 4, 1),
(21, 7, 4, 1, 13, 'GIVE AWAY\nAnyone who is the first to write Y in brackets will have 5000frs MTN airtime from MTN ambassador Ni Alenne Menget Ats.YELLOW IS BEST.', 'https://www.facebook.com/alenne.menget/posts/4716207898411916&', NULL, 1, '', '2021-07-13 13:04:30', '2021-07-13 13:04:30', 23, 7),
(22, 7, 4, 2, 2, 'MTN Home: Découvrez l\'internet à domicile moins cher de MTN Cameroon | ECHOSTECH | TELE\'ASU', 'https://www.youtube.com/watch?v=AhIKWbuDq_o&t=25s', NULL, 1, '', '2021-07-15 09:29:13', '2021-07-15 09:29:13', 18, 4),
(23, 7, 4, 1, 8, 'Bonjour ici . ✈️✈️✈️✈️', 'https://www.facebook.com/photo/?fbid=353003396196087&set=a.276310033865424&__cft__[0]=AZXebTfIDIhdo0Tv3hl9Hrx5-VFAtHd3nXFxQ_fsRIhCHySNPidUv8IK_DHnqN3iz0KG6-DV505mof4rk7j9Quq2Tt9eX0g9-XvMdj6k1CEgFat-6pMTInNiQnHrhVTQgv5mWj8DbBh7t3U7GayNOoWg&__tn__=EH-R', NULL, 1, '', '2021-07-15 09:51:03', '2021-07-15 09:51:03', 21, 8),
(24, 7, 2, 1, 3, 'Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic. \nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim. \nBimoulee d’abord !!!', 'https://www.facebook.com/photo/?fbid=353588536137573&set=a.276310040532090&__cft__[0]=AZVEPMdc-4BO2pnbEvrf_qRMgUiLNOlTz36JI0D5tzyBxtRKBr4MT23erKmWGFCb5JTOf5mCv-7GsgQ7NnfWVTJ5WGNmTfMWtpua3MTBEtrz1DnmDWbCyKAyZzmOt__ChycAMkYjYQY-betYc_WR8VUL&__tn__=EH-R', NULL, 1, '', '2021-07-15 10:06:56', '2021-07-15 10:06:56', 21, 8),
(25, 7, 2, 4, 3, 'Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic.\n\nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim.\n\nBimoulee d’abord !!!', 'https://www.instagram.com/p/CRTICi4llQJ/?utm_source=ig_web_copy_link', NULL, 1, '', '2021-07-15 10:13:43', '2021-07-15 10:13:43', 21, 8),
(26, 7, 2, 4, 3, '', 'https://www.instagram.com/p/CRRBEGiFjsS/?utm_source=ig_web_copy_link', NULL, 1, '@valerie_ayena ', '2021-07-15 10:18:42', '2021-07-15 10:18:42', 21, 8),
(27, 7, 2, 1, 2, 'Ma grande sœur chérie Valerie Ayena a un mapane ce soir , je suis passé me rassurer qu’elle était prêt avant de retourner vérifier que les clients de MTN qui ont tapé *211# et qui ont gagné un séjour à Dubaï sont entrain de bien enjoy le moment. \nBimoulee d’abord !!!', 'https://fb.watch/v/Mru3NO3u/', NULL, 1, '', '2021-07-15 10:27:03', '2021-07-15 10:27:03', 21, 8),
(28, 7, 2, 4, 2, 'Ma grande sœur chérie @valerie_ayena est déjà prêt pour le @theemigala de ce soir.\n\nBonne chance à toi ohh ', 'https://www.instagram.com/p/CRUCf_5FIsh/?utm_source=ig_web_copy_link', NULL, 1, '', '2021-07-15 10:32:39', '2021-07-15 10:32:39', 21, 8),
(29, 7, 2, 1, 3, 'La journée peut enfin commencer , en route le périple du jour. \nJe vais aussi voir le Burj khalifa aujourd’hui  avec mes yeux. ????????????????\nN’hésites pas de taper *211# pour souscrit au forfait mtn Magic et tentez aussi de gagner un voyage pour deux à Dubaï . \nJe vais partager avec vous les journées des gagnants. \nBimoulee d’abord !!!', 'https://www.facebook.com/photo/?fbid=354207319409028&set=a.276310033865424&__cft__[0]=AZXMIcuihyrGdPqkfrHaftKudSf1iKoj69PrK40-zjLeCAE7AfJ3bs7js8toYdTqLFJIkTf3PSaR8sYJ6I8zbUacspl3MrD3hTGCrVvMfGbaQh7cGwsHiTMI2A9RYq6PFh1gyFfNHG_u1qqm63kgBeFw&__tn__=EH-R', NULL, 1, '', '2021-07-15 10:41:34', '2021-07-15 10:41:34', 21, 8),
(30, 7, 2, 4, 3, 'La journée peut enfin commencer , en route le périple du jour.\n\nJe vais aussi voir le Burj khalifa aujourd’hui avec mes yeux. ????????????????\n\nN’hésites pas de taper *211# pour souscrit au forfait mtn Magic et tentez aussi de gagner un voyage pour deux à Dubaï .\n\nJe vais partager avec vous les journées des gagnants.\n\nBimoulee d’abord !!!', 'https://www.instagram.com/p/CRVnYPIlBiW/?utm_source=ig_web_copy_link', NULL, 1, '', '2021-07-15 10:45:55', '2021-07-15 10:45:55', 21, 8),
(31, 7, 6, 1, 2, 'I can\'t explain how happy I\'m today for my family, but mostly my mom. A single woman who flawlessly raised 5 kids in an extreme poverty.\nToday she\'s shining all over billboards and TV commercials and she just got started. \nThank you @mtncameroon for making mama look great as a true Queen that she is.', 'https://fb.watch/v/39ta1GTRA/', NULL, 1, '', '2021-07-15 11:12:04', '2021-07-15 11:12:04', 24, 10),
(32, 7, 4, 1, 3, 'UNBREAKABLE ????\nWe stand, we fight odds, we overcome challenges and most of all, we stand proud.', 'https://www.facebook.com/photo/?fbid=358587218959827&set=a.297530621732154&__cft__[0]=AZWwqFy9HknYFQpyKH6ZvoGthTWZ0XgBQ1YpilrZTvki360eEi1bWjuwuEJICiKn7XVxZMgdXOEI19zbzWNujhCAaC7KWPHNji16oC5jtzqjfgnA58uvK93fZEsr0XxgnWDi7RZMqlnjwnkLScFLnkqt&__tn__=EH-R', NULL, 1, '', '2021-07-15 11:22:13', '2021-07-15 11:22:13', 24, NULL),
(33, 7, 4, 1, 3, 'This moment is something I have been waiting for. It is hard to put this feeling in to words. The belt is back home where it belongs ???????????? #CROWNEDCHAMP #AfricaSon', 'https://www.facebook.com/photo/?fbid=320121819473034&set=pcb.320121979473018&__cft__[0]=AZVYHKLZ3tE-TYPYTqZXEyuahnGpBwQJkhz8JgOgwi13gD3nugTr_1gGvFJ3k7HfSf3naA-EFOjK-HbZDQvswtDNdkJWvhAc0YCMHMW1qb8oPM5eKrH85BrZ6dichTD6KlJ_Nm4UUk4a07mU8wZ6Qswn&__tn__=*bH-R', NULL, 1, '', '2021-07-15 11:52:59', '2021-07-15 11:52:59', 24, NULL),
(35, 7, 4, 1, 3, 'It was great having the opportunity to talk with the students of the University of Buéa while doing the #unbeatable campaign and presentating the belt with @mtncameroon. I had a awesome moment out there and was in awe by their joy, happiness and excitement ???? @ Buea Cameroon', 'https://www.facebook.com/photo/?fbid=329661935185689&set=pcb.329661991852350&__cft__[0]=AZWvBP9Km-Xfz1NSo08eItK_glATG3QXVMJCKuihDJYCUjpjE9hkTtBspVV0_699gJDbTiTAJqyAsTu82JIAbO3N2tMEgBz0zZnDeQpwMEe93HvfOspVZfGyli4KZr6nOYRn6-02k_dNsg8yfunHTHQT&__tn__=*bH-R', NULL, 1, '', '2021-07-15 12:06:01', '2021-07-15 12:06:01', 24, NULL),
(36, 7, 4, 1, 3, 'As I\'m leaving cameroon, I want to take this moment to thank @mtncameroon for all the help and support that they brought me to introduce the belt to the people. It was an unprecedented moment, and a lifetime experience that we witnessed during these campaigns and parades all over the country. \nThank you to the CEO, all the supervisors, managers and for all the #unbeatable team who did and #unbeatable job surrounding this historical moment. @ Yaoundé, Cameroon', 'https://www.facebook.com/photo/?fbid=343685403783342&set=pcb.343685763783306&__cft__[0]=AZV2rBQxsAJeo0IxrtDnE98NS7-gAc1C1Bn1x3WDQg3oVK1-W6wP1b8VBpZr1csBt17cuis7eDyKK-LN-rxwhWFH0GkhPIR2gawptUPAzEydaawHT6ZED5onagPzpj8vf9yBUeIprPSGR5jkVjhZhS84&__tn__=*bH-R', NULL, 1, '', '2021-07-15 12:17:40', '2021-07-15 12:17:40', 24, NULL),
(37, 11, 1, 1, 1, 'Comme la maman de Francis N\'Gannou, reçois de l\'argent de l\'Étranger directement dans ton compte MTN MoMo via WorldRemit, MoneyGram, Western Union...', 'https://www.facebook.com/watch/?v=912923002888388', NULL, 1, '#Imbattables #ReseauIndomptable', '2021-07-15 22:25:41', '2021-07-15 22:25:41', 1, 1),
(38, 11, 1, 1, 9, 'azeazeazezaezaezaezaezaezaezaezaezaeazezaeazezaezaeazeazezaeza', 'zeeaezaeazezaezaeaz', NULL, 1, NULL, '2021-07-15 22:56:51', '2021-07-15 22:56:51', 3, 1),
(39, 11, 1, 1, 7, 'dsfsfsdsdfsdfsdfsdfsdsdfsdfdfdsfdsfsdfsdfdsfdsfsdfs', 'sdfsfdsdsdfsdfsd', NULL, 1, NULL, '2021-07-15 23:00:40', '2021-07-15 23:00:40', 7, 1),
(40, 11, 1, 1, 7, 'dsfsfsdsdfsdfsdfsdfsdsdfsdfdfdsfdsfsdfsdfdsfdsfsdfs', 'sdfsfdsdsdfsdfsd', NULL, 1, NULL, '2021-07-15 23:02:14', '2021-07-15 23:02:14', 7, 1),
(41, 11, 1, 1, 1, 'sqdsqdqsdsqdsq', 'qsqsqqsdsqdqs', NULL, 1, NULL, '2021-07-15 23:05:22', '2021-07-15 23:05:22', 7, 1),
(42, 11, 1, 1, 1, 'sdfsdfsdfsdfsdf', 'ssdfsdfsdfsdfsdf', NULL, 1, NULL, '2021-07-15 23:06:43', '2021-07-15 23:06:43', 3, 1),
(43, 11, 1, 1, 1, 'Comme la sqsqsqdsqdsqsqdsq de Francis N\'Gannou, reçois de l\'argent de l\'Étranger directement dans ton compte MTN MoMo via WorldRemit, MoneyGram, Western Union...azeazezaezaeazezaezaezaeazeazeza', 'https://www.facebook.com/watch/?v=912923002888388', NULL, 1, '#Imbattables #ReseauIndomptable', '2021-07-15 23:07:25', '2021-07-15 23:07:25', 1, 1),
(44, 11, 1, 1, 13, 'sadghasfhgda uastdisa sadisad aisdiuasyudug asudiusgd aiisuduiuags aisdgiasugd asdiaugsdias iudgaisugd ouais gdu', 'jsasdhagshdghjsghdf', NULL, 1, NULL, '2021-07-16 10:49:26', '2021-07-16 10:49:26', 7, 1),
(45, 11, 1, 1, 11, 'sadisad assassin assassin assassin asdasda asdasda sadasdsa', 'sasdsdasdas', NULL, 1, NULL, '2021-07-16 10:51:29', '2021-07-16 10:51:29', 7, 1),
(46, 11, 1, 1, 6, 'sadasdasdsadasdasdadsa', 'dfsdfsdfsdsdfsdf', NULL, 1, NULL, '2021-07-16 11:00:50', '2021-07-16 11:00:50', 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `actions_details`
--

CREATE TABLE `actions_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `action_id` int(10) UNSIGNED NOT NULL,
  `kpi_id` int(10) UNSIGNED NOT NULL,
  `date_j` date DEFAULT NULL,
  `value` double NOT NULL DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `actions_details`
--

INSERT INTO `actions_details` (`id`, `user_id`, `action_id`, `kpi_id`, `date_j`, `value`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 3, '2021-06-30', 1000, 1, '2021-06-30 12:14:38', '2021-06-30 12:14:38'),
(3, 1, 1, 2, '2021-06-30', 500, 1, '2021-06-30 12:15:29', '2021-06-30 12:15:29'),
(4, 7, 3, 5, '2021-07-07', 18412, 1, '2021-07-07 15:13:58', '2021-07-07 15:13:58'),
(5, 7, 3, 4, '2021-07-07', 399, 1, '2021-07-07 15:14:46', '2021-07-07 15:14:46'),
(6, 7, 3, 3, '2021-07-07', 14, 1, '2021-07-07 15:15:11', '2021-07-07 15:15:11'),
(11, 7, 4, 4, '2021-07-07', 135, 1, '2021-07-07 15:59:03', '2021-07-07 15:59:03'),
(15, 7, 5, 5, '2021-07-07', 177, 1, '2021-07-07 16:25:45', '2021-07-07 16:25:45'),
(16, 7, 5, 4, '2021-07-07', 7, 1, '2021-07-07 16:26:07', '2021-07-07 16:26:07'),
(17, 7, 5, 3, '2021-07-07', 4, 1, '2021-07-07 16:26:30', '2021-07-07 16:26:30'),
(18, 7, 5, 1, '2021-07-07', 1300, 1, '2021-07-07 16:27:15', '2021-07-07 16:27:15'),
(19, 7, 6, 4, '2021-07-07', 9, 1, '2021-07-07 16:45:59', '2021-07-07 16:45:59'),
(20, 7, 6, 5, '2021-07-07', 2668, 1, '2021-07-07 16:46:24', '2021-07-07 16:46:24'),
(21, 7, 7, 4, '2021-07-07', 144, 1, '2021-07-07 17:22:01', '2021-07-07 17:22:01'),
(22, 7, 7, 5, '2021-07-07', 1000, 1, '2021-07-07 17:22:46', '2021-07-07 17:22:46'),
(23, 7, 7, 3, '2021-07-07', 86, 1, '2021-07-07 17:23:28', '2021-07-07 17:23:28'),
(24, 7, 8, 5, '2021-07-07', 2874, 1, '2021-07-07 17:27:53', '2021-07-07 17:27:53'),
(25, 7, 8, 4, '2021-07-07', 21, 1, '2021-07-07 17:29:16', '2021-07-07 17:29:16'),
(26, 7, 9, 5, '2021-07-07', 2900, 1, '2021-07-07 18:10:44', '2021-07-07 18:10:44'),
(27, 7, 9, 1, '2021-07-07', 85078, 1, '2021-07-07 18:11:54', '2021-07-07 18:11:54'),
(28, 7, 10, 5, '2021-07-07', 1900, 1, '2021-07-07 18:16:02', '2021-07-07 18:16:02'),
(29, 7, 10, 1, '2021-07-07', 55726, 1, '2021-07-07 18:16:49', '2021-07-07 18:16:49'),
(30, 7, 11, 5, '2021-07-07', 1500, 1, '2021-07-07 18:21:56', '2021-07-07 18:21:56'),
(31, 7, 11, 1, '2021-07-07', 45588, 1, '2021-07-07 18:23:55', '2021-07-07 18:23:55'),
(32, 7, 12, 5, '2021-07-07', 1600, 1, '2021-07-07 18:27:07', '2021-07-07 18:27:07'),
(33, 7, 12, 1, '2021-07-07', 44474, 1, '2021-07-07 18:27:39', '2021-07-07 18:27:39'),
(34, 7, 13, 5, '2021-07-07', 2000, 1, '2021-07-07 18:30:35', '2021-07-07 18:30:35'),
(35, 7, 13, 1, '2021-07-07', 53517, 1, '2021-07-07 18:31:15', '2021-07-07 18:31:15'),
(38, 7, 14, 5, '2021-07-08', 13, 1, '2021-07-08 15:23:32', '2021-07-08 15:23:32'),
(39, 7, 14, 4, '2021-07-08', 2, 1, '2021-07-08 15:23:51', '2021-07-08 15:23:51'),
(40, 7, 14, 1, '2021-07-08', 53, 1, '2021-07-08 15:24:46', '2021-07-08 15:24:46'),
(41, 7, 4, 5, '2021-07-08', 16959, 1, '2021-07-08 16:14:40', '2021-07-08 16:14:40'),
(42, 7, 15, 1, '2021-07-08', 81, 1, '2021-07-08 16:49:53', '2021-07-08 16:49:53'),
(43, 7, 15, 5, '2021-07-08', 18, 1, '2021-07-08 16:50:08', '2021-07-08 16:50:08'),
(44, 7, 10, 4, '2021-07-08', 145, 1, '2021-07-08 17:03:25', '2021-07-08 17:03:25'),
(45, 7, 13, 4, '2021-07-08', 98, 1, '2021-07-08 17:04:44', '2021-07-08 17:04:44'),
(46, 7, 12, 4, '2021-07-08', 81, 1, '2021-07-08 17:06:21', '2021-07-08 17:06:21'),
(47, 7, 9, 4, '2021-07-08', 203, 1, '2021-07-08 17:08:04', '2021-07-08 17:08:04'),
(48, 7, 11, 4, '2021-07-08', 71, 1, '2021-07-08 17:11:10', '2021-07-08 17:11:10'),
(49, 7, 18, 4, '2021-07-12', 5000, 1, '2021-07-12 11:24:56', '2021-07-12 11:24:56'),
(50, 7, 18, 5, '2021-07-12', 62000, 1, '2021-07-12 11:25:38', '2021-07-12 11:25:38'),
(51, 7, 18, 3, '2021-07-12', 146, 1, '2021-07-12 11:26:04', '2021-07-12 11:26:04'),
(52, 7, 19, 5, '2021-07-12', 3553, 1, '2021-07-12 12:24:55', '2021-07-12 12:24:55'),
(53, 7, 19, 4, '2021-07-12', 78, 1, '2021-07-12 12:25:22', '2021-07-12 12:25:22'),
(54, 7, 20, 5, '2021-07-12', 101, 1, '2021-07-12 13:39:54', '2021-07-12 13:39:54'),
(55, 7, 20, 4, '2021-07-12', 13, 1, '2021-07-12 13:40:20', '2021-07-12 13:40:20'),
(56, 7, 20, 3, '2021-07-12', 15, 1, '2021-07-12 13:40:48', '2021-07-12 13:40:48'),
(57, 7, 21, 5, '2021-07-13', 77, 1, '2021-07-13 13:05:46', '2021-07-13 13:05:46'),
(58, 7, 21, 4, '2021-07-13', 166, 1, '2021-07-13 13:06:15', '2021-07-13 13:06:15'),
(59, 7, 21, 3, '2021-07-13', 18, 1, '2021-07-13 13:06:42', '2021-07-13 13:06:42'),
(60, 7, 22, 1, '2021-07-15', 32, 1, '2021-07-15 09:31:18', '2021-07-15 09:31:18'),
(61, 7, 22, 4, '2021-07-15', 0, 1, '2021-07-15 09:31:43', '2021-07-15 09:31:43'),
(62, 7, 22, 5, '2021-07-15', 0, 1, '2021-07-15 09:32:18', '2021-07-15 09:32:18'),
(63, 7, 23, 5, '2021-07-15', 15000, 1, '2021-07-15 09:52:47', '2021-07-15 09:52:47'),
(64, 7, 23, 4, '2021-07-15', 722, 1, '2021-07-15 09:53:15', '2021-07-15 09:53:15'),
(65, 7, 23, 3, '2021-07-15', 15, 1, '2021-07-15 09:53:42', '2021-07-15 09:53:42'),
(66, 7, 24, 5, '2021-07-15', 18, 1, '2021-07-15 10:08:47', '2021-07-15 10:08:47'),
(67, 7, 24, 4, '2021-07-15', 687, 1, '2021-07-15 10:09:06', '2021-07-15 10:09:06'),
(68, 7, 24, 3, '2021-07-15', 22, 1, '2021-07-15 10:09:41', '2021-07-15 10:09:41'),
(69, 7, 25, 5, '2021-07-15', 7342, 1, '2021-07-15 10:15:17', '2021-07-15 10:15:17'),
(70, 7, 25, 4, '2021-07-15', 110, 1, '2021-07-15 10:15:32', '2021-07-15 10:15:32'),
(71, 7, 26, 4, '2021-07-15', 63, 1, '2021-07-15 10:20:57', '2021-07-15 10:20:57'),
(72, 7, 26, 5, '2021-07-15', 4091, 1, '2021-07-15 10:21:27', '2021-07-15 10:21:27'),
(73, 7, 27, 5, '2021-07-15', 2570, 1, '2021-07-15 10:29:02', '2021-07-15 10:29:02'),
(74, 7, 27, 4, '2021-07-15', 90, 1, '2021-07-15 10:29:24', '2021-07-15 10:29:24'),
(75, 7, 27, 3, '2021-07-15', 6, 1, '2021-07-15 10:29:45', '2021-07-15 10:29:45'),
(76, 7, 28, 5, '2021-07-15', 17871, 1, '2021-07-15 10:33:31', '2021-07-15 10:33:31'),
(77, 7, 28, 4, '2021-07-15', 45, 1, '2021-07-15 10:33:53', '2021-07-15 10:33:53'),
(78, 7, 29, 5, '2021-07-15', 4320, 1, '2021-07-15 10:42:38', '2021-07-15 10:42:38'),
(79, 7, 29, 4, '2021-07-15', 215, 1, '2021-07-15 10:43:02', '2021-07-15 10:43:02'),
(80, 7, 29, 3, '2021-07-15', 2, 1, '2021-07-15 10:43:15', '2021-07-15 10:43:15'),
(81, 7, 30, 5, '2021-07-15', 1596, 1, '2021-07-15 10:48:04', '2021-07-15 10:48:04'),
(82, 7, 30, 4, '2021-07-15', 22, 1, '2021-07-15 10:48:20', '2021-07-15 10:48:20'),
(83, 7, 31, 5, '2021-07-15', 7430, 1, '2021-07-15 11:13:28', '2021-07-15 11:13:28'),
(84, 7, 31, 4, '2021-07-15', 308, 1, '2021-07-15 11:13:55', '2021-07-15 11:13:55'),
(85, 7, 31, 3, '2021-07-15', 196, 1, '2021-07-15 11:14:25', '2021-07-15 11:14:25'),
(86, 7, 32, 5, '2021-07-15', 19000, 1, '2021-07-15 11:24:56', '2021-07-15 11:24:56'),
(87, 7, 32, 4, '2021-07-15', 293, 1, '2021-07-15 11:25:19', '2021-07-15 11:25:19'),
(88, 7, 32, 3, '2021-07-15', 204, 1, '2021-07-15 11:25:42', '2021-07-15 11:25:42'),
(89, 7, 33, 5, '2021-07-15', 88000, 1, '2021-07-15 11:59:37', '2021-07-15 11:59:37'),
(90, 7, 33, 4, '2021-07-15', 5400, 1, '2021-07-15 11:59:58', '2021-07-15 11:59:58'),
(91, 7, 33, 3, '2021-07-15', 3900, 1, '2021-07-15 12:00:15', '2021-07-15 12:00:15'),
(92, 7, 35, 5, '2021-07-15', 58000, 1, '2021-07-15 12:09:25', '2021-07-15 12:09:25'),
(93, 7, 35, 4, '2021-07-15', 1600, 1, '2021-07-15 12:09:51', '2021-07-15 12:09:51'),
(94, 7, 35, 3, '2021-07-15', 404, 1, '2021-07-15 12:10:39', '2021-07-15 12:10:39'),
(95, 11, 1, 2, '2021-07-16', 200, 1, '2021-07-16 09:50:48', '2021-07-16 09:50:48'),
(96, 11, 41, 1, '2021-07-16', 500, 1, '2021-07-16 11:31:35', '2021-07-16 11:31:35'),
(97, 11, 41, 1, '2021-07-16', 1000000, 1, '2021-07-16 11:42:49', '2021-07-16 11:42:49'),
(98, 11, 41, 3, '2021-07-16', 500, 1, '2021-07-16 12:41:54', '2021-07-16 12:41:54'),
(99, 11, 40, 1, '2021-07-16', 500, 1, '2021-07-16 12:48:12', '2021-07-16 12:48:12'),
(100, 11, 46, 5, '2021-07-16', 200, 1, '2021-07-16 13:06:03', '2021-07-16 13:06:03'),
(101, 11, 46, 5, '2021-07-16', 300, 1, '2021-07-16 13:08:56', '2021-07-16 13:08:56');

-- --------------------------------------------------------

--
-- Structure de la table `actions_files`
--

CREATE TABLE `actions_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `action_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(45) NOT NULL,
  `link` varchar(45) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `actions_files`
--

INSERT INTO `actions_files` (`id`, `action_id`, `type`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 'Images', '209175941_364675355016080_7574562898651915843', 1, '2021-07-07 15:12:09', '2021-07-07 15:12:09'),
(2, 4, 'Images', '209175941_364675355016080_7574562898651915843', 1, '2021-07-07 15:48:50', '2021-07-07 15:48:50'),
(3, 18, 'Images', '215812257_352934223079247_1191266640396946237', 1, '2021-07-12 11:51:36', '2021-07-12 11:51:36'),
(4, 18, 'Images', '214852297_352934089745927_2279933334192918930', 1, '2021-07-12 11:51:55', '2021-07-12 11:51:55'),
(5, 18, 'Images', '216026872_352934103079259_5280819255147809554', 1, '2021-07-12 11:52:14', '2021-07-12 11:52:14'),
(6, 18, 'Images', '216034267_352934253079244_2093664310384675475', 1, '2021-07-12 11:52:35', '2021-07-12 11:52:35'),
(7, 19, 'Images', '214852297_352934089745927_2279933334192918930', 1, '2021-07-12 12:22:15', '2021-07-12 12:22:15'),
(8, 19, 'Images', '215701110_352934193079250_5291100503327127319', 1, '2021-07-12 12:22:43', '2021-07-12 12:22:43'),
(9, 19, 'Images', '215812257_352934223079247_1191266640396946237', 1, '2021-07-12 12:23:11', '2021-07-12 12:23:11'),
(10, 19, 'Images', '216026872_352934103079259_5280819255147809554', 1, '2021-07-12 12:23:40', '2021-07-12 12:23:40'),
(11, 23, 'Images', 'Capture d’écran 2021-07-15 114940.jpg', 1, '2021-07-15 09:52:10', '2021-07-15 09:52:10'),
(12, 24, 'Images', 'Capture d’écran 2021-07-15 120517.jpg', 1, '2021-07-15 10:08:22', '2021-07-15 10:08:22'),
(13, 25, 'Images', 'Capture d’écran 2021-07-15 120517(1).jpg', 1, '2021-07-15 10:14:34', '2021-07-15 10:14:34'),
(14, 26, 'Images', 'Capture d’écran 2021-07-15 114940(1).jpg', 1, '2021-07-15 10:19:45', '2021-07-15 10:19:45'),
(15, 30, 'Images', 'Capture d’écran 2021-07-15 124702.jpg', 1, '2021-07-15 10:47:30', '2021-07-15 10:47:30'),
(16, 32, 'Images', 'Capture d’écran 2021-07-15 132321.jpg', 1, '2021-07-15 11:23:52', '2021-07-15 11:23:52'),
(17, 33, 'Images', 'Capture d’écran 2021-07-15 135419.jpg', 1, '2021-07-15 11:56:44', '2021-07-15 11:56:44'),
(18, 33, 'Images', 'Capture d’écran 2021-07-15 135540.jpg', 1, '2021-07-15 11:56:57', '2021-07-15 11:56:57'),
(19, 33, 'Images', 'Capture d’écran 2021-07-15 135853.jpg', 1, '2021-07-15 12:00:40', '2021-07-15 12:00:40'),
(20, 35, 'Images', 'Capture d’écran 2021-07-15 140713.jpg', 1, '2021-07-15 12:07:39', '2021-07-15 12:07:39'),
(21, 35, 'Images', 'Capture d’écran 2021-07-15 140822.jpg', 1, '2021-07-15 12:08:41', '2021-07-15 12:08:41'),
(22, 36, 'Images', 'Capture d’écran 2021-07-15 141240.jpg', 1, '2021-07-15 12:23:40', '2021-07-15 12:23:40'),
(23, 36, 'Images', 'Capture d’écran 2021-07-15 141329.jpg', 1, '2021-07-15 12:23:57', '2021-07-15 12:23:57'),
(25, 36, 'Images', 'Capture d’écran 2021-07-15 141411.jpg', 1, '2021-07-15 12:24:21', '2021-07-15 12:24:21'),
(26, 36, 'Images', 'Capture d’écran 2021-07-15 141451.jpg', 1, '2021-07-15 12:24:37', '2021-07-15 12:24:37'),
(27, 36, 'Images', 'Capture d’écran 2021-07-15 141533.jpg', 1, '2021-07-15 12:24:51', '2021-07-15 12:24:51'),
(28, 36, 'Images', 'Capture d’écran 2021-07-15 141615.jpg', 1, '2021-07-15 12:25:05', '2021-07-15 12:25:05'),
(29, 43, 'Images', 'mtn_news.jpg', 1, '2021-07-16 12:04:19', '2021-07-16 12:04:19'),
(30, 1, 'Image', 'cBlPa6kAOdqSP90BcI4hl2SckKvHIcu7pTRwgNHI.jpg', 1, '2021-07-16 12:19:33', '2021-07-16 12:19:33'),
(31, 1, 'Image', 's3R4vFBTiUCTKb9qLXG1VNKyaXfoAEpYGPrz7LsV.jpg', 1, '2021-07-16 12:34:59', '2021-07-16 12:34:59'),
(32, 1, 'Image', 'yJWqqBx2H9T8mY3AV908HYulZMr5oijWAyiUQwSj.jpg', 1, '2021-07-16 12:50:09', '2021-07-16 12:50:09');

-- --------------------------------------------------------

--
-- Structure de la table `actions_types`
--

CREATE TABLE `actions_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `icon` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `actions_types`
--

INSERT INTO `actions_types` (`id`, `name`, `status`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'Commentaires ', 1, '', '2021-06-30 08:51:19', '2021-06-30 08:51:19'),
(2, 'Publication Vidéo ', 1, '', '2021-06-30 08:51:35', '2021-06-30 08:51:35'),
(3, 'Publication Texte', 1, '', '2021-06-30 08:51:52', '2021-06-30 08:51:52'),
(6, 'Live', 1, '', '2021-07-07 11:27:40', '2021-07-07 11:27:40'),
(7, 'Insert Ads', 1, '', '2021-07-07 15:38:16', '2021-07-07 15:38:16'),
(8, 'Apparition aux évènements', 1, '', '2021-07-07 15:38:50', '2021-07-07 15:38:50'),
(9, 'Episode WebSerie', 1, '', '2021-07-07 15:40:09', '2021-07-07 15:40:09'),
(10, 'Experience Sharing', 1, '', '2021-07-07 15:40:29', '2021-07-07 15:40:29'),
(11, 'Photo OOH', 1, '', '2021-07-07 15:41:02', '2021-07-07 15:41:02'),
(12, 'Monday Motivation', 1, '', '2021-07-07 15:42:01', '2021-07-07 15:42:01'),
(13, 'Gaming', 1, '', '2021-07-07 15:42:19', '2021-07-07 15:42:19');

-- --------------------------------------------------------

--
-- Structure de la table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `images` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `campaigns`
--

INSERT INTO `campaigns` (`id`, `company_id`, `name`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`, `images`) VALUES
(1, 1, 'Default Campagn', '2021-06-29 00:00:00', '2026-06-23 00:00:00', 1, '2021-06-29 15:47:11', '2021-06-29 15:47:11', NULL),
(2, 1, 'Magic Dubaï', '2021-05-01 00:00:00', '2022-07-01 00:00:00', 1, '2021-07-07 11:12:59', '2021-07-07 11:12:59', NULL),
(3, 1, 'MoMo Bonus Imbattable', '2021-06-15 00:00:00', '2021-07-15 00:00:00', 1, '2021-07-07 11:15:54', '2021-07-07 11:15:54', NULL),
(4, 1, 'Unbeatable Campaign', '2021-01-01 00:00:00', '2026-07-01 00:00:00', 1, '2021-07-07 11:19:26', '2021-07-07 11:19:26', NULL),
(5, 1, 'YaMo', '2021-07-01 00:00:00', '2021-12-01 00:00:00', 1, '2021-07-07 11:21:29', '2021-07-07 11:21:29', NULL),
(6, 1, 'Remittence', '2021-05-01 00:00:00', '2021-12-31 00:00:00', 1, '2021-07-07 11:22:19', '2021-07-07 11:22:19', NULL),
(7, 1, 'Home', '2021-05-01 00:00:00', '2021-12-31 00:00:00', 1, '2021-07-07 11:23:02', '2021-07-07 11:23:02', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `campaign_contract_kpis`
--

CREATE TABLE `campaign_contract_kpis` (
  `id` int(10) UNSIGNED NOT NULL,
  `kpi_id` int(10) UNSIGNED NOT NULL,
  `campaign_contract_id` int(10) UNSIGNED NOT NULL,
  `values` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `contract_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `campaign_contract_kpis`
--

INSERT INTO `campaign_contract_kpis` (`id`, `kpi_id`, `campaign_contract_id`, `values`, `status`, `created_at`, `updated_at`, `contract_id`) VALUES
(1, 3, 1, 500, 1, '2021-06-29 15:55:58', '2021-06-29 15:55:58', NULL),
(2, 1, 1, 250, 1, '2021-06-29 15:56:27', '2021-06-29 15:56:27', NULL),
(3, 1, 1, 250, 1, '2021-07-07 11:44:07', '2021-07-07 11:44:07', NULL),
(4, 2, 1, 10, 1, '2021-07-07 13:03:55', '2021-07-07 13:03:55', NULL),
(7, 1, 2, 10, 1, '2021-07-07 13:27:29', '2021-07-07 13:27:29', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'macro influenceur ', 'BIG', 1, '2021-06-28 14:09:03', '2021-06-28 14:09:03'),
(2, 'micro influenceur ', 'MIF', 1, '2021-07-07 08:40:49', '2021-07-07 08:40:49'),
(3, 'nano influenceur ', 'NIF', 1, '2021-07-07 08:42:41', '2021-07-07 08:42:41');

-- --------------------------------------------------------

--
-- Structure de la table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `phone`, `status`, `created_at`, `updated_at`, `logo`) VALUES
(1, 'MTN', 'Roger.Andang@mtn.com', '237 677550936', 1, '2021-06-28 12:35:38', '2021-06-28 12:35:38', ''),
(2, 'MUTZIG', 'cbitom@sabc-cm.com', '666666666', 1, '2021-07-02 14:18:05', '2021-07-02 14:18:05', '');

-- --------------------------------------------------------

--
-- Structure de la table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `influenceur_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `scane` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contracts`
--

INSERT INTO `contracts` (`id`, `start_date`, `end_date`, `description`, `created_at`, `updated_at`, `influenceur_id`, `status`, `scane`, `label`, `company_id`) VALUES
(1, '2021-01-01', '2021-12-31', 'contrat entre MTN et Cabrel Nanjip ', '2021-06-28 14:39:10', '2021-06-28 14:39:10', 13, 1, 'INF - Contrat Engagement et Cession de Droit - Cabrel NANJIP(1).doc', '', 1),
(2, '2021-01-01', '2021-12-31', 'contrat entre MTN et Syndy Emane ', '2021-07-07 13:12:42', '2021-07-07 13:12:42', 3, 1, 'INF - Contrat Engagement et Cession de Droit Syndy Emade (1).doc', '', 1),
(3, '2021-01-01', '2021-12-31', 'contrat entre MTN et Fingon ', '2021-07-07 14:12:17', '2021-07-07 14:12:17', 1, 1, '', '', 1),
(4, '2021-01-01', '2021-12-31', 'contrat entre MTN et Flavien Kouatcha ', '2021-07-07 14:49:19', '2021-07-07 14:49:19', 7, 1, 'INF - Contrat Engagement et Cession de Droit - Flavien KOUATCHA.doc', '', 1),
(5, '2021-01-01', '2021-12-31', 'contrat entre MTN et Frieda Choco', '2021-07-07 14:52:28', '2021-07-07 14:52:28', 10, 1, 'INF - Contrat Engagement et Cession de Droit - Frieda Choco Bronze.doc', '', 1),
(6, '2021-01-01', '2021-12-31', 'contrat entre MTN et Cabrel Nanjip ', '2021-07-07 14:56:49', '2021-07-07 14:56:49', 13, 1, 'INF - Contrat Engagement et Cession de Droit - Cabrel NANJIP.doc', '', 1),
(7, '2021-01-01', '2021-12-31', 'Contrat entre MTN et Muriel Blanche', '2021-07-07 15:07:01', '2021-07-07 15:07:01', 2, 1, '', '', 1),
(10, '2021-01-01', '2021-12-31', 'contrat entre MTN et Tenor', '2021-07-07 15:36:28', '2021-07-07 15:36:28', 4, 1, '', '', 1),
(11, '2021-01-01', '2021-12-31', 'contrat entre MTN et Minks', '2021-07-07 15:39:07', '2021-07-07 15:39:07', 5, 1, '', '', 1),
(12, '2021-01-01', '2021-12-31', 'contrat entre MTN et Ko-C', '2021-07-07 15:40:37', '2021-07-07 15:40:37', 6, 1, '', '', 1),
(13, '2021-01-01', '2021-12-31', 'contrat entre MTN et Philippe Simo', '2021-07-07 15:41:54', '2021-07-07 15:41:54', 8, 1, '', '', 1),
(14, '2021-01-01', '2021-12-31', 'contrat entre MTN et Claudel Noubissi ', '2021-07-07 15:43:42', '2021-07-07 15:43:42', 9, 1, '', '', 1),
(15, '2021-01-01', '2021-12-31', 'contrat entre OS et Caprice Audrey ', '2020-12-31 23:00:00', '2021-11-30 23:00:00', 14, 1, '', '', 1),
(16, '2021-01-01', '2021-12-31', 'contrat entre MTN et Dr Ni', '2020-12-31 23:00:00', '2021-11-30 23:00:00', 15, 1, '', '', 1),
(17, '2021-01-01', '2021-12-31', 'contrat entre OS et Filantrio ', '2021-07-07 16:13:15', '2021-07-07 16:13:15', 16, 1, '', '', 1),
(18, '2021-01-01', '2021-12-31', 'contrat entre OS et Annie Payep ', '2021-07-07 16:15:34', '2021-07-07 16:15:34', 17, 1, '', '', 1),
(19, '2021-01-01', '2021-12-31', 'contrat entre OS et Chouchou Mpacko ', '2021-07-07 16:17:40', '2021-07-07 16:17:40', 18, 1, '', '', 1),
(20, '2021-01-01', '2021-12-31', 'contrat entre OS et Dr selfie ', '2021-07-07 16:19:06', '2021-07-07 16:19:06', 19, 1, '', '', 1),
(21, '2021-05-01', '2021-12-31', 'Contrat entre MTN et Takam', '2021-07-07 18:05:21', '2021-07-07 18:05:21', 11, 1, '', '', 1),
(22, '2021-07-09', '2021-10-09', 'Contrat entre OS et Ma\'a Jacky', '2021-07-12 10:52:02', '2021-07-12 10:52:02', 20, 1, '', '', 1),
(23, '2021-06-01', '2022-06-01', 'Contrat entre MTN et Allenne Menget', '2021-07-13 12:56:21', '2021-07-13 12:56:21', 21, 1, '', '', 1),
(24, '2021-05-01', '2021-12-31', 'Contrat entre MTN et Francis N\'gannou', '2021-07-15 11:08:45', '2021-07-15 11:08:45', 22, 1, '', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `contract_campaigns`
--

CREATE TABLE `contract_campaigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `contract_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contract_campaigns`
--

INSERT INTO `contract_campaigns` (`id`, `contract_id`, `campaign_id`, `status`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1', '2021-06-29', '2024-06-20', '2021-06-29 15:49:11', '2021-06-29 15:49:11'),
(2, 1, 4, '1', '2021-07-07', '2026-07-01', '2021-07-07 11:41:46', '2021-07-07 11:41:46'),
(3, 3, 4, '1', '2021-06-01', '2021-12-31', '2021-07-08 10:12:56', '2021-07-08 10:12:56'),
(4, 3, 1, '1', '2021-06-01', '2021-12-31', '2021-07-08 10:13:32', '2021-07-08 10:13:32');

-- --------------------------------------------------------

--
-- Structure de la table `contract_kpis`
--

CREATE TABLE `contract_kpis` (
  `id` int(10) UNSIGNED NOT NULL,
  `kpi_id` int(10) UNSIGNED NOT NULL,
  `contract_id` int(10) UNSIGNED NOT NULL,
  `values` double NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contract_kpis`
--

INSERT INTO `contract_kpis` (`id`, `kpi_id`, `contract_id`, `values`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 500, 1, '2021-06-28 14:46:59', '2021-06-28 14:46:59'),
(2, 3, 1, 1000, 1, '2021-06-29 15:57:34', '2021-06-29 15:57:34'),
(3, 4, 3, 5000, 1, '2021-07-08 10:10:07', '2021-07-08 10:10:07'),
(4, 5, 3, 20000, 1, '2021-07-08 10:10:22', '2021-07-08 10:10:22'),
(5, 2, 3, 100, 1, '2021-07-08 10:10:59', '2021-07-08 10:10:59'),
(6, 1, 3, 50000, 1, '2021-07-08 10:11:14', '2021-07-08 10:11:14'),
(7, 3, 3, 1000, 1, '2021-07-08 10:11:52', '2021-07-08 10:11:52');

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `company_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `groups`
--

INSERT INTO `groups` (`id`, `name`, `type`, `status`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 'influenceurs Classe 1', 'influenceur ', 1, 1, '2021-06-30 13:17:04', '2021-06-30 13:17:04');

-- --------------------------------------------------------

--
-- Structure de la table `group_user`
--

CREATE TABLE `group_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `influencer_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `group_user`
--

INSERT INTO `group_user` (`id`, `influencer_id`, `group_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2021-07-01 09:38:41', '2021-07-01 09:38:41');

-- --------------------------------------------------------

--
-- Structure de la table `influencers`
--

CREATE TABLE `influencers` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `full_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `images` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `influencers`
--

INSERT INTO `influencers` (`id`, `company_id`, `category_id`, `name`, `full_name`, `email`, `phone`, `status`, `created_at`, `updated_at`, `images`) VALUES
(1, 1, 1, 'Figon ', 'Figon Tralala', 'Figontralala@figon.com', '677777777', 1, '2021-06-28 14:10:13', '2021-06-28 14:10:13', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIRFRIRERESEREREREREREREREPERERGBgZGRgYGBgcIS4lHB4rHxgYJjgnKy8xNTU1GiQ7QjszPy40NTEBDAwMEA8QHBISGjYhISsxNDQ0NDQxMTExNDQ0NDQ0MTQ1MTQxNDQ0NDQ0NDE0NDQ0NzExMTQ0NDQxNDQ0MTQ0Mf/AABEIAKkBKwMBIgACEQEDEQH/'),
(2, 1, 1, 'Muriel Blanche ', 'Muriel Blanche ', 'murielblanche@os.com', '686888855', 1, '2021-07-07 08:46:27', '2021-07-07 08:46:27', 'https://pbs.twimg.com/profile_images/1356295643405438976/sOFIkv7e_400x400.jpg'),
(3, 1, 1, 'Syndy Emane ', 'Syndy Emane ', 'synemane@gmail.com ', '677777777', 1, '2021-07-07 09:14:11', '2021-07-07 09:14:11', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgVFhUYGBgYGBgYGBgYGBgYGBoaGBgZGhoaGRgcIS4lHB4rIRgYJjgmKy8xNTU1GiU7QDs0Py40NTEBDAwMEA8QHhISHjQrJCE1NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAOAA4QMBIgACEQEDEQH/'),
(4, 1, 1, 'Tenor ', 'Tenor ', 'tenor@gmail.com', '677777777', 1, '2021-07-07 09:17:59', '2021-07-07 09:17:59', 'https://www.vudaf.com/wp-content/uploads/2019/11/tenor.jpg'),
(5, 1, 2, 'Minks ', 'Minks ', 'minnkss@gmail.com', '677777777', 1, '2021-07-07 09:20:35', '2021-07-07 09:20:35', 'https://cdn.camerounweb.com/imagelib/src/Minks_en_feat_xmaleya.jpg'),
(6, 1, 2, 'Ko-C', 'Ko-C', 'koc@kocc.os ', '677777777', 1, '2021-07-07 09:32:33', '2021-07-07 09:32:33', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQg1drE_vfBy63_a6O_sJUlwvaoigOGSQwmvqStewvaqE5wiv7FQ-c4ajlifffsgkuLshI&usqp=CAU'),
(7, 1, 2, 'Flavien Kouatcha ', 'Flavien Kouatcha ', 'flavien@koua.os', '677777777', 1, '2021-07-07 09:37:01', '2021-07-07 09:37:01', NULL),
(8, 1, 2, 'Philippe Simo ', 'Philippe Simo ', 'philipp@simo.os', '677777777', 1, '2021-07-07 09:38:36', '2021-07-07 09:38:36', NULL),
(9, 1, 2, 'Claudel Noubissi ', 'Claudel Noubissi ', 'claudel@noub.os ', '677777777', 1, '2021-07-07 09:40:39', '2021-07-07 09:40:39', NULL),
(10, 1, 2, 'Frieda Choco ', 'Frieda Choco ', 'frifri@cho.os', '677777777', 1, '2021-07-07 09:44:04', '2021-07-07 09:44:04', NULL),
(11, 1, 2, 'Ulrich Takam ', 'Ulrich Takam ', 'ulrich@tak.os', '674847864', 1, '2021-07-07 10:03:59', '2021-07-07 10:03:59', 'http://leconomie.cm/wp-content/uploads/2020/03/Ulrich-Takam.jpg'),
(12, 1, 2, 'Atome ', 'Atome ', 'at@me.os ', '675378958', 1, '2021-07-07 10:06:10', '2021-07-07 10:06:10', NULL),
(13, 1, 2, 'Cabrel Nanjip ', 'Cabrel Nanjip ', 'cabrel@nanjip.os', '677777777', 1, '2021-07-07 10:08:42', '2021-07-07 10:08:42', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgVEhIYEhIYEhEYEhERGBgREhISGBgZGRgYGBgcIS4lHB4rIRgaJzooKy8xNTU1GiQ7QDs0Py41NTEBDAwMEA8QHxISHzQrJCs3NDY2MTQ0NDQ0NDcxNDQ1NDU0MTQ0NDQ0NDQ0NDQ0NDQ0OjQ0NDQ9NDU0NDQxNDQ0NP/AABEIAOEA4QMBIgACEQEDEQH/'),
(14, 1, 2, 'Caprice Audrey ', 'Caprice Audrey ', 'caprice@audrey.os', '677777777', 1, '2021-07-07 10:33:05', '2021-07-07 10:33:05', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgVFhYYGBgYGBgYGBgZGBgYGBgaGhocGRoaGBkcIS4lHB4rHxoaJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHjQsJCw0NDQxNDQ0NDQ0NjQ0NDQ0MTE0NDQ0NDY0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAPYAzQMBIgACEQEDEQH/'),
(15, 1, 1, 'Dr Ni ', 'Dr Ni ', 'dr@ni.os', '677777777', 1, '2021-07-07 11:16:47', '2021-07-07 11:16:47', NULL),
(16, 1, 3, 'Filantrio ', 'Filantrio ', 'flan@trio.os', '677777777', 1, '2021-07-07 11:19:32', '2021-07-07 11:19:32', NULL),
(17, 1, 2, 'Annie Payep ', 'Annie Payep ', 'annie@payep.os ', '677777777', 1, '2021-07-07 11:21:38', '2021-07-07 11:21:38', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUUFBgVFBUZGRgaGyAeGxobGiMaGxggGxskIh4aHhskIC0kGyApHhobJjclKS4wNDQ0GiM5PzkyPi0yNDABCwsLEA8QHRISHTIrIykyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIwMjIyMjIyMv/AABEIAOEA4QMBIgACEQEDEQH/'),
(18, 1, 3, 'Chouchou Mpacko ', 'Chouchou Mpacko ', 'chou@mpack.os ', '677777777', 1, '2021-07-07 11:23:48', '2021-07-07 11:23:48', NULL),
(19, 1, 3, 'Dr Selfie ', 'Dr Selfie ', 'dr@selfie.os', '677777777', 1, '2021-07-07 11:25:27', '2021-07-07 11:25:27', 'https://www.google.com/search?q=Dr+Selfie&client=firefox-b-d&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj45vqDltbxAhVG3KQKHX6dAlsQ_AUoAXoECAEQAw&biw=1664&https://p16-va.tiktokcdn.com/musically-maliva-obj/1661560638772229~c5_720x720.jpeg?x-expires=1607620100&x-s'),
(20, 1, 3, 'Ma\'a jacky', 'Ma\'a jacky', 'Ma\'a jacky@gmail.com', '677777777', 1, '2021-07-12 10:35:47', '2021-07-12 10:35:47', NULL),
(21, 1, 1, 'Allenne Menget (Mr Ni)', 'Allenne Menget (Mr Ni)', 'AllenneMenget(Mr Ni)@gmail.com', '679797979', 1, '2021-07-13 12:54:25', '2021-07-13 12:54:25', NULL),
(22, 1, 1, 'Francis N\'gannou', 'Francis N\'gannou', 'Francis N\'gannou@gmail.com', '677777777', 1, '2021-07-15 11:05:44', '2021-07-15 11:05:44', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `influencer_managers`
--

CREATE TABLE `influencer_managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `influenceur_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `influencer_managers`
--

INSERT INTO `influencer_managers` (`id`, `user_id`, `influenceur_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2021-07-02 14:58:05', '2021-07-02 14:58:05'),
(2, 11, 1, 1, '2021-07-02 12:58:05', '2021-07-02 12:58:05'),
(3, 11, 2, 1, '2021-07-02 12:58:05', '2021-07-02 12:58:05'),
(4, 11, 13, 1, '2021-07-02 12:58:05', '2021-07-02 12:58:05');

-- --------------------------------------------------------

--
-- Structure de la table `kpis`
--

CREATE TABLE `kpis` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `short_label` varchar(45) DEFAULT NULL,
  `companies_id` int(10) UNSIGNED NOT NULL,
  `default_val` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `kpis`
--

INSERT INTO `kpis` (`id`, `code`, `label`, `short_label`, `companies_id`, `default_val`, `created_at`, `updated_at`, `status`) VALUES
(1, 'NBVU', 'Nombre de vues ', NULL, 1, NULL, '2021-06-28 14:27:25', '2021-06-28 14:27:25', 1),
(2, 'TEGM', 'Nombre de publications ', NULL, 1, NULL, '2021-06-28 14:28:17', '2021-06-28 14:28:17', 1),
(3, 'NBIP', 'Nombre des partages ', NULL, 1, NULL, '2021-06-28 14:28:45', '2021-06-28 14:28:45', 1),
(4, 'NBCM', 'Nombre de commentaires ', NULL, 1, NULL, '2021-07-07 11:05:20', '2021-07-07 11:05:20', 1),
(5, 'NBLK', 'Nombre de likes ', NULL, 1, NULL, '2021-07-07 11:06:05', '2021-07-07 11:06:05', 1);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`id`, `campaign_id`, `text`, `status`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 1, 'Test message à faire valider  ', 1, '2021-06-30 13:11:26', '2021-06-30 13:11:26', 1),
(2, 1, 'Merci de réagir sur  la propagande menée par Orange contre MTN.  Lien : https://www.facebook.com/MTNCAMEROON', 1, '2021-06-30 13:13:24', '2021-06-30 13:13:24', 1);

-- --------------------------------------------------------

--
-- Structure de la table `message_groupe_push`
--

CREATE TABLE `message_groupe_push` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `push_text` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `message_groupe_push`
--

INSERT INTO `message_groupe_push` (`id`, `message_id`, `group_id`, `push_text`, `created_by`, `created_at`) VALUES
(1, 2, 1, 'test', 1, '2021-07-01 10:36:18'),
(2, 2, 1, 'ffrrr', 1, '2021-07-01 10:37:13'),
(3, 2, 1, 'pomoin', 1, '2021-07-01 10:39:23');

-- --------------------------------------------------------

--
-- Structure de la table `message_user`
--

CREATE TABLE `message_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `read_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  `influenceur_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `message_user`
--

INSERT INTO `message_user` (`id`, `message_id`, `user_id`, `read_at`, `created_at`, `updated_at`, `status`, `influenceur_id`) VALUES
(1, 2, 1, NULL, '2021-07-01 09:39:23', '2021-07-01 09:39:23', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `platforms`
--

CREATE TABLE `platforms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `platforms`
--

INSERT INTO `platforms` (`id`, `name`, `status`, `created_at`, `updated_at`, `icon`) VALUES
(1, 'Facebook ', 1, '2021-06-30 08:56:17', '2021-06-30 08:56:17', 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGhlaWdodD0iMTAwJSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyOyIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgd2lkdGg9IjEwMCUiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2VyaWY9Imh0dHA6Ly93d3cuc2VyaWYuY29tLyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxwYXRoIGQ9Ik00NDkuNDQ2LDBjMzQuNTI1LDAgNjIuNTU0LDI4LjAzIDYyLjU1NCw2Mi41NTRsMCwzODYuODkyYzAsMzQuNTI0IC0yOC4wMyw2Mi41NTQgLTYyLjU1NCw2Mi41NTRsLTEwNi40NjgsMGwwLC0xOTIuOTE1bDY2LjYsMGwxMi42NzIsLTgyLjYyMWwtNzkuMjcyLDBsMCwtNTMuNjE3YzAsLTIyLjYwMyAxMS4wNzMsLTQ0LjYzNiA0Ni41OCwtNDQuNjM2bDM2LjA0MiwwbDAsLTcwLjM0YzAsMCAtMzIuNzEsLTUuNTgyIC02My45ODIsLTUuNTgyYy02NS4yODgsMCAtMTA3Ljk2LDM5LjU2OSAtMTA3Ljk2LDExMS4yMDRsMCw2Mi45NzFsLTcyLjU3MywwbDAsODIuNjIxbDcyLjU3MywwbDAsMTkyLjkxNWwtMTkxLjEwNCwwYy0zNC41MjQsMCAtNjIuNTU0LC0yOC4wMyAtNjIuNTU0LC02Mi41NTRsMCwtMzg2Ljg5MmMwLC0zNC41MjQgMjguMDI5LC02Mi41NTQgNjIuNTU0LC02Mi41NTRsMzg2Ljg5MiwwWiIgc3R5bGU9ImZpbGw6IzE3NzdmMjsiLz48L3N2Zz4='),
(2, 'YouTube', 1, '2021-06-30 08:56:24', '2021-06-30 08:56:24', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAEvUlEQVR4Xu1bW28bRRg936ztFKcJiZOUAsW2nE1bKYAEiAZ+AbdXkFCF4AUkeIUHkJCQuFbqRQgBDZeKQuGplbiX9hc0QdBUFEFJYhySiDStL0kotuN4dz60C2scaIode9pdvPuS2NrvnDNnvpndmflMaPGLWrz98A3wM6DFHfCHQIsngD8J+kOgGUMgl0hcbZpt4aAsh8vEXQFNtJsmhRxsQUaIWbRXcRGBu6q5GbQIgJ3viGRecmDF+axpvGKYMh9kWiyLYEHTSoVIKrXUqP41M4AByiQSA8IQNwMYBOF6JrmZpLgGxL1g2gjiMEAbGxXRWDz/DqYCyP6bYSHPEYt5MH4F8IMMyNO9qdQkVZlbzfcvA6bi8Q2dCDwBxuMAtjYmzjXREyAMRwIYpmSytKYBuUQiyoY4AsIO10hvohBmPhWAdn/XzESqMtScf+b7+zcFDZwEaEsTOV0HxYzZthBu60gm05a4yhDIRPXjRLjLdYoVCJLA8b7p5D0VAzLxgSFiHlXA5VpIlnJH72zqGzsD0jH9XQE86lq1CoRJ4EDfdPIx24BcTJ9kQFfA415IxmTPTHIrLUaj3QaFslQ1H7hXdfOUMcAaGRHKxvQ7AZxoHrR3kFiIIcpF9YeY8KF3ZDdPKRHtpFxcf4YZu5oH6x0kYnqa0jF9nwCeVCk7cOMgZG4Bcm5OJc06sHmPlQEfMOPhdUTXHNK280GEn3sWpYOHUHxjPzifrzlW8Y0HKRsdOArie1USWQa0v/KiTSHnz6G4Zx9KH38KcGX1q5J+TWwm+QVlo/rXqhc/1QY4aozvTqPw/Eswxk5dkcb/2Rs8ahlwBoTtKlVczACbjxmlTz5DcdduyLS9Nrm8F/MZykT7p4gorpJ5TQP+IuVCEcvvHMDy8Nvg0qrlukpZkJBTVgacBWGzSqb/MsDhNn+ZtueHlaPHVMqpwuY5ysT0HAHdKhlrNcDRUD4xgsILL8P8aVylLDA4a70KW8+ksEqmeg2wtRgmSoePoLD3VXAup0iezFsZYBCgKWKwYddlgDM/LC2h+NrrWD70kW1KMy8GG74B/hBo+Umw5R+D/otQi78KZ2OJUUAMNfPx8k8sty6GmDFC2Vj/lwDdd7kMcNNymAifW/sB74P4EdUG/L0h8iY4X1BJVzu2xHuUvUHfC4Gnao+q/063bokx8W5/U7Tlt8Wzcf0OMEbqT2zvR1gHpP7RmNWP2ahulZAMeL9P62gB83jPzM/b/eNxy7OFmH6LBMbq8M/zt0opb++bTX1bKZFJx/RjArjb8y2rpQFMX/XMTNpvvxUDlrYMRspi+aTqLfJa9Km8xyqSChni1s65icwqA6wPdpmcicOqF0cqG3gpbALGBIsHLlom5wT+bwslmfZHQvzWJQslq92rlMqa2k1gHmTwtULQdWzyJhboI6Z2gMMg6rxSPWrzMv8GQp6BApjOC+C8BJ8lEtZZ/I9SM7+vq1R2PY1Jb9vWIVdWwm1msL0Ms1sDNkhoVzlYJMwAJHVUYxN41WEMgxZWcQu+wFIznO8EzKJJVAyyWCxp5bwIhQp94+MX1qN3tY5GETwe7/9ewOMd2LB8PwMattDjAH4GeLwDG5bvZ0DDFnoc4A8LaULP6gB1GgAAAABJRU5ErkJggg=='),
(3, 'Twitter', 1, '2021-06-30 08:56:44', '2021-06-30 08:56:44', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAezklEQVR4Xu2dCZQVxdmG3+q7zcqwQ4QBFBEBBRExUdSI+XElGiXuYVMTsvzGNfprYiTGkxOjJnrM5gpDosaYxA0TETdEUREVNxaVbQaUnVmYuXPvndv1n69hzDDM0nede7veOoczzJnq6vqer/rt6lq+Ushw6jevrm9I4wQbGKW0fShgHaKhewDoDqVLFFQgw1Vg8SSQswQ0dAxa7QZQraB2QWG1VlgFGx/HLCzeMq10ayYrrzJRePm86qMA6yLY9iRAjYJSGblPJurOMkkgZwhorWHpj+y4tdDy2Y9WTeu+LN11S9uD2fvBbaWFvtAsG/oSS6kR6a4oyyMB0wlorVcoSz0UjkXu235pn7p08EhZAAY+UNPT8uMKG+pypSBdeyYSIIHMEtipoO+BX99deXH3XancKnkB0FoNqqidagO3K6X6plIJXksCJJAUARGCWyrXdbsHs5WdTAlJCcDAv9QcrGzMBdSEZG7Ka0iABNJHwNb6NQt6RtWM7msSLTVhARhYUXs2gIeUjOIzkQAJ5AQBrVGrlPpe1fTSxxKpkHsBmK2tAYPrbrcsXJ3IDZiXBEggqwTuqJpWeh2U0m7u6k4AXtb+8sq6+wDMdFMo85AACXQdAaXUw33CJTPfmaVindWiUwEYd68ObCmse0JpnNFZYfw7CZBAbhDQGvP7RUrP6UwEOhYArVX53Lo5sDA9N8xiLUiABNwTUI9UrSuZ2tEMQYcCMGBO7Z385nePmzlJINcIKG3/pnJG9+vbq1e7AjBoXt25Wuu/55pBrA8JkEBiBLRSUzZOK/1XW1e1KQDlc6uHamW9o4CyxG7F3CRAArlGQAPVVtweV3lJ97Wt67a/AMh3/7zaxVzkk2tuZH1IIBUC9qtV08pObD09uJ8ADJxXe6nSeCCVW/FaEiCB3COglJ5WOa3sLy1rto8AyMYe5cdqKNU796rPGpEACaRCwNZ6SygSH752Vs+a5nL2EYDyippbAHVTKjfhtSRAArlMQP28anrpL/cTANnPX+ALbeCW3lx2HutGAikT2BluigxpjifwZQ+gfG7ttVC4PeXiWQAJkECuE7imanq330olWwhA9YdQ1mG5XnPWjwRIIDUCEllo44yyUV8KQPmcmvGw1NLUiuXVJEAC+ULAtq2xm2aWLHd6AOXzan8LjavypfKsJwmQQGoEFHB75fRu1+0RgLns/qeGk1eTQH4RUNDLK6eXjVUStz9o25sZuju/HMjakkBKBLTW8aDqqwZV1H1bQz+eUmG8mARIIO8IyCYhNaiiZraGujnvas8KkwAJpEhA/VyVz615FEpdkGJJvJwESCDPCEjoMDWwom6Zgh6XZ3VndUmABFIloPC2GlhRu14Bg1Mti9eTAAnkFwGtsU6VV9TuANAzv6rO2pIACaRMQOvtauDcmohSKphyYSyABEggrwho6Ij0AFwdIJBXlrGyJEACrghQAFxhYiYS8CYBCoA3/UqrSMAVAQqAK0zMRALeJEAB8KZfaRUJuCJAAXCFiZlIwJsEKADe9CutIgFXBCgArjAxEwl4kwAFwJt+pVUk4IoABcAVJmYiAW8SoAB406+0igRcEaAAuMLETCTgTQIUAG/6lVaRgCsCFABXmJiJBLxJgALgTb/SKhJwRYAC4AoTM5GANwlQALzpV1pFAq4IUABcYWImEvAmAQqAN/1Kq0jAFQEKgCtMzEQC3iRAAfCmX2kVCbgiQAFwhYmZSMCbBCgA3vQrrSIBVwQoAK4wMRMJeJMABcCbfqVVJOCKAAXAFSZmIgFvEqAAeNOvtIoEXBGgALjCxEwk4E0CFABv+pVWkYArAhQAV5iYiQS8SYAC4E2/0ioScEWAAuAKEzMlSsBSwIgeFsb29uHgMgsHdbMwqNRCWVCh0A8U+RVsDdTFNHbHNHZFNNbU2Pik2sZnNTaWbo1jRyNPrk+Ue6L5KQCJEmP+dgnIQ33qID9OGeTH1/r50COkkqYl4rByVxyLv4hj/vomfLAjnnRZvLB9AhQAto6UCYzu5cOMQwM4bbAfxf7kH/qOKrJql43HPovh8TUx1Eaz3zMQMTugWOHjnXbKvHKpAApALnkjz+pydF8fLh8dxNcP8Get5vLwP7gyigdWxlCXBSEo9CtcMiKAH4wKYspzDVhdTQHImrN5o9wkIG/C2eMLnO5+VyURgjuWRzFvddQZS0h3ko7M+cMCuHJ0CP2KlPMJMvnZhnTfpsvLYw+gy12QPxWQzv1lI4O45oigM4iXC+mdbXFc/0ajM3iYjiSDlBcMC2D68AAGllhfFnndkkb87bNYOm6RU2UYIwADii3MGhXAz5dGcsoB+VKZniGF3x5XgJMGdN1bvz1W0Tjwq3cjeGhlNGmcB3azMGN4wHnrtxa3z+s1jn9iN2Lp0Zik65iJC40RgNuPLcD5Bwcw++3UGkomnJDrZY7p7cP9Jxaif1FuvPXb4yVv6JveakTE5YRBr4I9sxbfHBLAMf19aM86eWnMXZW8uOSyf40QgPISC4u+VQy/BcQ18L1XwlhY1ZTLfsmZuh3/FT/um1iQsdH9dBv61pY4Zr4UdtYWtJV6733oJw8J4Kv9fPB1omlram2c/HS9J9/+wscIAbjtmAJcOCzwZXtoaNI4b0GYc8udPH2nD/bjnuMLEfjvp3C6n9eMlPfutjimvRh2pgvlgT+6nw9f7evDV/v7cWh3C7JIyW26eGEYi7/w7svC8wIg0zjvnVe833fdzojG+Qu8N63jtmF3lu+EA/yYc1L+PfzNdq2ttaE1MLQsefV6al0Mly9u7AxVXv/d8wIweYgffzyhsE0nbQ1rnLugAetqPTi6k0KzlG/+v51cmDfd/hRMbffSTfU2TnmmoUsWHWXCnvbK9LwA3HdiYYfz1eLobz8XhvxkAmRg7LnJxc7ct6mpSQPnLWjAsq0uRxPzGJSnBaAkIN3/EoR8HXuoss7GhQvDqNpttgjIt3HFNwqzurIvF5+d/3ujEY986r05/7ZYe1oAZAT74Ultd/9bw/iiQeOi5xsgo76mplmjgvjpuJCp5jt2//7DKH7znjlrRTwtAD86PIjrx7pv0NsbNb6zsAErdpknArJQ6qWziiCDpqamOauimL00Ajcri6V3eURvC699kd+fCZ4WgD9/vRAylZVIqolqXPpS2NmPblJ6YGIhTi5PjJWX+Pzxoyh+/W7nb37Z+XjRIQGcPsjvTDUu357f7cTTAvDGlGLImy3RJEtLr3ujEf9aa8Z34LH9ZdS/KFFMnsjfZAO3LOt4pd+hPSycUu7HaYMDGNljT3u6dVkE963I/9WBnhUAWbyy5julSTdS6Qbe9X4Ed70fddUlTPpGOXDhI5OKcNxXOhkpzYF6prsK8sn3g0VhyOrBlkn2Akj3fuIAvzODNLh035fIS5uaMPPFsCfahWcFQKazZAYg1fTE2piz26wxv3t67WKQkF1PnW7e2//fG5rws7canbBjA0osjOphOSsGj+rrw+G9fGhvKGRLg8ap8+s9E67MswIgMehe+VZxqs+/c/3KXTa+vyjsyQVDdx1XgHMO+u8y6bQAy/FCPtoZd7YPDyuznJWCbrc2yyajC55vgGxB9kryrAAc2ceHJ09L35tNNpf8ZEkjnt3gnXXhEr5r2XnFRq/4c/sgS9CRH74ahvQcvJQ8KwDyTSvftulMMi7wwIoobnsvAhkozPd07tAA7pxQkO9mZKX+MlAovvda8qwApLsH0NLxEqDyitfCzqdBPqcHJxZiksFTf259JzEIf/F251OEbsvLpXyeFQD5tnv5rPSMAbTlMOkB3Lk8gntXZCYmXaYbieyDf//8EnQLmrvwxw1j+eT70avhjMQddHP/TOfxrADIPvB30zAL0JkDZEBI1o7nW7RY2fH3jIGj/535s+XfZQbomtcbIZuDvJo8KwBBH/DpxaXthnlKp0NlMcn9K6K464MownnSWmYeGsQvjna/TDqdvPKhLNkMdOObjZ598zf7wLMCIAYuPrt4v0UcmWx8G3fbTtDRFzbm/kjxrV8twLThZk3/ufW9DPb9cpm7PQFuy8zVfJ4WgGT2AqTDUUs2x52lojLfnKvp0UlFmGDg6r/O/CG9uN8u9+aAX1u2e1oA/vfwIK5LYDdgZ40jkb/LvPGT62K4/b1oTgYbWXJO8T5x7xOxzYt5JU6kxP5/en3u997Syd/TAnDiAD/mfcNdPIB0Qm1ZlswWSLjqP32UW0Lw4QUlzkm9TMCGOtuJFJ3v07rJ+NLTAlDgA5afX+J6qWcyAN1eIwOFT6+P4Z4Po84x2F2d1k4tbXe9e1fXLZv3f2VTkxP4U7aBm5g8LQDi0D99vRBnJBgTIJMNQc4lWFDZhIrVUbyxuWvGCCREmsyQmJ5e/bzJ2dOfibMF84Wt5wXgm0P8+EM7UYG72kkSunre6hge+zSG+ixOH0rsv3VTszNF2tWMO7r/P9fEcNXr3g773Rl/zwuAhG5adu7+5wJ0Biabf5eNRrLJRAKQvLklnpU30ooLSyBsTE4ySPtjj8f978y/nhcAAXDz+BAuHRHsjEVO/F2Ck8qBFE+va8LHO+MZCzrx5pQSyDHfJqf565ucHX4mJyMEQBr64rNL8u6IKxGDFzc24YWqJry+ucn1oZduGvSCbxZjxN7wVm7yezGP9LokzoPJyQgBEAffcWwBzjs4f1e+yTz10i1xJ1iphLCSYJSpHFfdVYukculhe2b9no0+JidjBEDiui08sxgyNeiFJCHKRATe3x53wpjL58LaGtv1xpVrjgjhitH58VmUKX/JmMuVr3EQ0JgJ0MsPD+InXbQyMFONuGW5ErJqdXXcCV1WuVujqk5+7vn/9rC9T1zDM4f48fscnR3JBiu5x6Of7on3aHIypgcgTpZIwf+ZXIxDuiceKtwLjUR2KsqpyLsi2plpkBj3JqeK1THc9BYFwJgegDT2cX18ePzUIq6CM/nJ32u7aceAteVyo3oAzQAuGRHE7PHcC2+6BnjlcI9U/GikAAiw244pwIXD8ndWIBWn89o9BK5d0oi/f2bG6U/t+dxYAZD18HIclnwSMJlJ4LKXw3i+yqztv6097VkB6BFSzmBXR0mWwsp2YTkNhsk8Amf+uyHvD/dM1WueFQAJBPK1fj78/qMoXt7Y1O6SWjkc46GTCnFMf4pAqo0p364f9/hubAsbNQa+n4s8KwAtowF9vNPG3z6NYv6GpjbPdCv0K9x9XIFzECSTGQQkUMuwh+syttciXyh6VgBmjgjiF61G+mUv/pLNTc7xXsu2xvFZjf3lzjvZFnPZyCBuODIEv5nLBPKlzaalnuvrbJzwRH1aysrnQjwrAGcfFHDe6h0l2Ya7fLvtBO+UU1+3hW2Ul1q4anQIElacybsEJBKQBAMxPXlWAGR0/4k0Hg5qekPxmv1yjoOE/jY9eVYAsnUykOkNKF/tlz0AshfA9ORZARDHvnNuCfoUmh30wvQG3p795zzX4IwDmZ48LQD3TyzEKTz91vQ2vp/9shHq8Md2o87QSMAtgXhaAL4/Kogbx3HNPxVgXwISln3iU5wBECqeFgDZ9vvCmZk7IpwPVn4S+MeaGK42PBpws+c8LQBi5MvfKsbQbpzYz89HNTO1lhgAEguAyeM9AHGwRACSSEBMJNBM4IxnG/DhDg4Aev4TQAwcUGzhtXOK4eNkABUAcAb+Rj+2G7IqlMmAHoA4+cGJhZjE2QC2dwALq5pw6ctcAWjMGIAYOr6vD/88tYgPAAnglmURPLAiShJ7CXh+ELDZ0xXfKMTEAdztZ3rLP/mZeqza1fWnM+eKH4wRgFE9Lcw/g2MBudLwuqIem+ptHPNPzv+3ZG+MAIjREghUAoIymUlgzqoobl7KDUDGCoBE/3nhrCJnZoDJPAJTXwhj0edmxwBs7XWjegDNA4KPncJzAUx7/GX6b+zjuyGRgJj+S8A4ARDTuUfAvEdAwn9LGHCmfQkYKQCyJuh3xxXgnIN4LoApDwS7/2172kgBEBQS908WCHFq0PsSsKNRY/zju12fnOx9IoZ/AjSbX+RXjghM+AoDAHq50XP0v33vGtsDaEYiJwbfc3whTh/MRUJeFYHT5tdDQsMz7U/AeAFwPgcUcMO4EL47kmsEvPaQvLstjm/9p8FrZqXNHgpAC5RnHRjAr48JQdYLMHmDwDWvN+LxNdz73543KQCtyJSXWLj92AIcy6PC8l4BaqIaR/+jHuEm7v2lACTQnOX9f97BAVx7RAj9itgbSABdTmX9w4dR3PYel/525BT2ADqgI7MEl40MOPsHeoYoBDn1dHdSGVnxN+GJ3c6JT0ycBUipDRT4gClDA5g+PIhDe3AfQUows3SxfPfL9z9TxwTYA0iwhUik4TOHBPCNgT6M6OGDxY5BggQzn13i/su+/0+qOfXXGW0KwF5C8hwn0lnsW6hw0gA/fnBYEAcy6nBn7Syrf5+/vgk/fJVhv9xApwDspfT06UUoDiisq7WxPaxRF9NocHaOanQLKpTt/dc9pHBwmYUeHBNw076ynkfe/pOersenNXz7u4FPAdhLiTsE3TSX3M/z5LoYfryY3/5uPUUB2EtKuvRvTCmBLA1myk8CTfaet/+aWr793XqQAtCC1J0TCnDuUG4Rdtt4ci3fnJVR3Pw25/0T8QsFoAWt4d0tLPhmMUf2E2lBOZK3NqpxwhP12BlJZCg3RyrfhdWgALSC/7sJBc6cP1N+Ebh1WQT3Md5/wk6jALRCJgFD5UBRWfzDlB8E5LjvU+bXM95fEu6iALQBTeb2bzgylAROXpJtAtLhv/D5BizZzGifybCnALRBTQ4SffK0IozpzW5AMo0qm9c89lkMP2Gwz6SRUwDaQXdQNzlJqAglAa71Tbp1ZfhCifV30lP12MWBv6RJUwA6QHfaID/+fGIhKAFJt6+MXviDRWE8u4EHfaQCmQLQCb0fHR7E9WM5HpBKI8vEtf9aG8OVr3HFX6psKQAuCPJMQReQspjliwaNk5+uh0T8YUqNAAXABT/5BPj5+BAu5cGiLmhlNots9vnOCw147QuO+qeDNAUgAYpXjQniyjEhjgkkwCzdWe/+IIo7l3O5b7q4UgASJHnGYD9kz4CEC2PKLgGZ6794YQPi7PmnDTwFIAmUsmfg7uMLMZLhwZKgl9wlEtvv1Pn1kKk/pvQRoAAkyVK2DV81JoRZo4LcQpwkQ7eXSYDP859vwDvb+N3vlpnbfBQAt6TayScLhmSAUMKDMWWGgEz3ybQfU/oJUADSxHRcHx8uHx10Thvm6ECaoAJgbP/0sWyrJApAmvkO7WbhwkMCmHJQAL0KKAWp4P33hj3BPWXqjykzBCgAmeEK2VAkvYJJ5X4c09+HkT188DPcmGvaMuI/7cUGbvF1TSy5jBSA5LglfJXEFxjZ0+eEEB9SaqFP4Z5Iw2N6WRhYQmVoCXT59jguWhjG7hhf/Qk3tAQvoAAkCCxd2eXj4LKRe/YZBLnr+EusEs772881cIdfuhpaJ+VQALIEuuVtZGzgzmMLcNJAzhy05CKRfS5Y2MDz/LLYJikAWYQtt5LpwjsmFKA3Bwj3Ib9yl42LFjZwoU+W2yMFIEvA5dyBG8eFcPZBAU4TtmL+8U7bWeLLiL5ZaowtbkMByDBz2TIw7dAgrj0iyOhCbbCW0f7vvRKGhPVmyj4BCkAGmU/4ig+zxxdA9g4w7U/gqXUxXLOkkVN9Xdg4KAAZgD++rw9XjwlBBICpbQJyis8vlkW4yKeLGwgFII0OkAdeHnwRAKa2CcjGnp+91Yi/fca1/bnQRigAKXpBogafeaAfFw8L4PBefPA7wrm5QeP7i8J4l7v6Umx16bucApAkS3nY5aGXh5+hwzuHuGxrHLMWhbEtzMG+zmllLwcFwCVrSwFjeu1Z2y//OLDnDpxE7/njR1H87v0I5PhuptwiQAHowB+yWOeovj5ni+//DPQ76/eZ3BPYVG87obvf2sJAHu6pZTcnBWAv725BhWFlFo7o7cPYPj4c2ZubdFJpik+ui+Fnb0U4v58KxCxcm/cCMLa3DycO8DubR7aG7S8bXG0UaP7aLAsCQZ9CoQ+QB73ApzCgRKG8xMKgEuXsxpOdeUypE5C3/k/fjOClTTyxJ3WamS8h7wVAEI3u5cMtR4dwZB+Owme+ybR9BxHbRz+J4dZ3ItzG21VOSOK+nhAAsVsG6S44OICfjA0xEk8SDSGVS5ZujeOWtyP4YAe/9VPh2BXXekYAmuFJV/7qMUFcfEiQ++wz3KIq62z86t0IJHQXU34S8JwANLvhgGKFHx4WcnoFDLiR3sa5vVHjTx9FUbE6ynX86UWb9dI8KwAthWDWyCAuOiSIEIcIUmpgsl23YlUM96+I8js/JZK5c7HnBWAfIRgVxLlDA1y5l2D7k5H9B1fG8PAnMYSbuJIvQXw5nd0YAWj2QvPa/enDgxjBo706bJyyfPfBlVEsqGwCn/ucfo6TrpxxAtCS1Nf6+TBteBCnDvIzZPdeMLKeQvbpP/JpDKt2ce1u0k9WnlxotAA0+0iCdJ4+2I/JgwM4up/PielvUorZwIsbm/DPNTFnAY/8zmQGAQpAKz/Lev8zBgcweYgfR/XxOesLvJgk5v6rn8fxfFWT89BXR/ht70U/d2YTBaADQhLI8/gD/JjQ34dj+/shU4v5muTx/rTaxpLNTXhxYxxvbGniFF6+OjON9aYAJABTTvU5dq8YyLFfuSwI0o3/pDqOpVvieHNLHLJab0cj3/IJuNuIrBSAFNzcPaQwsoflHPklMwry/0O6+xDIYgxQOThTIu2sr7OxalccEmJ7xa44Vlfb3H+fgm9NuZQCkGZPywGg8ulwQLGF/kUK/YssDCje81N+D1pw1iFIPtmZKAOOLSMKyTx71Aa0hrOzMRwHdjZqJ2a+vMGdn2Eblbs1ZCluVb3NrnyafWhScRQAk7xNW0mgFQEKAJsECRhMgAJgsPNpOglQANgGSMBgAhQAg51P00mAAsA2QAIGE6AAGOx8mk4CFAC2ARIwmAAFwGDn03QSoACwDZCAwQQoAAY7n6aTAAWAbYAEDCZAATDY+TSdBCgAbAMkYDABCoDBzqfpJEABYBsgAYMJUAAMdj5NJwEKANsACRhMgAJgsPNpOglQANgGSMBgAhQAg51P00mAAsA2QAIGE6AAGOx8mk4CFAC2ARIwmAAFwGDn03QSoACwDZCAwQQoAAY7n6aTAAWAbYAEDCZAATDY+TSdBCgAbAMkYDABCoDBzqfpJKAGzq2JKKWCREECJGAWAQ0dkR7ADgA9zTKd1pIACUDr7WpgRe16BQwmDhIgAbMIaI11amBF3TIFPc4s02ktCZAAFN5W5RV1jwD6QuIgARIwi4AC/io9gJsV9GyzTKe1JEAC0OomNXBu3RSl9D+IgwRIwCwCGjhH9ZtX1zdo25uhlDLLfFpLAiYT0HY8oPo5D3353OoPoazDTMZB20nAJAJa6/c2zig70hGAAXNq77QsXG0SANpKAiYTUNr+TeWM7tfv6QHMqz4K2nrbZCC0nQRMImBb1hGbppa8/+V3Pz8DTHI/bTWZgIb+eOP0MueT/78CMK/2GmjcYTIY2k4CRhDQ+uqqGWW/20cA+vx9a0lBQ2gdlOptBAQaSQImEtDYEY5HDtx+aZ+6fQRAfhlUUTNbQ91sIhfaTAJGENDqpqoZpbc227rP3P+gh6t76Ca1GlB9jIBBI0nAIAK21ltCkfjwtbN61rQpAM6MQEXtJQAeNIgLTSUBIwho6Kkbp5f9taWx+6/+01oNqKh91VLqOCOo0EgSMIGAbS+qmlE2EUrpjgVgz8rAoVpZ7yigzAQ2tJEEvExAA9U+ZR+5YVr3da3tbHf9/6CKum9r6Me9DIa2kYDnCWittVJTNk7v9kRbtna4Aai8ovZ2ANd6HhINJAGPEtDArzdO73ZDe+Z1vAPQGQ+oe8hSmOFRPjSLBDxLQCn1cOXUkqmtv/s7HQNomWHcvTqwJVT3L6Uw2bOkaBgJeI2AxjN9I6VT3pmlYh2Z5i4GwMvaX76h5s9Q1qVe40R7SMBrBCTUV5/G0ks6e/jFbncCIDm1VoMqan6tlXWd14DRHhLwBIE9A363bZxWemNH3f6EPgFagymvqD1La8xRCj08AY1GkIAHCGiNWstSl1VOK01o5s59D6AFpMHzqg+0NeYC1gkeYEcTSCC/Cdj2Ih/0jPUze6xP1JCkBKD5JoPm1Z0bt+17LKX6JXpj5icBEkiNgIbebGlcXzm921/cdvlb3zElAZDCDrp3Z1msIPBjaH0FFHqlZhKvJgES6JSAxg5A3RWIxO5pubGn0+vayJCyADSXuTeewHcBfQkDjCbjCl5DAp0Q0PZHgHqosShy/7bz+u5OB6+0CUDLygyu2DVWw3choCdpYDSgrHRUlmWQgFkEtK2ADwC1MG5bj2yaWbI83fZnRABaVvKAR2p7WzEcr7QaBYURWutDYKGn0uiutS7h0eTpdinLyycCWuuoUmq3VqiGjZ2WpVZrG6u00h/bASz+/KJu2zNpz/8DW4+W91EZsPgAAAAASUVORK5CYII='),
(4, 'Instagram', 1, '2021-07-07 15:34:41', '2021-07-07 15:34:41', 'https://fr.wikipedia.org/wiki/Instagram#/media/Fichier:Instagram_logo_2016.svg'),
(5, 'TikTok', 1, '2021-07-07 15:35:11', '2021-07-07 15:35:11', 'https://cdn.worldvectorlogo.com/logos/tiktok-logo.svg'),
(6, 'LinkedIn', 1, '2021-07-07 15:35:11', '2021-07-07 15:35:11', 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Linkedin_icon.svg/256px-Linkedin_icon.svg.png');

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `status`, `created_at`, `company_id`, `images`) VALUES
(1, 'MTN YaMo', 1, '2021-07-09 13:52:08', 1, NULL),
(2, 'Ayoba', 1, '2021-07-09 13:52:24', 1, NULL),
(3, 'Yabadoo', 1, '2021-07-09 13:52:49', 1, NULL),
(4, 'MTN Home', 1, '2021-07-09 13:56:53', 1, NULL),
(5, 'MTN Prestige', 1, '2021-07-09 13:58:05', 1, NULL),
(6, 'MTN Business', 1, '2021-07-09 13:58:22', 1, NULL),
(7, 'MTN Plus', 1, '2021-07-09 13:58:46', 1, NULL),
(8, 'MTN Magic', 1, '2021-07-09 13:59:07', 1, NULL),
(9, 'Wanda Net', 1, '2021-07-09 14:07:16', 1, NULL),
(10, 'MTN MoMo', 1, '2021-07-15 12:54:34', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sc_log`
--

CREATE TABLE `sc_log` (
  `id` int(8) NOT NULL,
  `inserted_date` datetime DEFAULT NULL,
  `username` varchar(90) NOT NULL,
  `application` varchar(200) NOT NULL,
  `creator` varchar(30) NOT NULL,
  `ip_user` varchar(32) NOT NULL,
  `action` varchar(30) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sc_log`
--

INSERT INTO `sc_log` (`id`, `inserted_date`, `username`, `application`, `creator`, `ip_user`, `action`, `description`) VALUES
(1, '2021-07-02 09:08:30', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(2, '2021-07-02 09:08:39', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(3, '2021-07-02 09:08:39', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(4, '2021-07-02 09:14:47', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(5, '2021-07-02 09:14:50', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(6, '2021-07-02 09:14:52', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(7, '2021-07-02 09:38:51', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(8, '2021-07-02 11:04:58', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(9, '2021-07-02 11:06:03', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(10, '2021-07-02 11:06:20', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(11, '2021-07-02 11:07:57', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(12, '2021-07-02 11:08:01', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(13, '2021-07-02 11:08:01', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(14, '2021-07-02 11:08:09', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(15, '2021-07-02 11:08:12', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(16, '2021-07-02 11:14:17', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(17, '2021-07-02 11:14:19', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(18, '2021-07-02 11:14:19', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(19, '2021-07-02 11:14:24', 'admin', 'form_categories', 'Scriptcase', '127.0.0.1', 'access', ''),
(20, '2021-07-02 11:17:09', 'admin', 'form_companies', 'Scriptcase', '127.0.0.1', 'access', ''),
(21, '2021-07-02 11:18:05', 'admin', 'form_companies', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 2||--> fields <-- name (new)  : MUTZIG||email (new)  : cbitom@sabc-cm.com||phone (new)  : 666666666||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(22, '2021-07-02 11:18:37', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(23, '2021-07-02 11:18:40', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(24, '2021-07-02 11:21:14', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(25, '2021-07-02 11:21:18', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(26, '2021-07-02 11:21:18', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(27, '2021-07-02 11:21:28', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(28, '2021-07-02 11:21:30', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(29, '2021-07-02 11:22:55', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(30, '2021-07-02 11:22:58', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(31, '2021-07-02 11:22:58', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(32, '2021-07-02 11:23:02', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(33, '2021-07-02 11:23:05', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(34, '2021-07-02 11:24:45', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- code : CMANAGER||--> fields <-- label (new)  : Compagni manager ||description (new)  : Compagni manager ||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null'),
(35, '2021-07-02 11:26:25', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(36, '2021-07-02 11:26:27', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(37, '2021-07-02 11:26:27', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(38, '2021-07-02 11:29:14', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(39, '2021-07-02 11:29:16', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(40, '2021-07-02 11:29:25', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(41, '2021-07-02 11:31:34', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(42, '2021-07-02 11:31:36', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(43, '2021-07-02 11:31:36', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(44, '2021-07-02 11:31:40', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(45, '2021-07-02 11:31:43', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(46, '2021-07-02 11:32:54', 'admin', 'sec_form_sec_groups', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- code : CMANAGER||--> fields <-- label (new)  : Compagni manager ||description (new)  : Compagni manager ||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null'),
(47, '2021-07-02 11:43:21', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(48, '2021-07-02 11:48:45', 'admin', 'form_influencer_managers', 'Scriptcase', '127.0.0.1', 'access', ''),
(49, '2021-07-02 11:51:33', 'admin', 'form_influencer_managers', 'Scriptcase', '127.0.0.1', 'access', ''),
(50, '2021-07-02 11:51:59', 'admin', 'form_influencer_managers', 'Scriptcase', '127.0.0.1', 'access', ''),
(51, '2021-07-02 11:52:52', 'admin', 'form_influencer_managers', 'Scriptcase', '127.0.0.1', 'access', ''),
(52, '2021-07-02 11:57:35', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(53, '2021-07-02 11:57:37', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(54, '2021-07-02 11:57:38', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(55, '2021-07-02 11:57:54', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(56, '2021-07-02 11:57:57', 'admin', 'form_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(57, '2021-07-02 11:57:57', 'admin', 'form_influencer_managers', 'Scriptcase', '127.0.0.1', 'access', ''),
(58, '2021-07-02 11:58:05', 'admin', 'form_influencer_managers', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 1||--> fields <-- user_id (new)  : 1||influenceur_id (new)  : 1||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(59, '2021-07-02 11:59:16', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(60, '2021-07-02 11:59:22', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(61, '2021-07-02 12:00:41', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- login : mtn||--> fields <-- password (new)  : 4f7ab0b3d2281cb27d1946daaec20a71||name (new)  :  test mtn||email (new)  : testmtn@tst.com||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||groups (new)  : CMANAGER'),
(62, '2021-07-02 14:10:40', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(63, '2021-07-02 14:10:52', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(64, '2021-07-02 14:10:52', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(65, '2021-07-05 11:41:15', '', 'sec_Login', 'Scriptcase', '154.72.169.146', 'access', ''),
(66, '2021-07-06 12:17:00', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(67, '2021-07-06 12:17:17', '', 'sec_Login', 'User', '154.72.168.5', 'login Fail', 'Quelqu\'un a essayÃ© de se connecter avec cet utilisateuradmin'),
(68, '2021-07-06 12:17:34', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'ConnectÃ© avec succÃ¨s dans le systÃ¨me !'),
(69, '2021-07-06 12:17:35', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(70, '2021-07-06 12:17:46', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(71, '2021-07-06 12:21:14', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(72, '2021-07-06 12:21:25', 'admin', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(73, '2021-07-06 12:22:23', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(74, '2021-07-06 12:24:35', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(75, '2021-07-06 12:24:44', 'admin', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(76, '2021-07-06 12:24:56', 'admin', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(77, '2021-07-06 12:26:11', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(78, '2021-07-06 12:26:44', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(79, '2021-07-06 12:26:51', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(80, '2021-07-06 13:29:20', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(81, '2021-07-06 13:29:43', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'ConnectÃ© avec succÃ¨s dans le systÃ¨me !'),
(82, '2021-07-06 13:29:44', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(83, '2021-07-06 13:29:47', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(84, '2021-07-06 13:29:48', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(85, '2021-07-06 13:30:03', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(86, '2021-07-06 13:31:07', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(87, '2021-07-06 13:31:29', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'ConnectÃ© avec succÃ¨s dans le systÃ¨me !'),
(88, '2021-07-06 13:31:29', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(89, '2021-07-06 13:31:35', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(90, '2021-07-06 13:31:37', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(91, '2021-07-06 13:32:02', 'admin', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(92, '2021-07-06 13:32:17', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(93, '2021-07-06 13:32:19', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(94, '2021-07-06 13:34:37', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(95, '2021-07-06 13:35:09', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'ConnectÃ© avec succÃ¨s dans le systÃ¨me !'),
(96, '2021-07-06 13:35:11', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(97, '2021-07-06 13:35:14', 'admin', 'grid_messages', 'Scriptcase', '154.72.168.5', 'access', ''),
(98, '2021-07-06 13:35:15', 'admin', 'grid_message_groupe_push', 'Scriptcase', '154.72.168.5', 'access', ''),
(99, '2021-07-06 13:35:16', 'admin', 'grid_message_user', 'Scriptcase', '154.72.168.5', 'access', ''),
(100, '2021-07-06 13:35:17', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(101, '2021-07-06 13:35:22', 'admin', 'form_companies', 'Scriptcase', '154.72.168.5', 'access', ''),
(102, '2021-07-06 13:35:26', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(103, '2021-07-06 13:35:28', 'admin', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'access', ''),
(104, '2021-07-06 13:35:33', 'admin', 'form_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(105, '2021-07-06 13:35:40', 'admin', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(106, '2021-07-06 13:35:44', 'admin', 'form_platforms', 'Scriptcase', '154.72.168.5', 'access', ''),
(107, '2021-07-06 13:35:46', 'admin', 'form_categories', 'Scriptcase', '154.72.168.5', 'access', ''),
(108, '2021-07-06 13:35:50', 'admin', 'form_messages', 'Scriptcase', '154.72.168.5', 'access', ''),
(109, '2021-07-06 13:39:04', 'admin', 'grid_message_groupe_push', 'Scriptcase', '154.72.168.5', 'access', ''),
(110, '2021-07-06 13:39:23', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(111, '2021-07-06 13:39:46', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(112, '2021-07-06 13:39:47', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(113, '2021-07-06 13:39:56', 'admin', 'grid_messages', 'Scriptcase', '154.72.168.5', 'access', ''),
(114, '2021-07-06 16:16:10', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(115, '2021-07-06 16:16:19', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(116, '2021-07-06 16:16:50', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(117, '2021-07-06 16:16:52', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(118, '2021-07-06 16:16:59', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(119, '2021-07-06 16:17:05', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(120, '2021-07-06 16:17:07', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(121, '2021-07-06 16:17:46', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(122, '2021-07-06 16:58:25', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(123, '2021-07-06 16:58:45', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(124, '2021-07-06 16:58:46', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(125, '2021-07-06 16:58:55', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(126, '2021-07-06 16:59:00', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(127, '2021-07-06 17:00:42', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- login : navarro||--> fields <-- password (new)  : 5725b81ce75933009d7a21ef242c42d1||name (new)  : Navaro||email (new)  : vnavarro@opensolutions-it.com ||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||groups (new)  : ADMIN'),
(128, '2021-07-06 17:01:02', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(129, '2021-07-06 17:31:55', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(130, '2021-07-06 17:31:59', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(131, '2021-07-06 17:57:32', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(132, '2021-07-06 17:57:56', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(133, '2021-07-06 17:58:16', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(134, '2021-07-06 17:58:16', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(135, '2021-07-06 17:58:25', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(136, '2021-07-06 17:58:32', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(137, '2021-07-06 18:01:21', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(138, '2021-07-06 18:01:25', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(139, '2021-07-06 18:08:11', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(140, '2021-07-06 18:08:25', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(141, '2021-07-06 18:09:07', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 0||login : brice||--> fields <-- company_id (new)  : 1||type (new)  : ADMIN||name (new)  : Brice||full_name (new)  : navarro||email (new)  : vnavarro@opensolutions-it.com||phone (new)  : 699990838||password (new)  : aa1bf4646de67fd9086cf6c79007026c||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : ADMIN'),
(142, '2021-07-06 18:09:43', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(143, '2021-07-06 18:09:48', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(144, '2021-07-06 18:09:54', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(145, '2021-07-06 18:11:03', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(146, '2021-07-06 18:11:06', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(147, '2021-07-06 18:12:17', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(148, '2021-07-06 18:12:21', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(149, '2021-07-06 18:14:56', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 0||login : Anïs||--> fields <-- company_id (new)  : 1||type (new)  : ADMIN||name (new)  : Anaïs Ndédi||full_name (new)  : Anaïs Ndédi||email (new)  : andedi@opensolutions-it.com||phone (new)  : 666666666||password (new)  : 06001e417180d3f0413b10948e58adce||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : ADMIN'),
(150, '2021-07-06 18:15:12', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(151, '2021-07-06 18:16:18', 'admin', 'sec_grid_sec_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(152, '2021-07-06 18:16:21', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'access', ''),
(153, '2021-07-06 18:18:13', 'admin', 'sec_form_edit_users', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 0||login : navarro21||--> fields <-- company_id (new)  : 1||type (new)  : ADMIN||name (new)  : Navaro ||full_name (new)  : Navaro ||email (new)  : vnavarro@opensolutions-it.com||phone (new)  : 659476961||password (new)  : 6e4c8b2acece4b4e0682787f6c25542a||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : ADMIN'),
(154, '2021-07-07 08:31:08', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(155, '2021-07-07 08:32:33', 'navarro21 ', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(156, '2021-07-07 08:32:34', 'navarro21 ', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(157, '2021-07-07 08:36:05', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(158, '2021-07-07 08:36:24', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(159, '2021-07-07 08:37:18', 'navarro21 ', 'form_categories', 'Scriptcase', '154.72.168.5', 'access', ''),
(160, '2021-07-07 08:39:27', 'navarro21 ', 'form_categories', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 1||--> fields <-- name (old)  : Gros Influenceurs ||name (new)  : macro influenceur '),
(161, '2021-07-07 08:40:49', 'navarro21 ', 'form_categories', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 2||--> fields <-- name (new)  : micro influenceur ||type (new)  : MIF||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(162, '2021-07-07 08:41:40', 'navarro21 ', 'form_categories', 'Scriptcase', '154.72.168.5', 'access', ''),
(163, '2021-07-07 08:41:57', '', 'sec_Login', 'Scriptcase', '41.244.244.6', 'access', ''),
(164, '2021-07-07 08:42:41', 'navarro21 ', 'form_categories', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 3||--> fields <-- name (new)  : nano influenceur ||type (new)  : NI||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(165, '2021-07-07 08:43:01', 'navarro21 ', 'form_categories', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 3||--> fields <-- type (old)  : NI||type (new)  : NIF'),
(166, '2021-07-07 08:43:10', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(167, '2021-07-07 08:44:34', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(168, '2021-07-07 08:46:27', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 2||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Muriel Blanche ||full_name (new)  : Muriel Blanche ||email (new)  : murielblanche@os.com||phone (new)  : 668688885885||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(169, '2021-07-07 08:46:44', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(170, '2021-07-07 08:47:05', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(171, '2021-07-07 08:47:30', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(172, '2021-07-07 08:48:03', '', 'sec_Login', 'Scriptcase', '41.244.244.4', 'access', ''),
(173, '2021-07-07 09:14:11', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 3||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Syndy Emane ||full_name (new)  : Syndy Emane ||email (new)  : synemane@gmail.com ||phone (new)  : 780976544||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(174, '2021-07-07 09:14:40', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(175, '2021-07-07 09:17:59', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Tenor ||full_name (new)  : Tenor ||email (new)  : tenor@gmail.com||phone (new)  : 756594990||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(176, '2021-07-07 09:18:30', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(177, '2021-07-07 09:20:35', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 5||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Minks ||full_name (new)  : Minks ||email (new)  : minnkss@gmail.com||phone (new)  : 0898676||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(178, '2021-07-07 09:21:30', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(179, '2021-07-07 09:24:35', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(180, '2021-07-07 09:27:19', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(181, '2021-07-07 09:28:36', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(182, '2021-07-07 09:32:33', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 6||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Ko-C||full_name (new)  : Ko-C||email (new)  : koc@kocc.os ||phone (new)  : 0987765||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(183, '2021-07-07 09:33:01', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(184, '2021-07-07 09:33:29', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(185, '2021-07-07 09:33:30', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(186, '2021-07-07 09:33:47', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(187, '2021-07-07 09:37:01', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 7||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Flavien Kouatcha ||full_name (new)  : Flavien Kouatcha ||email (new)  : flavien@koua.os||phone (new)  : 97765444||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(188, '2021-07-07 09:37:16', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(189, '2021-07-07 09:38:36', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 8||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Philippe Simo ||full_name (new)  : Philippe Simo ||email (new)  : philipp@simo.os||phone (new)  : 233644||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(190, '2021-07-07 09:38:53', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(191, '2021-07-07 09:40:39', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 9||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Claudel Noubissi ||full_name (new)  : Claudel Noubissi ||email (new)  : claudel@noub.os ||phone (new)  : 111111111111||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(192, '2021-07-07 09:40:58', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(193, '2021-07-07 09:44:04', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 10||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Frieda Choco ||full_name (new)  : Frieda Choco ||email (new)  : frifri@cho.os||phone (new)  : 0889885||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(194, '2021-07-07 09:45:05', 'navarro21 ', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(195, '2021-07-07 09:45:48', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(196, '2021-07-07 09:46:12', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(197, '2021-07-07 10:00:05', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(198, '2021-07-07 10:02:05', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(199, '2021-07-07 10:03:59', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 11||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Ulrich Takam ||full_name (new)  : Ulrich Takam ||email (new)  : ulrich@tak.os||phone (new)  : 694847864||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(200, '2021-07-07 10:04:20', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(201, '2021-07-07 10:06:10', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 12||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Atome ||full_name (new)  : Atome ||email (new)  : at@me.os ||phone (new)  : 765378958||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(202, '2021-07-07 10:06:24', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(203, '2021-07-07 10:06:34', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(204, '2021-07-07 10:07:03', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(205, '2021-07-07 10:08:42', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 13||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Cabrel Nanjip ||full_name (new)  : Cabrel Nanjip ||email (new)  : cabrel@nanjip.os||phone (new)  : 8887463||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(206, '2021-07-07 10:09:01', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(207, '2021-07-07 10:33:05', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 14||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Caprice Audrey ||full_name (new)  : Caprice Audrey ||email (new)  : caprice@audrey.os||phone (new)  : 58698608||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(208, '2021-07-07 10:33:25', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(209, '2021-07-07 10:33:30', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(210, '2021-07-07 10:55:49', 'navarro21 ', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(211, '2021-07-07 10:55:56', 'navarro21 ', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(212, '2021-07-07 10:57:10', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(213, '2021-07-07 10:57:36', 'navarro21 ', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(214, '2021-07-07 10:57:59', 'navarro21 ', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(215, '2021-07-07 10:58:16', 'navarro21 ', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(216, '2021-07-07 10:58:27', 'navarro21 ', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(217, '2021-07-07 10:58:34', 'navarro21 ', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(218, '2021-07-07 11:00:27', 'navarro21 ', 'form_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(219, '2021-07-07 11:02:30', 'navarro21 ', 'form_kpis', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 2||--> fields <-- label (old)  : Taux d\'engagements ||label (new)  : nombre de publications '),
(220, '2021-07-07 11:03:00', 'navarro21 ', 'form_kpis', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 2||--> fields <-- label (old)  : nombre de publications ||label (new)  : Nombre de publications '),
(221, '2021-07-07 11:03:42', 'navarro21 ', 'form_kpis', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 3||--> fields <-- label (old)  : Nombre d\'impression||label (new)  : Nombre des partages '),
(222, '2021-07-07 11:05:20', 'navarro21 ', 'form_kpis', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- code (new)  : NBCM||label (new)  : Nombre de commentaires ||companies_id (new)  : 1||created_at (new)  : ||updated_at (new)  : ||status (new)  : 1'),
(223, '2021-07-07 11:06:05', 'navarro21 ', 'form_kpis', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 5||--> fields <-- code (new)  : NBLK||label (new)  : Nombre de likes ||companies_id (new)  : 1||created_at (new)  : ||updated_at (new)  : ||status (new)  : 1'),
(224, '2021-07-07 11:06:48', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(225, '2021-07-07 11:07:57', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(226, '2021-07-07 11:08:40', '', 'sec_Login', 'Scriptcase', '41.244.244.5', 'access', ''),
(227, '2021-07-07 11:08:40', '', 'sec_Login', 'Scriptcase', '41.244.244.4', 'access', ''),
(228, '2021-07-07 11:09:14', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(229, '2021-07-07 11:09:15', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(230, '2021-07-07 11:10:30', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(231, '2021-07-07 11:12:59', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 2||--> fields <-- company_id (new)  : 1||name (new)  : Magic Dubaï||start_date (new)  : 2021-05-01 00:00:00||end_date (new)  : 2022-07-01 00:00:00||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(232, '2021-07-07 11:14:01', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 1||--> fields <-- name (old)  : MTN Imbattable||name (new)  : Default Campagn'),
(233, '2021-07-07 11:15:37', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(234, '2021-07-07 11:15:54', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 3||--> fields <-- company_id (new)  : 1||name (new)  : MoMo Bonus Imbattable||start_date (new)  : 2021-06-15 00:00:00||end_date (new)  : 2021-07-15 00:00:00||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(235, '2021-07-07 11:16:47', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 15||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Dr Ni ||full_name (new)  : Dr Ni ||email (new)  : dr@ni.os||phone (new)  : 3352634||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(236, '2021-07-07 11:17:01', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(237, '2021-07-07 11:19:26', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- company_id (new)  : 1||name (new)  : Ayoba||start_date (new)  : 2021-01-01 00:00:00||end_date (new)  : 2026-07-01 00:00:00||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(238, '2021-07-07 11:19:32', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 16||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Filantrio ||full_name (new)  : Filantrio ||email (new)  : flan@trio.os||phone (new)  : 4335657||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(239, '2021-07-07 11:19:49', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(240, '2021-07-07 11:21:29', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 5||--> fields <-- company_id (new)  : 1||name (new)  : YaMo||start_date (new)  : 2021-07-01 00:00:00||end_date (new)  : 2021-12-01 00:00:00||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(241, '2021-07-07 11:21:38', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 17||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Annie Payep ||full_name (new)  : Annie Payep ||email (new)  : annie@payep.os ||phone (new)  : 5445787879||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(242, '2021-07-07 11:22:00', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(243, '2021-07-07 11:22:19', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 6||--> fields <-- company_id (new)  : 1||name (new)  : Remittence||start_date (new)  : 2021-05-01 00:00:00||end_date (new)  : 2021-12-31 00:00:00||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(244, '2021-07-07 11:23:02', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 7||--> fields <-- company_id (new)  : 1||name (new)  : Home||start_date (new)  : 2021-05-01 00:00:00||end_date (new)  : 2021-12-31 00:00:00||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(245, '2021-07-07 11:23:28', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'access', ''),
(246, '2021-07-07 11:23:48', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 18||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Chouchou Mpacko ||full_name (new)  : Chouchou Mpacko ||email (new)  : chou@mpack.os ||phone (new)  : 5678933||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(247, '2021-07-07 11:23:48', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'access', ''),
(248, '2021-07-07 11:23:59', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(249, '2021-07-07 11:24:03', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(250, '2021-07-07 11:25:27', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 19||--> fields <-- company_id (new)  : 1||category_id (new)  : 2||name (new)  : Dr Selfie ||full_name (new)  : Dr Selfie ||email (new)  : dr@selfie.os||phone (new)  : 6536654||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(251, '2021-07-07 11:25:38', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(252, '2021-07-07 11:25:50', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(253, '2021-07-07 11:27:02', 'navarro21 ', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(254, '2021-07-07 11:27:24', 'navarro21 ', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(255, '2021-07-07 11:27:39', 'navarro21 ', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(256, '2021-07-07 11:27:40', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 6||--> fields <-- name (new)  : Live||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(257, '2021-07-07 11:28:03', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(258, '2021-07-07 11:29:39', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(259, '2021-07-07 11:30:00', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(260, '2021-07-07 11:32:46', 'Anïs', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(261, '2021-07-07 11:34:14', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(262, '2021-07-07 11:34:31', 'Anïs', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(263, '2021-07-07 11:34:50', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(264, '2021-07-07 11:34:58', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(265, '2021-07-07 11:35:04', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(266, '2021-07-07 11:35:59', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(267, '2021-07-07 11:36:35', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(268, '2021-07-07 11:37:11', 'Anïs', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(269, '2021-07-07 11:37:33', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(270, '2021-07-07 11:37:37', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(271, '2021-07-07 11:37:49', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(272, '2021-07-07 11:38:25', 'Anïs', 'form_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(273, '2021-07-07 11:41:46', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 2||--> fields <-- contract_id (new)  : 1||campaign_id (new)  : 4||status (new)  : 1||start_date (new)  : 2021-07-07||end_date (new)  : 2026-07-01||created_at (new)  : ||updated_at (new)  : '),
(274, '2021-07-07 11:42:11', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 2||--> fields <-- values (old)  : 1000||values (new)  : 250'),
(275, '2021-07-07 11:42:59', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(276, '2021-07-07 11:44:07', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 3||--> fields <-- kpi_id (new)  : 1||campaign_contract_id (new)  : 1||values (new)  : 250||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(277, '2021-07-07 12:17:53', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(278, '2021-07-07 12:18:02', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(279, '2021-07-07 12:18:03', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(280, '2021-07-07 12:23:38', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(281, '2021-07-07 12:24:11', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(282, '2021-07-07 12:24:11', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(283, '2021-07-07 12:24:41', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(284, '2021-07-07 12:24:42', 'admin', 'grid_message_groupe_push', 'Scriptcase', '154.72.168.5', 'access', ''),
(285, '2021-07-07 12:24:43', 'admin', 'grid_messages', 'Scriptcase', '154.72.168.5', 'access', ''),
(286, '2021-07-07 12:24:47', 'admin', 'form_companies', 'Scriptcase', '154.72.168.5', 'access', ''),
(287, '2021-07-07 12:24:51', 'admin', 'form_companies', 'Scriptcase', '154.72.168.5', 'access', ''),
(288, '2021-07-07 12:25:01', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(289, '2021-07-07 12:25:11', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(290, '2021-07-07 12:25:22', 'admin', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(291, '2021-07-07 12:25:32', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(292, '2021-07-07 12:25:39', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(293, '2021-07-07 12:25:43', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(294, '2021-07-07 12:26:42', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(295, '2021-07-07 12:27:11', 'navarro21', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(296, '2021-07-07 12:27:12', 'navarro21', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(297, '2021-07-07 12:27:55', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(298, '2021-07-07 12:28:14', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(299, '2021-07-07 12:28:59', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(300, '2021-07-07 12:31:28', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(301, '2021-07-07 12:31:45', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(302, '2021-07-07 12:56:59', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(303, '2021-07-07 12:59:33', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(304, '2021-07-07 12:59:48', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(305, '2021-07-07 13:03:55', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- kpi_id (new)  : 2||campaign_contract_id (new)  : 1||values (new)  : 10||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(306, '2021-07-07 13:05:17', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(307, '2021-07-07 13:12:42', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 2||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Syndy Emane ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 3||status (new)  : 1||scane (new)  : INF - Contrat Engagement et Cession de Droit Syndy Emade (1).doc||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(308, '2021-07-07 13:12:51', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(309, '2021-07-07 13:12:56', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(310, '2021-07-07 13:12:59', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(311, '2021-07-07 13:22:51', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(312, '2021-07-07 13:23:05', 'admin', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(313, '2021-07-07 13:23:13', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(314, '2021-07-07 13:23:22', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(315, '2021-07-07 13:23:25', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(316, '2021-07-07 13:26:02', 'admin', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(317, '2021-07-07 13:26:14', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(318, '2021-07-07 13:26:20', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(319, '2021-07-07 13:26:26', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(320, '2021-07-07 13:27:29', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 7||--> fields <-- kpi_id (new)  : 1||campaign_contract_id (new)  : 2||values (new)  : 10||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(321, '2021-07-07 13:27:49', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(322, '2021-07-07 13:32:28', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(323, '2021-07-07 13:32:38', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(324, '2021-07-07 13:36:53', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(325, '2021-07-07 13:37:03', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(326, '2021-07-07 13:37:20', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(327, '2021-07-07 13:37:22', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(328, '2021-07-07 13:37:24', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(329, '2021-07-07 13:38:06', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(330, '2021-07-07 13:40:42', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(331, '2021-07-07 13:40:49', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(332, '2021-07-07 13:40:50', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(333, '2021-07-07 13:40:53', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(334, '2021-07-07 13:42:46', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(335, '2021-07-07 13:43:00', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(336, '2021-07-07 13:43:09', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(337, '2021-07-07 13:43:16', 'admin', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(338, '2021-07-07 13:43:27', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(339, '2021-07-07 13:43:29', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(340, '2021-07-07 13:43:31', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(341, '2021-07-07 13:50:41', 'admin', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(342, '2021-07-07 13:50:51', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(343, '2021-07-07 13:50:55', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(344, '2021-07-07 13:51:00', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(345, '2021-07-07 13:51:42', 'admin', 'form_contracts', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 1||--> fields <-- start_date (old)  : 2019-02-01||start_date (new)  : 2021-01-01||end_date (old)  : 2025-06-10||end_date (new)  : 2021-12-31||description (old)  : Rédacteurs||description (new)  : contrat entre MTN et Syndy Emane ||influenceur_id (old)  : 1||influenceur_id (new)  : 3||scane (old)  : Profile-picture.jpg||scane (new)  : INF - Contrat Engagement et Cession de Droit Syndy Emade (1).doc'),
(346, '2021-07-07 13:58:24', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 1||--> fields <-- start_date (old)  : 2019-02-01||start_date (new)  : 2021-01-01||end_date (old)  : 2025-06-10||end_date (new)  : 2021-12-31||description (old)  : Rédacteurs||description (new)  : contrat entre MTN et Syndy Emane ||influenceur_id (old)  : 1||influenceur_id (new)  : 3||scane (old)  : Profile-picture.jpg||scane (new)  : INF - Contrat Engagement et Cession de Droit Syndy Emade (1).doc'),
(347, '2021-07-07 13:58:41', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(348, '2021-07-07 14:08:05', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(349, '2021-07-07 14:09:11', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(350, '2021-07-07 14:10:01', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(351, '2021-07-07 14:10:03', 'admin', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(352, '2021-07-07 14:10:11', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(353, '2021-07-07 14:10:14', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(354, '2021-07-07 14:10:17', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(355, '2021-07-07 14:12:17', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 3||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Fingon ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 1||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(356, '2021-07-07 14:12:28', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(357, '2021-07-07 14:12:32', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(358, '2021-07-07 14:12:34', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(359, '2021-07-07 14:42:49', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(360, '2021-07-07 14:43:00', 'navarro21', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(361, '2021-07-07 14:43:00', 'navarro21', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(362, '2021-07-07 14:43:32', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', '');
INSERT INTO `sc_log` (`id`, `inserted_date`, `username`, `application`, `creator`, `ip_user`, `action`, `description`) VALUES
(363, '2021-07-07 14:43:43', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(364, '2021-07-07 14:44:21', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(365, '2021-07-07 14:44:27', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(366, '2021-07-07 14:44:30', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(367, '2021-07-07 14:44:39', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(368, '2021-07-07 14:44:45', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(369, '2021-07-07 14:44:54', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(370, '2021-07-07 14:44:56', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(371, '2021-07-07 14:45:08', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(372, '2021-07-07 14:45:10', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(373, '2021-07-07 14:45:17', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(374, '2021-07-07 14:45:24', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'delete', '--> keys <-- id : 1||--> fields <-- start_date (old)  : 2021-01-01||end_date (old)  : 2021-12-31||description (old)  : contrat entre MTN et Syndy Emane ||created_at (old)  : 2021-06-28 16:39:10||updated_at (old)  : 2021-06-28 16:39:10||influenceur_id (old)  : 3||status (old)  : 1||scane (old)  : INF - Contrat Engagement et Cession de Droit Syndy Emade (1).doc||kip (old)  : ||campaign_contract_kpis (old)  : ||contract_campaigns (old)  : '),
(375, '2021-07-07 14:45:33', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(376, '2021-07-07 14:45:38', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(377, '2021-07-07 14:45:40', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(378, '2021-07-07 14:45:44', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(379, '2021-07-07 14:47:25', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(380, '2021-07-07 14:47:40', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(381, '2021-07-07 14:49:19', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Flavien Kouatcha ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 7||status (new)  : 1||scane (new)  : INF - Contrat Engagement et Cession de Droit - Flavien KOUATCHA.doc||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(382, '2021-07-07 14:49:26', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(383, '2021-07-07 14:49:30', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(384, '2021-07-07 14:49:33', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(385, '2021-07-07 14:50:08', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(386, '2021-07-07 14:50:36', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(387, '2021-07-07 14:51:13', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(388, '2021-07-07 14:52:19', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(389, '2021-07-07 14:52:28', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 5||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Frieda Choco||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 10||status (new)  : 1||scane (new)  : INF - Contrat Engagement et Cession de Droit - Frieda Choco Bronze.doc||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(390, '2021-07-07 14:52:38', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(391, '2021-07-07 14:52:41', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(392, '2021-07-07 14:52:43', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(393, '2021-07-07 14:52:53', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 1||--> fields <-- description (old)  : contrat entre MTN et Syndy Emane ||description (new)  : contrat entre MTN et Frieda Choco||influenceur_id (old)  : 3||influenceur_id (new)  : 10||scane (old)  : INF - Contrat Engagement et Cession de Droit Syndy Emade (1).doc||scane (new)  : INF - Contrat Engagement et Cession de Droit - Frieda Choco Bronze(1).doc'),
(394, '2021-07-07 14:53:16', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(395, '2021-07-07 14:54:01', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(396, '2021-07-07 14:54:07', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(397, '2021-07-07 14:54:23', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(398, '2021-07-07 14:54:35', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(399, '2021-07-07 14:54:51', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 5||--> fields <-- category_id (old)  : 1||category_id (new)  : 2'),
(400, '2021-07-07 14:55:00', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(401, '2021-07-07 14:55:21', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(402, '2021-07-07 14:55:27', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(403, '2021-07-07 14:55:34', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 6||--> fields <-- category_id (old)  : 1||category_id (new)  : 2'),
(404, '2021-07-07 14:55:44', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(405, '2021-07-07 14:56:25', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(406, '2021-07-07 14:56:32', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(407, '2021-07-07 14:56:49', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 6||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Cabrel Nanjip ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 13||status (new)  : 1||scane (new)  : INF - Contrat Engagement et Cession de Droit - Cabrel NANJIP.doc||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(408, '2021-07-07 14:56:55', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(409, '2021-07-07 14:56:57', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(410, '2021-07-07 14:56:59', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 7||--> fields <-- category_id (old)  : 1||category_id (new)  : 2'),
(411, '2021-07-07 14:56:59', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(412, '2021-07-07 14:57:03', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(413, '2021-07-07 14:57:24', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(414, '2021-07-07 14:57:31', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(415, '2021-07-07 14:57:38', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 8||--> fields <-- category_id (old)  : 1||category_id (new)  : 2'),
(416, '2021-07-07 14:58:00', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(417, '2021-07-07 14:58:16', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(418, '2021-07-07 14:58:20', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(419, '2021-07-07 14:58:27', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 9||--> fields <-- category_id (old)  : 1||category_id (new)  : 2'),
(420, '2021-07-07 14:58:32', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(421, '2021-07-07 14:58:42', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(422, '2021-07-07 14:58:56', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(423, '2021-07-07 14:59:04', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 10||--> fields <-- category_id (old)  : 1||category_id (new)  : 2'),
(424, '2021-07-07 14:59:08', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(425, '2021-07-07 15:00:03', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(426, '2021-07-07 15:00:12', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(427, '2021-07-07 15:00:17', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 16||--> fields <-- category_id (old)  : 2||category_id (new)  : 3'),
(428, '2021-07-07 15:00:19', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(429, '2021-07-07 15:00:46', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(430, '2021-07-07 15:00:53', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(431, '2021-07-07 15:01:03', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 18||--> fields <-- category_id (old)  : 2||category_id (new)  : 3'),
(432, '2021-07-07 15:01:08', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(433, '2021-07-07 15:01:31', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(434, '2021-07-07 15:01:37', 'Anïs', 'form_influencer_managers', 'Scriptcase', '154.72.168.5', 'access', ''),
(435, '2021-07-07 15:02:02', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 19||--> fields <-- category_id (old)  : 2||category_id (new)  : 3'),
(436, '2021-07-07 15:02:05', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(437, '2021-07-07 15:02:29', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'access', ''),
(438, '2021-07-07 15:03:10', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(439, '2021-07-07 15:03:32', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(440, '2021-07-07 15:04:58', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 1||--> fields <-- description (old)  : contrat entre MTN et Frieda Choco||description (new)  : contrat entre MTN et Cabrel Nanjip ||influenceur_id (old)  : 10||influenceur_id (new)  : 13||scane (old)  : INF - Contrat Engagement et Cession de Droit - Frieda Choco Bronze(1).doc||scane (new)  : INF - Contrat Engagement et Cession de Droit - Cabrel NANJIP(1).doc'),
(441, '2021-07-07 15:05:08', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(442, '2021-07-07 15:05:36', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(443, '2021-07-07 15:05:44', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(444, '2021-07-07 15:05:49', 'Anïs', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(445, '2021-07-07 15:07:01', 'Anïs', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 7||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : Contrat entre MTN et Muriel Blanche||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 2||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(446, '2021-07-07 15:07:10', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(447, '2021-07-07 15:07:14', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(448, '2021-07-07 15:07:16', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(449, '2021-07-07 15:07:24', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(450, '2021-07-07 15:07:33', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(451, '2021-07-07 15:08:21', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(452, '2021-07-07 15:11:08', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 3||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 3||text (new)  : Je reste comme ça les messages tombent seulement en cascades ! Que merci mumu '),
(453, '2021-07-07 15:11:14', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(454, '2021-07-07 15:11:16', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(455, '2021-07-07 15:12:09', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 1||--> fields <-- action_id (new)  : 3||type (new)  : Images||link (new)  : 209175941_364675355016080_7574562898651915843_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(456, '2021-07-07 15:13:58', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- user_id (new)  : 7||action_id (new)  : 3||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 18412||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(457, '2021-07-07 15:14:46', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 5||--> fields <-- user_id (new)  : 7||action_id (new)  : 3||kpi_id (new)  : 4||date_j (new)  : 2021-07-07||value (new)  : 399||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(458, '2021-07-07 15:15:11', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 6||--> fields <-- user_id (new)  : 7||action_id (new)  : 3||kpi_id (new)  : 3||date_j (new)  : 2021-07-07||value (new)  : 14||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(459, '2021-07-07 15:25:53', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(460, '2021-07-07 15:26:18', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(461, '2021-07-07 15:28:06', 'Anïs', 'form_platforms', 'Scriptcase', '154.72.168.5', 'access', ''),
(462, '2021-07-07 15:32:06', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(463, '2021-07-07 15:32:34', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(464, '2021-07-07 15:32:50', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(465, '2021-07-07 15:33:26', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 8||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Muriel Blanche ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 2||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(466, '2021-07-07 15:33:32', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(467, '2021-07-07 15:33:34', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(468, '2021-07-07 15:33:36', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(469, '2021-07-07 15:33:40', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(470, '2021-07-07 15:33:55', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(471, '2021-07-07 15:34:05', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(472, '2021-07-07 15:34:41', 'Anïs', 'form_platforms', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- name (new)  : Instagram||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(473, '2021-07-07 15:34:42', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 9||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Fingon  ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 1||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(474, '2021-07-07 15:34:46', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(475, '2021-07-07 15:34:49', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(476, '2021-07-07 15:34:51', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(477, '2021-07-07 15:35:11', 'Anïs', 'form_platforms', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 5||--> fields <-- name (new)  : Tik-tok||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(478, '2021-07-07 15:35:12', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(479, '2021-07-07 15:35:32', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(480, '2021-07-07 15:35:46', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(481, '2021-07-07 15:35:46', 'Anïs', 'form_categories', 'Scriptcase', '154.72.168.5', 'access', ''),
(482, '2021-07-07 15:36:00', 'Anïs', 'form_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(483, '2021-07-07 15:36:28', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 10||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Tenor||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 4||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(484, '2021-07-07 15:36:32', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(485, '2021-07-07 15:36:34', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(486, '2021-07-07 15:36:40', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(487, '2021-07-07 15:36:56', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(488, '2021-07-07 15:37:15', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'access', ''),
(489, '2021-07-07 15:37:54', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'delete', '--> keys <-- id : 4||--> fields <-- name (old)  : Like||status (old)  : 1||created_at (old)  : 2021-06-30 11:02:56||updated_at (old)  : 2021-06-30 11:02:56'),
(490, '2021-07-07 15:37:54', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(491, '2021-07-07 15:38:00', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'delete', '--> keys <-- id : 5||--> fields <-- name (old)  : Partage||status (old)  : 1||created_at (old)  : 2021-06-30 11:03:03||updated_at (old)  : 2021-06-30 11:03:03'),
(492, '2021-07-07 15:38:02', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(493, '2021-07-07 15:38:16', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 7||--> fields <-- name (new)  : insert||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(494, '2021-07-07 15:38:50', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 8||--> fields <-- name (new)  : Apparition aux évènements||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(495, '2021-07-07 15:39:07', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 11||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Minks||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 5||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(496, '2021-07-07 15:39:11', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(497, '2021-07-07 15:39:12', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(498, '2021-07-07 15:39:14', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(499, '2021-07-07 15:39:17', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(500, '2021-07-07 15:39:32', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(501, '2021-07-07 15:39:51', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(502, '2021-07-07 15:40:09', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 9||--> fields <-- name (new)  : Episode||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(503, '2021-07-07 15:40:29', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 10||--> fields <-- name (new)  : Expérience sharing||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(504, '2021-07-07 15:40:37', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 12||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Ko-C||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 6||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(505, '2021-07-07 15:40:46', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(506, '2021-07-07 15:40:47', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(507, '2021-07-07 15:40:48', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(508, '2021-07-07 15:40:53', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(509, '2021-07-07 15:41:02', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 11||--> fields <-- name (new)  : Photo OOH||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(510, '2021-07-07 15:41:05', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(511, '2021-07-07 15:41:12', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(512, '2021-07-07 15:41:54', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 13||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Philippe Simo||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 8||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(513, '2021-07-07 15:41:59', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(514, '2021-07-07 15:42:01', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(515, '2021-07-07 15:42:01', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 12||--> fields <-- name (new)  : Monday motivation||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(516, '2021-07-07 15:42:03', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(517, '2021-07-07 15:42:08', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(518, '2021-07-07 15:42:19', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 13||--> fields <-- name (new)  : Gaming||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(519, '2021-07-07 15:42:25', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(520, '2021-07-07 15:42:46', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(521, '2021-07-07 15:42:52', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(522, '2021-07-07 15:43:04', 'Anïs', 'form_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(523, '2021-07-07 15:43:09', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(524, '2021-07-07 15:43:10', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(525, '2021-07-07 15:43:12', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(526, '2021-07-07 15:43:40', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(527, '2021-07-07 15:43:42', 'navarro21', 'form_contracts', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 14||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Claudel Noubissi ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 9||status (new)  : 1||scane (new)  : ||kip (new)  : ||campaign_contract_kpis (new)  : ||contract_campaigns (new)  : '),
(528, '2021-07-07 15:43:42', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(529, '2021-07-07 15:43:52', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(530, '2021-07-07 15:43:53', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(531, '2021-07-07 15:43:55', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(532, '2021-07-07 15:43:57', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(533, '2021-07-07 15:43:57', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(534, '2021-07-07 15:44:03', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(535, '2021-07-07 15:44:05', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(536, '2021-07-07 15:44:19', 'admin', 'sec_sync_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(537, '2021-07-07 15:44:23', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(538, '2021-07-07 15:44:29', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(539, '2021-07-07 15:44:37', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(540, '2021-07-07 15:44:39', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(541, '2021-07-07 15:44:42', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(542, '2021-07-07 15:44:42', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(543, '2021-07-07 15:47:13', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 4||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 4||reaction_type_id (new)  : 3||text (new)  : Je reste comme ça les messages tombent seulement en cascades ! Que merci mumu '),
(544, '2021-07-07 15:47:17', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(545, '2021-07-07 15:47:19', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(546, '2021-07-07 15:47:56', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(547, '2021-07-07 15:48:08', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(548, '2021-07-07 15:48:17', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 4||--> fields <-- text (old)  : Je reste comme ça les messages tombent seulement en cascades ! Que merci mumu ????! Je dis que eh! Y a quoi même ????? c’est comme ça que un me dit qu’il a regardé mes dernières story sur Ayoba Instant Messaging, ça ne lui a pas coupé ses datas et qu’en plus de ça, il a reçu 100 mégas gratuits ????????!!!\r\nVoici le lien mes braves\r\n\r\nhttps://i.ayo.ba/dQjW/murielblanche allez chercher vos mégas ????????.\r\n\r\nJ’ai dit que ça va aller vite le tour ci!!❤️❤️????||text (new)  : Je reste comme ça les messages tombent seulement en cascades ! Que merci mumu ????! Je dis que eh! Y a quoi même ????? c’est comme ça que un me dit qu’il a regardé mes dernières story sur Ayoba Instant Messaging, ça ne lui a pas coupé ses datas et qu’en plus de ça, il a reçu 100 mégas gratuits ????????!!!\nVoici le lien mes braves\n\nhttps://i.ayo.ba/dQjW/murielblanche allez chercher vos mégas ????????.\n\nJ’ai dit que ça va aller vite le tour ci!!❤️❤️????||tags (old)  : \r\n#ayoba #LEKATALOG #mtncameroun #imbattable||tags (new)  : \n#ayoba #LEKATALOG #mtncameroun #imbattable'),
(549, '2021-07-07 15:48:18', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(550, '2021-07-07 15:48:26', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(551, '2021-07-07 15:48:27', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(552, '2021-07-07 15:48:28', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(553, '2021-07-07 15:48:50', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 2||--> fields <-- action_id (new)  : 4||type (new)  : Images||link (new)  : 209175941_364675355016080_7574562898651915843_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(554, '2021-07-07 15:48:56', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 15||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre OS et Caprice Audrey ||created_at (new)  : 2021-01-01 00:00:00||updated_at (new)  : 2021-12-01 00:00:00||influenceur_id (new)  : 14||status (new)  : ||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(555, '2021-07-07 15:49:01', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(556, '2021-07-07 15:49:03', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(557, '2021-07-07 15:49:04', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(558, '2021-07-07 15:49:11', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(559, '2021-07-07 15:49:15', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(560, '2021-07-07 15:49:36', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(561, '2021-07-07 15:49:40', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(562, '2021-07-07 15:49:41', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(563, '2021-07-07 15:50:21', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(564, '2021-07-07 15:50:39', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(565, '2021-07-07 15:52:35', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 16||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre MTN et Dr Ni||created_at (new)  : 2021-01-01 00:00:00||updated_at (new)  : 2021-12-01 00:00:00||influenceur_id (new)  : 15||status (new)  : ||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(566, '2021-07-07 15:52:38', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(567, '2021-07-07 15:52:39', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(568, '2021-07-07 15:52:41', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(569, '2021-07-07 15:52:54', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(570, '2021-07-07 15:53:48', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(571, '2021-07-07 15:54:00', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(572, '2021-07-07 15:57:09', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(573, '2021-07-07 15:57:16', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(574, '2021-07-07 15:57:17', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(575, '2021-07-07 15:57:27', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(576, '2021-07-07 15:57:40', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(577, '2021-07-07 15:57:43', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(578, '2021-07-07 15:57:44', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(579, '2021-07-07 16:05:10', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(580, '2021-07-07 16:05:17', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(581, '2021-07-07 16:05:18', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(582, '2021-07-07 16:05:27', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(583, '2021-07-07 16:05:34', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(584, '2021-07-07 16:05:39', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(585, '2021-07-07 16:05:40', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(586, '2021-07-07 16:07:09', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(587, '2021-07-07 16:07:20', 'admin', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(588, '2021-07-07 16:07:25', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(589, '2021-07-07 16:07:26', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(590, '2021-07-07 16:08:59', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(591, '2021-07-07 16:09:54', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(592, '2021-07-07 16:09:56', 'admin', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(593, '2021-07-07 16:10:00', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(594, '2021-07-07 16:10:01', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(595, '2021-07-07 16:10:58', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(596, '2021-07-07 16:11:24', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(597, '2021-07-07 16:11:46', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(598, '2021-07-07 16:11:53', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(599, '2021-07-07 16:12:55', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(600, '2021-07-07 16:13:15', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 17||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre OS et Filantrio ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 16||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(601, '2021-07-07 16:13:21', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(602, '2021-07-07 16:13:23', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(603, '2021-07-07 16:13:25', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(604, '2021-07-07 16:13:29', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(605, '2021-07-07 16:14:23', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(606, '2021-07-07 16:14:38', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(607, '2021-07-07 16:14:52', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(608, '2021-07-07 16:15:34', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 18||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre OS et Annie Payep ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 17||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(609, '2021-07-07 16:15:40', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(610, '2021-07-07 16:15:41', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(611, '2021-07-07 16:15:41', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(612, '2021-07-07 16:16:04', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(613, '2021-07-07 16:16:34', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(614, '2021-07-07 16:16:39', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(615, '2021-07-07 16:17:40', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 19||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre OS et Chouchou Mpacko ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 18||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(616, '2021-07-07 16:17:44', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(617, '2021-07-07 16:17:45', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(618, '2021-07-07 16:17:45', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(619, '2021-07-07 16:18:03', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(620, '2021-07-07 16:18:17', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(621, '2021-07-07 16:18:27', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(622, '2021-07-07 16:18:47', 'admin', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(623, '2021-07-07 16:18:49', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(624, '2021-07-07 16:18:50', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(625, '2021-07-07 16:18:58', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(626, '2021-07-07 16:19:05', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(627, '2021-07-07 16:19:06', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(628, '2021-07-07 16:19:06', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 20||--> fields <-- start_date (new)  : 2021-01-01||end_date (new)  : 2021-12-31||description (new)  : contrat entre OS et Dr selfie ||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 19||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(629, '2021-07-07 16:19:09', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(630, '2021-07-07 16:19:10', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(631, '2021-07-07 16:19:11', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(632, '2021-07-07 16:19:16', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(633, '2021-07-07 16:19:21', 'admin', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(634, '2021-07-07 16:19:25', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(635, '2021-07-07 16:19:26', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(636, '2021-07-07 16:20:13', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(637, '2021-07-07 16:20:17', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(638, '2021-07-07 16:20:17', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(639, '2021-07-07 16:20:23', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(640, '2021-07-07 16:20:30', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(641, '2021-07-07 16:20:46', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(642, '2021-07-07 16:24:40', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 5||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 7||platform_id (new)  : 1||reaction_type_id (new)  : 2||text (new)  : L’Internet IMBATTABLE avec Mtn Home pour toute la famille à partir de 15,000frs.\r\nC’est la magie ohh \r\nQui veut les data???\r\nNdedi Eyango KingmouannkumOffiel\r\nAlene Menget||link (new)  : https://fb.watch/v/1sQC-ayD1/||status (new)  : 1||tags (new)  : #mtnhome \r\n#brandambassador  #imbattable #alphafemale #mommaB||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 2||Fichier  (new)  : ||detail (new)  : '),
(643, '2021-07-07 16:24:50', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(644, '2021-07-07 16:24:51', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(645, '2021-07-07 16:25:45', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 15||--> fields <-- user_id (new)  : 7||action_id (new)  : 5||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 177||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(646, '2021-07-07 16:26:07', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 16||--> fields <-- user_id (new)  : 7||action_id (new)  : 5||kpi_id (new)  : 4||date_j (new)  : 2021-07-07||value (new)  : 7||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(647, '2021-07-07 16:26:30', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 17||--> fields <-- user_id (new)  : 7||action_id (new)  : 5||kpi_id (new)  : 3||date_j (new)  : 2021-07-07||value (new)  : 4||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(648, '2021-07-07 16:26:33', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 5||--> fields <-- text (old)  : L’Internet IMBATTABLE avec Mtn Home pour toute la famille à partir de 15,000frs.\r\nC’est la magie ohh \r\nQui veut les data???\r\nNdedi Eyango KingmouannkumOffiel\r\nAlene Menget||text (new)  : L’Internet IMBATTABLE avec Mtn Home pour toute la famille à partir de 15,000frs.\nC’est la magie ohh \nQui veut les data???\nNdedi Eyango KingmouannkumOffiel\nAlene Menget||tags (old)  : #mtnhome \r\n#brandambassador  #imbattable #alphafemale #mommaB||tags (new)  : #mtnhome \n#brandambassador  #imbattable #alphafemale #mommaB'),
(649, '2021-07-07 16:26:42', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(650, '2021-07-07 16:26:51', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(651, '2021-07-07 16:27:15', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 18||--> fields <-- user_id (new)  : 7||action_id (new)  : 5||kpi_id (new)  : 1||date_j (new)  : 2021-07-07||value (new)  : 1300||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(652, '2021-07-07 16:27:47', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(653, '2021-07-07 16:28:03', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(654, '2021-07-07 16:28:08', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(655, '2021-07-07 16:28:39', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(656, '2021-07-07 16:28:49', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(657, '2021-07-07 16:29:00', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(658, '2021-07-07 16:29:08', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(659, '2021-07-07 16:29:15', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(660, '2021-07-07 16:29:33', 'navarro21', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(661, '2021-07-07 16:29:43', 'navarro21', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(662, '2021-07-07 16:29:48', 'navarro21', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(663, '2021-07-07 16:29:49', 'navarro21', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(664, '2021-07-07 16:29:50', 'navarro21', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(665, '2021-07-07 16:30:04', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(666, '2021-07-07 16:30:11', 'navarro21', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(667, '2021-07-07 16:31:09', 'navarro21', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(668, '2021-07-07 16:31:52', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(669, '2021-07-07 16:31:58', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(670, '2021-07-07 16:32:00', 'navarro21', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(671, '2021-07-07 16:32:01', 'navarro21', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(672, '2021-07-07 16:32:16', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(673, '2021-07-07 16:32:26', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(674, '2021-07-07 16:32:36', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(675, '2021-07-07 16:32:49', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(676, '2021-07-07 16:32:54', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(677, '2021-07-07 16:32:54', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(678, '2021-07-07 16:32:59', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(679, '2021-07-07 16:33:33', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(680, '2021-07-07 16:33:38', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(681, '2021-07-07 16:33:52', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(682, '2021-07-07 16:33:57', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(683, '2021-07-07 16:34:12', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(684, '2021-07-07 16:34:29', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(685, '2021-07-07 16:34:30', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(686, '2021-07-07 16:34:46', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(687, '2021-07-07 16:34:47', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(688, '2021-07-07 16:45:26', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 6||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 7||platform_id (new)  : 4||reaction_type_id (new)  : 2||text (new)  : L’Internet IMBATTABLE avec @mtncameroon Home pour toute la famille à partir de 15,000frs.\r\nC’est la magie ohh\r\nQui veut les data???||link (new)  : https://fb.watch/v/1sQC-ayD1/||status (new)  : 1||tags (new)  : #mtnhome\r\n#brandambassador #imbattable #alphafemale #mommaB||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 2||Fichier  (new)  : ||detail (new)  : '),
(689, '2021-07-07 16:45:30', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(690, '2021-07-07 16:45:32', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(691, '2021-07-07 16:45:59', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 19||--> fields <-- user_id (new)  : 7||action_id (new)  : 6||kpi_id (new)  : 4||date_j (new)  : 2021-07-07||value (new)  : 9||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(692, '2021-07-07 16:46:24', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 20||--> fields <-- user_id (new)  : 7||action_id (new)  : 6||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 2668||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(693, '2021-07-07 16:46:47', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 6||--> fields <-- text (old)  : L’Internet IMBATTABLE avec @mtncameroon Home pour toute la famille à partir de 15,000frs.\r\nC’est la magie ohh\r\nQui veut les data???||text (new)  : L’Internet IMBATTABLE avec @mtncameroon Home pour toute la famille à partir de 15,000frs.\nC’est la magie ohh\nQui veut les data???||tags (old)  : #mtnhome\r\n#brandambassador #imbattable #alphafemale #mommaB||tags (new)  : #mtnhome\n#brandambassador #imbattable #alphafemale #mommaB'),
(694, '2021-07-07 16:46:53', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', '');
INSERT INTO `sc_log` (`id`, `inserted_date`, `username`, `application`, `creator`, `ip_user`, `action`, `description`) VALUES
(695, '2021-07-07 16:49:19', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(696, '2021-07-07 17:05:37', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(697, '2021-07-07 17:05:45', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(698, '2021-07-07 17:05:47', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(699, '2021-07-07 17:05:52', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(700, '2021-07-07 17:15:39', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(701, '2021-07-07 17:18:39', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(702, '2021-07-07 17:18:40', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(703, '2021-07-07 17:19:02', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(704, '2021-07-07 17:19:13', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(705, '2021-07-07 17:20:39', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 7||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 2||text (new)  : Ma République \r\nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre... \r\nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout! \r\nPardon allez vous-même un peu télécharger le way derrière le lien si '),
(706, '2021-07-07 17:20:41', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(707, '2021-07-07 17:20:41', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(708, '2021-07-07 17:22:01', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 21||--> fields <-- user_id (new)  : 7||action_id (new)  : 7||kpi_id (new)  : 4||date_j (new)  : 2021-07-07||value (new)  : 1000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(709, '2021-07-07 17:22:29', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 21||--> fields <-- user_id (old)  : ||user_id (new)  : 7||action_id (old)  : ||action_id (new)  : 7||value (old)  : 1000||value (new)  : 144'),
(710, '2021-07-07 17:22:46', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 22||--> fields <-- user_id (new)  : 7||action_id (new)  : 7||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 1000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(711, '2021-07-07 17:23:28', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 23||--> fields <-- user_id (new)  : 7||action_id (new)  : 7||kpi_id (new)  : 3||date_j (new)  : 2021-07-07||value (new)  : 86||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(712, '2021-07-07 17:23:38', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 7||--> fields <-- text (old)  : Ma République \r\nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre... \r\nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout! \r\nPardon allez vous-même un peu télécharger le way derrière le lien si ????????: https://i.ayo.ba/dQjW/yamo||text (new)  : Ma République \nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre... \nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout! \nPardon allez vous-même un peu télécharger le way derrière le lien si ????????: https://i.ayo.ba/dQjW/yamo'),
(713, '2021-07-07 17:23:45', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(714, '2021-07-07 17:24:43', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(715, '2021-07-07 17:24:48', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(716, '2021-07-07 17:24:49', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(717, '2021-07-07 17:24:52', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(718, '2021-07-07 17:24:55', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(719, '2021-07-07 17:24:55', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(720, '2021-07-07 17:25:08', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(721, '2021-07-07 17:25:15', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(722, '2021-07-07 17:25:38', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(723, '2021-07-07 17:25:44', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(724, '2021-07-07 17:25:49', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(725, '2021-07-07 17:26:07', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(726, '2021-07-07 17:26:10', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(727, '2021-07-07 17:26:11', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(728, '2021-07-07 17:26:13', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(729, '2021-07-07 17:26:15', 'admin', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(730, '2021-07-07 17:26:38', 'admin', 'sec_grid_sec_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(731, '2021-07-07 17:26:39', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(732, '2021-07-07 17:26:42', 'admin', 'sec_search_sec_groups', 'Scriptcase', '154.72.168.5', 'access', ''),
(733, '2021-07-07 17:26:44', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '154.72.168.5', 'access', ''),
(734, '2021-07-07 17:27:05', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 8||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 4||reaction_type_id (new)  : 2||text (new)  : Ma République\r\nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre...\r\n\r\nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout!\r\n\r\nPardon allez vous-même un peu télécharger le way derrière lien si '),
(735, '2021-07-07 17:27:08', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(736, '2021-07-07 17:27:10', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(737, '2021-07-07 17:27:53', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 24||--> fields <-- user_id (new)  : 7||action_id (new)  : 8||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 2874||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(738, '2021-07-07 17:29:16', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 25||--> fields <-- user_id (new)  : 7||action_id (new)  : 8||kpi_id (new)  : 4||date_j (new)  : 2021-07-07||value (new)  : 21||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(739, '2021-07-07 17:29:53', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 8||--> fields <-- text (old)  : Ma République\r\nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre...\r\n\r\nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout!\r\n\r\nPardon allez vous-même un peu télécharger le way derrière lien si ????????: https://i.ayo.ba/dQjW/yamo\r\n\r\net venez vous même me dire ce que vous en pensez.||text (new)  : Ma République\nPardon aidez-moi à lire l’affaire ci, peut-être c’est moi qui vois ça un genre...\n\nDonc pendant que je réchauffe la Grosse marmite♨️, d’autres sont là pour réchauffer mon nom partout!\n\nPardon allez vous-même un peu télécharger le way derrière lien si ????????: https://i.ayo.ba/dQjW/yamo\n\net venez vous même me dire ce que vous en pensez.'),
(740, '2021-07-07 17:29:57', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(741, '2021-07-07 17:58:19', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(742, '2021-07-07 17:58:35', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(743, '2021-07-07 17:58:35', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(744, '2021-07-07 17:58:43', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(745, '2021-07-07 17:58:54', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(746, '2021-07-07 18:00:23', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(747, '2021-07-07 18:00:59', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(748, '2021-07-07 18:02:45', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(749, '2021-07-07 18:02:52', 'Anïs', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(750, '2021-07-07 18:02:53', 'Anïs', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(751, '2021-07-07 18:03:01', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(752, '2021-07-07 18:03:08', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(753, '2021-07-07 18:03:27', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(754, '2021-07-07 18:03:31', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.5', 'access', ''),
(755, '2021-07-07 18:03:54', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.5', 'access', ''),
(756, '2021-07-07 18:04:00', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'access', ''),
(757, '2021-07-07 18:05:21', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 21||--> fields <-- start_date (new)  : 2021-05-01||end_date (new)  : 2021-12-31||description (new)  : Contrat entre MTN et Takam||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 11||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(758, '2021-07-07 18:05:25', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(759, '2021-07-07 18:05:27', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.5', 'access', ''),
(760, '2021-07-07 18:05:28', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.5', 'access', ''),
(761, '2021-07-07 18:06:53', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(762, '2021-07-07 18:07:00', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(763, '2021-07-07 18:09:37', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 9||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 2||reaction_type_id (new)  : 7||text (new)  : \r\nAmis A Vie. ( épisode 01 ) - Le Noir Coeur\r\n||link (new)  : https://www.youtube.com/watch?v=XulfZm0n3EA||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||Fichier  (new)  : ||detail (new)  : '),
(764, '2021-07-07 18:09:41', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(765, '2021-07-07 18:09:42', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(766, '2021-07-07 18:09:50', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 9||--> fields <-- reaction_type_id (old)  : 7||reaction_type_id (new)  : 9||text (old)  : \r\nAmis A Vie. ( épisode 01 ) - Le Noir Coeur\r\n||text (new)  : \nAmis A Vie. ( épisode 01 ) - Le Noir Coeur\n'),
(767, '2021-07-07 18:10:44', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 26||--> fields <-- user_id (new)  : 7||action_id (new)  : 9||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 2900||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(768, '2021-07-07 18:11:54', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 27||--> fields <-- user_id (new)  : 7||action_id (new)  : 9||kpi_id (new)  : 1||date_j (new)  : 2021-07-07||value (new)  : 85078||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(769, '2021-07-07 18:12:41', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(770, '2021-07-07 18:12:50', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(771, '2021-07-07 18:14:55', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 10||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 2||reaction_type_id (new)  : 9||text (new)  : Amis A Vie. ( épisode 02 ) - La mal bouche||link (new)  : https://www.youtube.com/watch?v=E0OIxZm2T7o||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||Fichier  (new)  : ||detail (new)  : '),
(772, '2021-07-07 18:15:02', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(773, '2021-07-07 18:15:04', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(774, '2021-07-07 18:16:02', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 28||--> fields <-- user_id (new)  : 7||action_id (new)  : 10||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 1900||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(775, '2021-07-07 18:16:49', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 29||--> fields <-- user_id (new)  : 7||action_id (new)  : 10||kpi_id (new)  : 1||date_j (new)  : 2021-07-07||value (new)  : 55726||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(776, '2021-07-07 18:17:41', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(777, '2021-07-07 18:18:47', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(778, '2021-07-07 18:20:24', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 11||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 2||reaction_type_id (new)  : 9||text (new)  : \r\nAmis à Vie. (épisode 03) , la boule zéro||link (new)  : https://www.youtube.com/watch?v=XsKZTjZnkGA||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||Fichier  (new)  : ||detail (new)  : '),
(779, '2021-07-07 18:20:30', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(780, '2021-07-07 18:20:33', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(781, '2021-07-07 18:21:56', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 30||--> fields <-- user_id (new)  : 7||action_id (new)  : 11||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 1500||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(782, '2021-07-07 18:23:55', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 31||--> fields <-- user_id (new)  : 7||action_id (new)  : 11||kpi_id (new)  : 1||date_j (new)  : 2021-07-07||value (new)  : 45588||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(783, '2021-07-07 18:24:10', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'update', '--> keys <-- id : 11||--> fields <-- text (old)  : \r\nAmis à Vie. (épisode 03) , la boule zéro||text (new)  : \nAmis à Vie. (épisode 03) , la boule zéro'),
(784, '2021-07-07 18:24:14', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(785, '2021-07-07 18:24:24', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(786, '2021-07-07 18:26:25', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 12||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 2||reaction_type_id (new)  : 9||text (new)  : Amis A Vie ( épisode 04 ) la conseillère||link (new)  : https://www.youtube.com/watch?v=-yS9_38EZno||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||Fichier  (new)  : ||detail (new)  : '),
(787, '2021-07-07 18:26:32', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(788, '2021-07-07 18:26:34', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(789, '2021-07-07 18:27:07', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 32||--> fields <-- user_id (new)  : 7||action_id (new)  : 12||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 1600||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(790, '2021-07-07 18:27:39', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 33||--> fields <-- user_id (new)  : 7||action_id (new)  : 12||kpi_id (new)  : 1||date_j (new)  : 2021-07-07||value (new)  : 44474||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(791, '2021-07-07 18:27:49', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(792, '2021-07-07 18:28:15', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(793, '2021-07-07 18:29:26', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 13||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 2||reaction_type_id (new)  : 9||text (new)  : Amis A Vie , ( épisode 05) , Viviane||link (new)  : https://www.youtube.com/watch?v=GlFYqLn7jB0||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||Fichier  (new)  : ||detail (new)  : '),
(794, '2021-07-07 18:29:33', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(795, '2021-07-07 18:29:35', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(796, '2021-07-07 18:30:35', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 34||--> fields <-- user_id (new)  : 7||action_id (new)  : 13||kpi_id (new)  : 5||date_j (new)  : 2021-07-07||value (new)  : 2000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(797, '2021-07-07 18:31:15', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'insert', '--> keys <-- id : 35||--> fields <-- user_id (new)  : 7||action_id (new)  : 13||kpi_id (new)  : 1||date_j (new)  : 2021-07-07||value (new)  : 53517||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(798, '2021-07-07 18:31:24', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(799, '2021-07-07 19:17:24', '', 'sec_Login', 'Scriptcase', '154.72.168.5', 'access', ''),
(800, '2021-07-07 19:17:57', 'admin', 'sec_Login', 'User', '154.72.168.5', 'login', 'Connecté avec succès dans le système !'),
(801, '2021-07-07 19:17:57', 'admin', 'menu', 'Scriptcase', '154.72.168.5', 'access', ''),
(802, '2021-07-07 19:18:08', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(803, '2021-07-07 19:18:38', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(804, '2021-07-07 19:18:47', 'admin', 'form_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(805, '2021-07-07 19:18:51', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.5', 'access', ''),
(806, '2021-07-07 19:18:52', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.5', 'access', ''),
(807, '2021-07-07 19:22:04', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.5', 'access', ''),
(808, '2021-07-07 19:40:50', '', 'sec_Login', 'Scriptcase', '129.0.76.216', 'access', ''),
(809, '2021-07-07 19:41:21', 'admin', 'sec_Login', 'User', '129.0.76.216', 'login', 'Connecté avec succès dans le système !'),
(810, '2021-07-07 19:41:22', 'admin', 'menu', 'Scriptcase', '129.0.76.216', 'access', ''),
(811, '2021-07-07 19:41:44', 'admin', 'form_companies', 'Scriptcase', '129.0.76.216', 'access', ''),
(812, '2021-07-08 09:01:44', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(813, '2021-07-08 09:13:08', 'admin', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(814, '2021-07-08 09:13:09', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(815, '2021-07-08 09:13:18', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(816, '2021-07-08 09:25:03', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(817, '2021-07-08 09:25:22', 'Anïs', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(818, '2021-07-08 09:25:22', 'Anïs', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(819, '2021-07-08 09:25:38', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(820, '2021-07-08 09:28:30', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(821, '2021-07-08 09:28:55', 'navarro21', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(822, '2021-07-08 09:28:55', 'navarro21', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(823, '2021-07-08 09:29:21', 'navarro21', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(824, '2021-07-08 09:32:57', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(825, '2021-07-08 09:33:11', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(826, '2021-07-08 09:33:16', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(827, '2021-07-08 09:38:30', 'admin', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(828, '2021-07-08 09:38:31', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(829, '2021-07-08 09:38:39', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(830, '2021-07-08 09:38:51', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(831, '2021-07-08 09:56:45', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(832, '2021-07-08 09:56:53', 'admin', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(833, '2021-07-08 09:56:54', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(834, '2021-07-08 09:57:04', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(835, '2021-07-08 09:57:09', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(836, '2021-07-08 09:57:33', 'admin', 'grid_actions_details_last', 'Scriptcase', '154.72.170.158', 'access', ''),
(837, '2021-07-08 09:58:09', 'admin', 'grid_message_groupe_push', 'Scriptcase', '154.72.170.158', 'access', ''),
(838, '2021-07-08 09:59:00', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(839, '2021-07-08 09:59:20', 'admin', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(840, '2021-07-08 09:59:33', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'access', ''),
(841, '2021-07-08 09:59:48', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(842, '2021-07-08 09:59:52', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(843, '2021-07-08 09:59:54', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(844, '2021-07-08 10:01:55', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 9||--> fields <-- description (old)  : contrat entre MTN et Fingon  ||description (new)  : contrat entre MTN et Fingon  2'),
(845, '2021-07-08 10:02:09', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(846, '2021-07-08 10:02:32', 'admin', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(847, '2021-07-08 10:02:44', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(848, '2021-07-08 10:05:31', 'admin', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(849, '2021-07-08 10:05:42', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'access', ''),
(850, '2021-07-08 10:06:06', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(851, '2021-07-08 10:06:08', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(852, '2021-07-08 10:06:12', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(853, '2021-07-08 10:06:29', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 8||--> fields <-- description (old)  : contrat entre MTN et Muriel Blanche ||description (new)  : contrat entre MTN et Muriel Blanche 2'),
(854, '2021-07-08 10:06:39', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(855, '2021-07-08 10:07:15', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'delete', '--> keys <-- id : 8||--> fields <-- start_date (old)  : 2021-01-01||end_date (old)  : 2021-12-31||description (old)  : contrat entre MTN et Muriel Blanche 2||created_at (old)  : 2021-07-07 17:33:26||updated_at (old)  : 2021-07-07 17:33:26||influenceur_id (old)  : 2||status (old)  : 1||scane (old)  : ||kpis (old)  : ||campagne (old)  : ||campaign_contract_kpis (old)  : '),
(856, '2021-07-08 10:07:30', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(857, '2021-07-08 10:07:33', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(858, '2021-07-08 10:07:33', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(859, '2021-07-08 10:07:34', 'admin', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(860, '2021-07-08 10:07:47', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(861, '2021-07-08 10:08:08', 'admin', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(862, '2021-07-08 10:08:15', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'access', ''),
(863, '2021-07-08 10:08:36', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(864, '2021-07-08 10:08:39', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(865, '2021-07-08 10:08:39', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'delete', '--> keys <-- id : 9||--> fields <-- start_date (old)  : 2021-01-01||end_date (old)  : 2021-12-31||description (old)  : contrat entre MTN et Fingon  2||created_at (old)  : 2021-07-07 17:34:42||updated_at (old)  : 2021-07-07 17:34:42||influenceur_id (old)  : 1||status (old)  : 1||scane (old)  : ||kpis (old)  : ||campagne (old)  : ||campaign_contract_kpis (old)  : '),
(866, '2021-07-08 10:08:42', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(867, '2021-07-08 10:08:53', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(868, '2021-07-08 10:08:54', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(869, '2021-07-08 10:08:55', 'admin', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(870, '2021-07-08 10:09:20', 'admin', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'access', ''),
(871, '2021-07-08 10:09:38', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(872, '2021-07-08 10:09:39', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(873, '2021-07-08 10:09:41', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(874, '2021-07-08 10:10:07', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 3||--> fields <-- kpi_id (new)  : 4||contract_id (new)  : 3||values (new)  : 5000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(875, '2021-07-08 10:10:22', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 4||--> fields <-- kpi_id (new)  : 5||contract_id (new)  : 3||values (new)  : 20000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(876, '2021-07-08 10:10:59', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 5||--> fields <-- kpi_id (new)  : 2||contract_id (new)  : 3||values (new)  : 100||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(877, '2021-07-08 10:11:14', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 6||--> fields <-- kpi_id (new)  : 1||contract_id (new)  : 3||values (new)  : 50000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(878, '2021-07-08 10:11:52', 'admin', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 7||--> fields <-- kpi_id (new)  : 3||contract_id (new)  : 3||values (new)  : 1000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(879, '2021-07-08 10:12:56', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 3||--> fields <-- contract_id (new)  : 3||campaign_id (new)  : 4||status (new)  : 1||start_date (new)  : 2021-06-01||end_date (new)  : 2021-12-31||created_at (new)  : ||updated_at (new)  : '),
(880, '2021-07-08 10:13:32', 'admin', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 4||--> fields <-- contract_id (new)  : 3||campaign_id (new)  : 1||status (new)  : 1||start_date (new)  : 2021-06-01||end_date (new)  : 2021-12-31||created_at (new)  : ||updated_at (new)  : '),
(881, '2021-07-08 14:05:17', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(882, '2021-07-08 14:05:50', 'admin', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(883, '2021-07-08 14:05:51', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(884, '2021-07-08 14:06:49', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(885, '2021-07-08 14:07:38', 'admin', 'grid_actions_details_last', 'Scriptcase', '154.72.170.158', 'access', ''),
(886, '2021-07-08 14:09:53', 'admin', 'grid_messages', 'Scriptcase', '154.72.170.158', 'access', ''),
(887, '2021-07-08 14:09:59', 'admin', 'grid_message_groupe_push', 'Scriptcase', '154.72.170.158', 'access', ''),
(888, '2021-07-08 14:10:03', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(889, '2021-07-08 14:10:52', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(890, '2021-07-08 14:12:27', 'admin', 'form_actions_types', 'Scriptcase', '154.72.170.158', 'access', ''),
(891, '2021-07-08 14:13:02', 'admin', 'form_actions_types', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 7||--> fields <-- name (old)  : insert||name (new)  : Insert Ads'),
(892, '2021-07-08 14:13:17', 'admin', 'form_actions_types', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 9||--> fields <-- name (old)  : Episode||name (new)  : Episode WebSerie'),
(893, '2021-07-08 14:13:32', 'admin', 'form_actions_types', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 10||--> fields <-- name (old)  : Expérience sharing||name (new)  : Experience Sharing'),
(894, '2021-07-08 14:13:42', 'admin', 'form_actions_types', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 12||--> fields <-- name (old)  : Monday motivation||name (new)  : Monday Motivation'),
(895, '2021-07-08 14:13:58', 'admin', 'form_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(896, '2021-07-08 14:17:13', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(897, '2021-07-08 15:09:21', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(898, '2021-07-08 15:09:45', 'Anïs', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(899, '2021-07-08 15:09:46', 'Anïs', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(900, '2021-07-08 15:10:15', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(901, '2021-07-08 15:13:14', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(902, '2021-07-08 15:18:59', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 14||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 5||platform_id (new)  : 5||reaction_type_id (new)  : 2||text (new)  : Le njoh vous fait aussi ca?'),
(903, '2021-07-08 15:19:12', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(904, '2021-07-08 15:19:14', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(905, '2021-07-08 15:23:32', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 38||--> fields <-- user_id (new)  : 7||action_id (new)  : 14||kpi_id (new)  : 5||date_j (new)  : 2021-07-08||value (new)  : 13||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(906, '2021-07-08 15:23:51', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 39||--> fields <-- user_id (new)  : 7||action_id (new)  : 14||kpi_id (new)  : 4||date_j (new)  : 2021-07-08||value (new)  : 2||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(907, '2021-07-08 15:24:46', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 40||--> fields <-- user_id (new)  : 7||action_id (new)  : 14||kpi_id (new)  : 1||date_j (new)  : 2021-07-08||value (new)  : 53||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(908, '2021-07-08 15:24:59', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(909, '2021-07-08 15:41:40', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(910, '2021-07-08 16:03:11', 'Anïs', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(911, '2021-07-08 16:03:12', 'Anïs', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(912, '2021-07-08 16:03:43', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(913, '2021-07-08 16:07:14', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(914, '2021-07-08 16:07:25', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(915, '2021-07-08 16:07:27', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(916, '2021-07-08 16:08:41', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(917, '2021-07-08 16:12:37', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(918, '2021-07-08 16:13:01', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(919, '2021-07-08 16:13:06', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(920, '2021-07-08 16:14:40', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 41||--> fields <-- user_id (new)  : 7||action_id (new)  : 4||kpi_id (new)  : 5||date_j (new)  : 2021-07-08||value (new)  : 16959||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(921, '2021-07-08 16:15:05', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(922, '2021-07-08 16:43:40', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(923, '2021-07-08 16:48:59', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 15||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 5||platform_id (new)  : 5||reaction_type_id (new)  : 2||text (new)  : Avec MTN tous change '),
(924, '2021-07-08 16:49:10', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(925, '2021-07-08 16:49:12', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(926, '2021-07-08 16:49:53', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 42||--> fields <-- user_id (new)  : 7||action_id (new)  : 15||kpi_id (new)  : 1||date_j (new)  : 2021-07-08||value (new)  : 81||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(927, '2021-07-08 16:50:08', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 43||--> fields <-- user_id (new)  : 7||action_id (new)  : 15||kpi_id (new)  : 5||date_j (new)  : 2021-07-08||value (new)  : 18||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(928, '2021-07-08 16:57:33', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(929, '2021-07-08 16:58:03', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(930, '2021-07-08 16:58:10', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(931, '2021-07-08 16:58:11', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(932, '2021-07-08 17:03:25', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 44||--> fields <-- user_id (new)  : 7||action_id (new)  : 10||kpi_id (new)  : 4||date_j (new)  : 2021-07-08||value (new)  : 145||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(933, '2021-07-08 17:03:32', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(934, '2021-07-08 17:03:54', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(935, '2021-07-08 17:04:12', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(936, '2021-07-08 17:04:14', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(937, '2021-07-08 17:04:44', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 45||--> fields <-- user_id (new)  : 7||action_id (new)  : 13||kpi_id (new)  : 4||date_j (new)  : 2021-07-08||value (new)  : 98||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(938, '2021-07-08 17:04:55', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(939, '2021-07-08 17:05:23', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(940, '2021-07-08 17:05:38', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(941, '2021-07-08 17:05:40', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(942, '2021-07-08 17:06:21', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 46||--> fields <-- user_id (new)  : 7||action_id (new)  : 12||kpi_id (new)  : 4||date_j (new)  : 2021-07-08||value (new)  : 81||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(943, '2021-07-08 17:06:26', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(944, '2021-07-08 17:06:48', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(945, '2021-07-08 17:07:03', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(946, '2021-07-08 17:07:05', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(947, '2021-07-08 17:08:04', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 47||--> fields <-- user_id (new)  : 7||action_id (new)  : 9||kpi_id (new)  : 4||date_j (new)  : 2021-07-08||value (new)  : 203||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(948, '2021-07-08 17:09:02', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(949, '2021-07-08 17:10:05', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(950, '2021-07-08 17:10:21', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(951, '2021-07-08 17:10:24', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(952, '2021-07-08 17:11:10', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 48||--> fields <-- user_id (new)  : 7||action_id (new)  : 11||kpi_id (new)  : 4||date_j (new)  : 2021-07-08||value (new)  : 71||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(953, '2021-07-08 17:12:18', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(954, '2021-07-08 18:58:04', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(955, '2021-07-08 18:58:05', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(956, '2021-07-08 18:58:13', 'admin', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(957, '2021-07-08 18:58:14', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(958, '2021-07-08 18:58:22', 'admin', 'grid_message_user', 'Scriptcase', '154.72.170.158', 'access', ''),
(959, '2021-07-08 18:58:42', 'admin', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(960, '2021-07-08 18:58:44', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(961, '2021-07-08 18:59:18', 'admin', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(962, '2021-07-08 19:08:43', 'admin', 'form_companies', 'Scriptcase', '154.72.170.158', 'access', ''),
(963, '2021-07-09 04:34:20', '', 'sec_Login', 'Scriptcase', '154.72.168.37', 'access', ''),
(964, '2021-07-09 04:34:25', 'admin', 'sec_Login', 'User', '154.72.168.37', 'login', 'Connecté avec succès dans le système !'),
(965, '2021-07-09 04:34:26', 'admin', 'menu', 'Scriptcase', '154.72.168.37', 'access', ''),
(966, '2021-07-09 04:44:11', 'admin', 'grid_influencers', 'Scriptcase', '154.72.168.37', 'access', ''),
(967, '2021-07-09 11:50:34', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(968, '2021-07-09 11:50:53', 'Anïs', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(969, '2021-07-09 11:50:55', 'Anïs', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(970, '2021-07-09 11:51:20', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'access', ''),
(971, '2021-07-09 11:52:08', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 1||--> fields <-- name (new)  : YaMo||status (new)  : 1||created_at (new)  : '),
(972, '2021-07-09 11:52:24', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 2||--> fields <-- name (new)  : Ayoba||status (new)  : 1||created_at (new)  : '),
(973, '2021-07-09 11:52:49', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 3||--> fields <-- name (new)  : Yabadoo||status (new)  : 1||created_at (new)  : '),
(974, '2021-07-09 11:56:53', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 4||--> fields <-- name (new)  : MTN Home||status (new)  : 1||created_at (new)  : '),
(975, '2021-07-09 11:57:41', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 1||--> fields <-- name (old)  : YaMo||name (new)  : MTN YaMo'),
(976, '2021-07-09 11:58:05', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 5||--> fields <-- name (new)  : MTN Prestige||status (new)  : 1||created_at (new)  : '),
(977, '2021-07-09 11:58:22', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 6||--> fields <-- name (new)  : MTN Business||status (new)  : 1||created_at (new)  : '),
(978, '2021-07-09 11:58:46', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 7||--> fields <-- name (new)  : MTN Plus||status (new)  : 1||created_at (new)  : '),
(979, '2021-07-09 11:59:07', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 8||--> fields <-- name (new)  : MTN Magic||status (new)  : 1||created_at (new)  : '),
(980, '2021-07-09 12:07:16', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.158', 'insert', '--> keys <-- id : 9||--> fields <-- name (new)  : Wanda Net||status (new)  : 1||created_at (new)  : '),
(981, '2021-07-09 12:08:23', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(982, '2021-07-09 12:09:04', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(983, '2021-07-09 12:09:22', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(984, '2021-07-09 12:09:25', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(985, '2021-07-09 12:10:29', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(986, '2021-07-09 12:12:12', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 4||--> fields <-- name (old)  : Ayoba||name (new)  : Unbeatable Campaign'),
(987, '2021-07-09 12:12:32', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(988, '2021-07-09 12:12:47', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(989, '2021-07-09 12:12:49', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(990, '2021-07-09 12:13:12', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 15||--> fields <-- campaign_id (old)  : 5||campaign_id (new)  : 4||text (old)  : Avec MTN tous change ????||text (new)  : ||product_id (old)  : ||product_id (new)  : 1'),
(991, '2021-07-09 12:13:56', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 15||--> fields <-- text (old)  : ||text (new)  : Avec MTN tous change '),
(992, '2021-07-09 12:14:01', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(993, '2021-07-09 12:14:41', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(994, '2021-07-09 12:14:55', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(995, '2021-07-09 12:14:57', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(996, '2021-07-09 12:24:26', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 14||--> fields <-- campaign_id (old)  : 5||campaign_id (new)  : 4||text (old)  : Le njoh vous fait aussi ca????????????? code: *220*2#||text (new)  : Le njoh vous fait aussi ca? code: *220*2#||product_id (old)  : ||product_id (new)  : 1'),
(997, '2021-07-09 12:24:30', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(998, '2021-07-09 12:25:43', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(999, '2021-07-09 12:25:54', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1000, '2021-07-09 12:25:56', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1001, '2021-07-09 12:26:14', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 13||--> fields <-- product_id (old)  : ||product_id (new)  : 8'),
(1002, '2021-07-09 12:26:18', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1003, '2021-07-09 12:26:37', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1004, '2021-07-09 12:26:59', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1005, '2021-07-09 12:27:01', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1006, '2021-07-09 12:27:15', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 12||--> fields <-- product_id (old)  : ||product_id (new)  : 8'),
(1007, '2021-07-09 12:27:25', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1008, '2021-07-09 12:27:55', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1009, '2021-07-09 12:28:12', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1010, '2021-07-09 12:28:18', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1011, '2021-07-09 12:28:29', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 11||--> fields <-- product_id (old)  : ||product_id (new)  : 8'),
(1012, '2021-07-09 12:28:37', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1013, '2021-07-09 12:28:57', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1014, '2021-07-09 12:29:08', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1015, '2021-07-09 12:29:09', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1016, '2021-07-09 12:29:22', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 10||--> fields <-- product_id (old)  : ||product_id (new)  : 8'),
(1017, '2021-07-09 12:29:28', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1018, '2021-07-09 12:29:47', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1019, '2021-07-09 12:30:02', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1020, '2021-07-09 12:30:05', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1021, '2021-07-09 12:30:22', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 9||--> fields <-- product_id (old)  : ||product_id (new)  : 8'),
(1022, '2021-07-09 12:30:27', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1023, '2021-07-09 12:31:02', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1024, '2021-07-09 12:31:14', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1025, '2021-07-09 12:31:17', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1026, '2021-07-09 12:31:27', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 8||--> fields <-- product_id (old)  : ||product_id (new)  : 2'),
(1027, '2021-07-09 12:31:31', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1028, '2021-07-09 12:31:58', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1029, '2021-07-09 12:32:12', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', '');
INSERT INTO `sc_log` (`id`, `inserted_date`, `username`, `application`, `creator`, `ip_user`, `action`, `description`) VALUES
(1030, '2021-07-09 12:32:14', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1031, '2021-07-09 12:32:32', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 7||--> fields <-- product_id (old)  : ||product_id (new)  : 2'),
(1032, '2021-07-09 12:32:37', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1033, '2021-07-09 12:33:01', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1034, '2021-07-09 12:33:19', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1035, '2021-07-09 12:33:21', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1036, '2021-07-09 12:34:25', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 6||--> fields <-- campaign_id (old)  : 7||campaign_id (new)  : 4||text (old)  : L’Internet IMBATTABLE avec @mtncameroon Home pour toute la famille à partir de 15,000frs.\nC’est la magie ohh\nQui veut les data???||text (new)  : ||product_id (old)  : ||product_id (new)  : 4'),
(1037, '2021-07-09 12:34:28', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1038, '2021-07-09 12:34:59', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1039, '2021-07-09 12:35:12', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1040, '2021-07-09 12:35:16', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1041, '2021-07-09 12:35:40', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 5||--> fields <-- campaign_id (old)  : 7||campaign_id (new)  : 4||product_id (old)  : ||product_id (new)  : 4'),
(1042, '2021-07-09 12:35:43', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1043, '2021-07-09 12:36:33', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1044, '2021-07-09 12:36:47', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.158', 'access', ''),
(1045, '2021-07-09 12:36:51', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.158', 'access', ''),
(1046, '2021-07-09 12:36:55', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.158', 'update', '--> keys <-- id : 6||--> fields <-- text (old)  : ||text (new)  : L’Internet IMBATTABLE avec Mtn Home pour toute la famille à partir de 15,000frs.\nC’est la magie ohh \nQui veut les data???\nNdedi Eyango KingmouannkumOffiel\nAlene Menget'),
(1047, '2021-07-09 12:37:02', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1048, '2021-07-09 14:02:52', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(1049, '2021-07-09 14:03:04', 'admin', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(1050, '2021-07-09 14:03:04', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(1051, '2021-07-09 14:16:01', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(1052, '2021-07-09 14:16:20', 'admin', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(1053, '2021-07-09 14:16:21', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(1054, '2021-07-09 14:16:42', 'admin', 'chart_vues_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1055, '2021-07-09 14:16:42', 'admin', 'chart_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1056, '2021-07-09 14:16:42', 'admin', 'chart_engagements_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1057, '2021-07-09 14:16:48', 'admin', 'chart_actions_details_canneaux', 'Scriptcase', '154.72.170.158', 'access', ''),
(1058, '2021-07-09 14:16:48', 'admin', 'chart_actions_canneau', 'Scriptcase', '154.72.170.158', 'access', ''),
(1059, '2021-07-09 14:16:48', 'admin', 'chart_likes_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1060, '2021-07-09 14:17:16', 'admin', 'grid_vue_engagement_influenceurs', 'Scriptcase', '154.72.170.158', 'access', ''),
(1061, '2021-07-09 14:20:35', 'admin', 'grid_vue_engagement_influenceurs', 'Scriptcase', '154.72.170.158', 'access', ''),
(1062, '2021-07-09 14:20:56', 'admin', 'chart_engegement_kpi', 'Scriptcase', '154.72.170.158', 'access', ''),
(1063, '2021-07-09 14:20:57', 'admin', 'chart_engegement_products', 'Scriptcase', '154.72.170.158', 'access', ''),
(1064, '2021-07-09 14:20:58', 'admin', 'chart_engegement_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1065, '2021-07-09 14:21:01', 'admin', 'chart_engegement_campagne', 'Scriptcase', '154.72.170.158', 'access', ''),
(1066, '2021-07-09 14:27:57', 'admin', 'chart_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1067, '2021-07-09 14:27:57', 'admin', 'chart_engagements_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1068, '2021-07-09 14:27:58', 'admin', 'chart_actions_canneau', 'Scriptcase', '154.72.170.158', 'access', ''),
(1069, '2021-07-09 14:27:59', 'admin', 'chart_actions_details_canneaux', 'Scriptcase', '154.72.170.158', 'access', ''),
(1070, '2021-07-09 14:28:00', 'admin', 'chart_vues_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1071, '2021-07-09 14:28:06', 'admin', 'chart_likes_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1072, '2021-07-09 14:28:14', 'admin', 'chart_engegement_campagne', 'Scriptcase', '154.72.170.158', 'access', ''),
(1073, '2021-07-09 14:28:14', 'admin', 'chart_engegement_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1074, '2021-07-09 14:29:17', '', 'chart_engegement_kpi', 'Scriptcase', '154.72.170.158', 'access', ''),
(1075, '2021-07-09 14:29:17', '', 'chart_engegement_products', 'Scriptcase', '154.72.170.158', 'access', ''),
(1076, '2021-07-09 14:29:29', 'admin', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(1077, '2021-07-09 14:30:08', 'admin', 'chart_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1078, '2021-07-09 14:30:08', 'admin', 'chart_engagements_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1079, '2021-07-09 14:30:08', 'admin', 'chart_likes_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1080, '2021-07-09 14:30:08', 'admin', 'chart_vues_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1081, '2021-07-09 14:30:09', 'admin', 'chart_actions_canneau', 'Scriptcase', '154.72.170.158', 'access', ''),
(1082, '2021-07-09 14:30:09', 'admin', 'chart_actions_details_canneaux', 'Scriptcase', '154.72.170.158', 'access', ''),
(1083, '2021-07-09 14:31:34', 'admin', 'grid_vue_engagement_influenceurs', 'Scriptcase', '154.72.170.158', 'access', ''),
(1084, '2021-07-09 14:33:00', 'admin', 'chart_engegement_products', 'Scriptcase', '154.72.170.158', 'access', ''),
(1085, '2021-07-09 14:33:01', 'admin', 'chart_engegement_campagne', 'Scriptcase', '154.72.170.158', 'access', ''),
(1086, '2021-07-09 14:33:01', 'admin', 'chart_engegement_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1087, '2021-07-09 14:33:01', 'admin', 'chart_engegement_kpi', 'Scriptcase', '154.72.170.158', 'access', ''),
(1088, '2021-07-09 14:35:20', 'admin', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1089, '2021-07-09 14:50:02', '', 'sec_Login', 'Scriptcase', '154.72.170.158', 'access', ''),
(1090, '2021-07-09 14:50:43', 'Anïs', 'sec_Login', 'User', '154.72.170.158', 'login', 'Connecté avec succès dans le système !'),
(1091, '2021-07-09 14:50:44', 'Anïs', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(1092, '2021-07-09 14:51:02', 'Anïs', 'chart_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1093, '2021-07-09 14:51:03', 'Anïs', 'chart_vues_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1094, '2021-07-09 14:51:03', 'Anïs', 'chart_actions_canneau', 'Scriptcase', '154.72.170.158', 'access', ''),
(1095, '2021-07-09 14:51:04', 'Anïs', 'chart_engagements_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1096, '2021-07-09 14:51:04', 'Anïs', 'chart_actions_details_canneaux', 'Scriptcase', '154.72.170.158', 'access', ''),
(1097, '2021-07-09 14:51:07', 'Anïs', 'chart_likes_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1098, '2021-07-09 14:56:05', 'Anïs', 'menu', 'Scriptcase', '154.72.170.158', 'access', ''),
(1099, '2021-07-09 14:56:24', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.170.158', 'access', ''),
(1100, '2021-07-09 14:56:27', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1101, '2021-07-09 14:57:16', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.170.158', 'access', ''),
(1102, '2021-07-09 14:57:27', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.170.158', 'access', ''),
(1103, '2021-07-09 14:57:39', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(1104, '2021-07-09 14:57:42', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.170.158', 'access', ''),
(1105, '2021-07-09 14:57:43', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.170.158', 'access', ''),
(1106, '2021-07-09 15:00:07', 'Anïs', 'grid_vue_engagement_influenceurs', 'Scriptcase', '154.72.170.158', 'access', ''),
(1107, '2021-07-09 15:00:07', 'Anïs', 'chart_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1108, '2021-07-09 15:00:08', 'Anïs', 'chart_vues_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1109, '2021-07-09 15:00:09', 'Anïs', 'chart_engagements_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1110, '2021-07-09 15:00:10', 'Anïs', 'chart_actions_canneau', 'Scriptcase', '154.72.170.158', 'access', ''),
(1111, '2021-07-09 15:00:11', 'Anïs', 'chart_actions_details_canneaux', 'Scriptcase', '154.72.170.158', 'access', ''),
(1112, '2021-07-09 15:00:11', 'Anïs', 'chart_likes_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1113, '2021-07-09 15:02:16', 'Anïs', 'chart_engegement_products', 'Scriptcase', '154.72.170.158', 'access', ''),
(1114, '2021-07-09 15:02:17', 'Anïs', 'chart_engegement_campagne', 'Scriptcase', '154.72.170.158', 'access', ''),
(1115, '2021-07-09 15:02:18', 'Anïs', 'chart_engegement_actions', 'Scriptcase', '154.72.170.158', 'access', ''),
(1116, '2021-07-09 15:02:19', 'Anïs', 'chart_engegement_kpi', 'Scriptcase', '154.72.170.158', 'access', ''),
(1117, '2021-07-12 10:17:56', '', 'sec_Login', 'Scriptcase', '154.72.168.155', 'access', ''),
(1118, '2021-07-12 10:18:13', '', 'sec_Login', 'Scriptcase', '41.244.244.5', 'access', ''),
(1119, '2021-07-12 10:18:14', 'Anïs', 'sec_Login', 'User', '154.72.168.155', 'login', 'Connecté avec succès dans le système !'),
(1120, '2021-07-12 10:18:15', 'Anïs', 'menu', 'Scriptcase', '154.72.168.155', 'access', ''),
(1121, '2021-07-12 10:18:16', '', 'sec_Login', 'Scriptcase', '41.244.244.5', 'access', ''),
(1122, '2021-07-12 10:18:22', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1123, '2021-07-12 10:19:08', '', 'sec_Login', 'User', '41.244.244.5', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateur'),
(1124, '2021-07-12 10:20:07', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1125, '2021-07-12 10:20:16', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1126, '2021-07-12 10:25:37', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1127, '2021-07-12 10:26:57', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1128, '2021-07-12 10:26:57', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1129, '2021-07-12 10:27:40', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1130, '2021-07-12 10:30:28', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1131, '2021-07-12 10:31:48', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1132, '2021-07-12 10:32:19', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1133, '2021-07-12 10:33:00', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1134, '2021-07-12 10:35:47', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 20||--> fields <-- company_id (new)  : 1||category_id (new)  : 3||name (new)  : Ma\'a jacky||full_name (new)  : Ma\'a jacky||email (new)  : Ma\'a jacky@gmail.com||phone (new)  : 677777777||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(1135, '2021-07-12 10:37:22', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1136, '2021-07-12 10:41:00', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1137, '2021-07-12 10:43:42', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1138, '2021-07-12 10:45:14', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1139, '2021-07-12 10:46:13', 'Anïs', 'form_categories', 'Scriptcase', '154.72.168.155', 'access', ''),
(1140, '2021-07-12 10:46:21', 'Anïs', 'form_campaigns', 'Scriptcase', '154.72.168.155', 'access', ''),
(1141, '2021-07-12 10:46:27', 'Anïs', 'form_actions_types', 'Scriptcase', '154.72.168.155', 'access', ''),
(1142, '2021-07-12 10:48:13', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1143, '2021-07-12 10:48:13', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1144, '2021-07-12 10:48:14', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1145, '2021-07-12 10:48:21', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1146, '2021-07-12 10:49:29', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.155', 'access', ''),
(1147, '2021-07-12 10:49:58', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.168.155', 'access', ''),
(1148, '2021-07-12 10:52:02', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 22||--> fields <-- start_date (new)  : 2021-07-09||end_date (new)  : 2021-10-09||description (new)  : Contrat entre OS et Ma\'a Jacky||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 20||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(1149, '2021-07-12 10:52:33', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.155', 'access', ''),
(1150, '2021-07-12 10:52:36', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.155', 'access', ''),
(1151, '2021-07-12 10:52:40', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.168.155', 'access', ''),
(1152, '2021-07-12 10:54:06', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.155', 'access', ''),
(1153, '2021-07-12 10:54:50', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1154, '2021-07-12 10:56:09', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1155, '2021-07-12 11:06:01', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 0||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 0||platform_id (new)  : 0||reaction_type_id (new)  : 3||text (new)  : Nouvelle collaboration avec MTN Cameroon \r\nDisons merci à Dieu\r\nIgwesu!||link (new)  : https://scontent-lhr8-1.xx.fbcdn.net/v/t39.30808-6/216026872_352934103079259_5280819255147809554_n.jpg?_nc_cat=106&ccb=1-3&_nc_sid=8bfeb9&_nc_eui2=AeEVfpRsRJ1ODTP7fIWKdvM0woYmfit0CnTChiZ-K3QKdDUbR6LtQiVrvXB3Nndu-hhGXMrlj5EboTMkEkPL42V0&_nc_ohc=JgHXElLC0ic||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 22||product_id (new)  : 1||Fichier  (new)  : ||detail (new)  : '),
(1156, '2021-07-12 11:07:11', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 0||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 0||platform_id (new)  : 0||reaction_type_id (new)  : 3||text (new)  : Nouvelle collaboration avec MTN Cameroon \r\nDisons merci à Dieu\r\nIgwesu!||link (new)  : https://scontent-lhr8-1.xx.fbcdn.net/v/t39.30808-6/216026872_352934103079259_5280819255147809554_n.jpg?_nc_cat=106&ccb=1-3&_nc_sid=8bfeb9&_nc_eui2=AeEVfpRsRJ1ODTP7fIWKdvM0woYmfit0CnTChiZ-K3QKdDUbR6LtQiVrvXB3Nndu-hhGXMrlj5EboTMkEkPL42V0&_nc_ohc=JgHXElLC0ic||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 22||product_id (new)  : 1||Fichier  (new)  : ||detail (new)  : '),
(1157, '2021-07-12 11:18:23', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1158, '2021-07-12 11:20:36', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1159, '2021-07-12 11:23:05', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 18||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 5||reaction_type_id (new)  : 3||text (new)  : Nouvelle collaboration avec MTN Cameroon \r\nDisons merci à Dieu\r\nIgwesu!||link (new)  : https://scontent-lhr8-1.xx.fbcdn.net/v/t39.30808-6/216026872_352934103079259_5280819255147809554_n.jpg?_nc_cat=106&ccb=1-3&_nc_sid=8bfeb9&_nc_eui2=AeEVfpRsRJ1ODTP7fIWKdvM0woYmfit0CnTChiZ-K3QKdDUbR6LtQiVrvXB3Nndu-hhGXMrlj5EboTMkEkPL42V0&_nc_ohc=JgHXElLC0ic||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 22||product_id (new)  : 1||Fichier  (new)  : ||detail (new)  : '),
(1160, '2021-07-12 11:23:43', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'access', ''),
(1161, '2021-07-12 11:23:46', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'access', ''),
(1162, '2021-07-12 11:24:56', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 49||--> fields <-- user_id (new)  : 7||action_id (new)  : 18||kpi_id (new)  : 4||date_j (new)  : 2021-07-12||value (new)  : 5000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1163, '2021-07-12 11:25:38', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 50||--> fields <-- user_id (new)  : 7||action_id (new)  : 18||kpi_id (new)  : 5||date_j (new)  : 2021-07-12||value (new)  : 62000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1164, '2021-07-12 11:26:04', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 51||--> fields <-- user_id (new)  : 7||action_id (new)  : 18||kpi_id (new)  : 3||date_j (new)  : 2021-07-12||value (new)  : 146||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1165, '2021-07-12 11:26:19', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'update', '--> keys <-- id : 18||--> fields <-- text (old)  : Nouvelle collaboration avec MTN Cameroon \r\nDisons merci à Dieu\r\nIgwesu!||text (new)  : Nouvelle collaboration avec MTN Cameroon \nDisons merci à Dieu\nIgwesu!'),
(1166, '2021-07-12 11:26:34', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1167, '2021-07-12 11:27:22', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1168, '2021-07-12 11:27:48', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'access', ''),
(1169, '2021-07-12 11:27:53', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'access', ''),
(1170, '2021-07-12 11:51:36', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 3||--> fields <-- action_id (new)  : 18||type (new)  : Images||link (new)  : 215812257_352934223079247_1191266640396946237_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1171, '2021-07-12 11:51:55', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 4||--> fields <-- action_id (new)  : 18||type (new)  : Images||link (new)  : 214852297_352934089745927_2279933334192918930_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1172, '2021-07-12 11:52:14', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 5||--> fields <-- action_id (new)  : 18||type (new)  : Images||link (new)  : 216026872_352934103079259_5280819255147809554_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1173, '2021-07-12 11:52:35', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 6||--> fields <-- action_id (new)  : 18||type (new)  : Images||link (new)  : 216034267_352934253079244_2093664310384675475_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1174, '2021-07-12 11:53:45', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1175, '2021-07-12 12:17:30', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1176, '2021-07-12 12:18:50', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 19||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 4||reaction_type_id (new)  : 3||text (new)  : Nouveau contrat avec @mtncameroon||link (new)  : https://www.instagram.com/p/CRHvd9uFpXb/?utm_source=ig_web_copy_link||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 22||product_id (new)  : 1||Fichier  (new)  : ||detail (new)  : '),
(1177, '2021-07-12 12:19:08', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'access', ''),
(1178, '2021-07-12 12:19:18', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'access', ''),
(1179, '2021-07-12 12:22:15', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 7||--> fields <-- action_id (new)  : 19||type (new)  : Images||link (new)  : 214852297_352934089745927_2279933334192918930_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1180, '2021-07-12 12:22:43', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 8||--> fields <-- action_id (new)  : 19||type (new)  : Images||link (new)  : 215701110_352934193079250_5291100503327127319_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1181, '2021-07-12 12:23:11', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 9||--> fields <-- action_id (new)  : 19||type (new)  : Images||link (new)  : 215812257_352934223079247_1191266640396946237_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1182, '2021-07-12 12:23:40', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 10||--> fields <-- action_id (new)  : 19||type (new)  : Images||link (new)  : 216026872_352934103079259_5280819255147809554_n.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1183, '2021-07-12 12:24:55', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 52||--> fields <-- user_id (new)  : 7||action_id (new)  : 19||kpi_id (new)  : 5||date_j (new)  : 2021-07-12||value (new)  : 3553||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1184, '2021-07-12 12:25:22', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 53||--> fields <-- user_id (new)  : 7||action_id (new)  : 19||kpi_id (new)  : 4||date_j (new)  : 2021-07-12||value (new)  : 78||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1185, '2021-07-12 12:28:46', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1186, '2021-07-12 12:31:33', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1187, '2021-07-12 12:31:42', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'access', ''),
(1188, '2021-07-12 12:31:45', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'access', ''),
(1189, '2021-07-12 12:31:51', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'update', '--> keys <-- id : 18||--> fields <-- platform_id (old)  : 5||platform_id (new)  : 1'),
(1190, '2021-07-12 12:36:56', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1191, '2021-07-12 12:38:01', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1192, '2021-07-12 13:19:27', '', 'sec_Login', 'Scriptcase', '154.72.168.155', 'access', ''),
(1193, '2021-07-12 13:20:01', 'Anïs', 'sec_Login', 'User', '154.72.168.155', 'login', 'Connecté avec succès dans le système !'),
(1194, '2021-07-12 13:20:02', 'Anïs', 'menu', 'Scriptcase', '154.72.168.155', 'access', ''),
(1195, '2021-07-12 13:23:16', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1196, '2021-07-12 13:25:54', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1197, '2021-07-12 13:38:48', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 20||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 2||text (new)  : Y\'ello!\r\nIt\'s holiday time! For the 4th YaMo Class, we\'re going to talk about \"how to make money with agriculture\". Stop everything, because Flavien Kouatcha, our expert, is going to meet you this saturday, July 10th from 11am LIVE on our Facebook page. Come and get practical tips on how to get started and do business.\r\n||link (new)  : https://fb.watch/v/18slEMMTI/||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 4||product_id (new)  : 1||Fichier  (new)  : ||detail (new)  : '),
(1198, '2021-07-12 13:38:59', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'access', ''),
(1199, '2021-07-12 13:39:06', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'access', ''),
(1200, '2021-07-12 13:39:54', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 54||--> fields <-- user_id (new)  : 7||action_id (new)  : 20||kpi_id (new)  : 5||date_j (new)  : 2021-07-12||value (new)  : 101||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1201, '2021-07-12 13:40:20', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 55||--> fields <-- user_id (new)  : 7||action_id (new)  : 20||kpi_id (new)  : 4||date_j (new)  : 2021-07-12||value (new)  : 13||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1202, '2021-07-12 13:40:48', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 56||--> fields <-- user_id (new)  : 7||action_id (new)  : 20||kpi_id (new)  : 3||date_j (new)  : 2021-07-12||value (new)  : 15||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1203, '2021-07-12 13:40:57', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'update', '--> keys <-- id : 20||--> fields <-- text (old)  : Y\'ello!\r\nIt\'s holiday time! For the 4th YaMo Class, we\'re going to talk about \"how to make money with agriculture\". Stop everything, because Flavien Kouatcha, our expert, is going to meet you this saturday, July 10th from 11am LIVE on our Facebook page. Come and get practical tips on how to get started and do business.\r\n||text (new)  : Y\'ello!\nIt\'s holiday time! For the 4th YaMo Class, we\'re going to talk about \"how to make money with agriculture\". Stop everything, because Flavien Kouatcha, our expert, is going to meet you this saturday, July 10th from 11am LIVE on our Facebook page. Come and get practical tips on how to get started and do business.\n'),
(1204, '2021-07-12 13:41:00', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1205, '2021-07-13 09:09:18', '', 'sec_Login', 'Scriptcase', '154.72.168.155', 'access', ''),
(1206, '2021-07-13 09:09:34', 'Anïs', 'sec_Login', 'User', '154.72.168.155', 'login', 'Connecté avec succès dans le système !'),
(1207, '2021-07-13 09:09:35', 'Anïs', 'menu', 'Scriptcase', '154.72.168.155', 'access', ''),
(1208, '2021-07-13 09:10:15', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1209, '2021-07-13 12:50:25', '', 'sec_Login', 'Scriptcase', '154.72.168.155', 'access', ''),
(1210, '2021-07-13 12:50:34', 'Anïs', 'sec_Login', 'User', '154.72.168.155', 'login', 'Connecté avec succès dans le système !'),
(1211, '2021-07-13 12:50:35', 'Anïs', 'menu', 'Scriptcase', '154.72.168.155', 'access', ''),
(1212, '2021-07-13 12:50:56', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1213, '2021-07-13 12:51:13', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1214, '2021-07-13 12:52:17', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1215, '2021-07-13 12:52:55', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1216, '2021-07-13 12:54:25', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 21||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Allenne Menget (Mr Ni)||full_name (new)  : Allenne Menget (Mr Ni)||email (new)  : AllenneMenget(Mr Ni)@gmail.com||phone (new)  : 679797979||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(1217, '2021-07-13 12:54:45', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.168.155', 'access', ''),
(1218, '2021-07-13 12:55:02', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.168.155', 'access', ''),
(1219, '2021-07-13 12:55:19', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.168.155', 'access', ''),
(1220, '2021-07-13 12:56:21', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 23||--> fields <-- start_date (new)  : 2021-06-01||end_date (new)  : 2022-06-01||description (new)  : Contrat entre MTN et Allenne Menget||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 21||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(1221, '2021-07-13 12:56:37', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.168.155', 'access', ''),
(1222, '2021-07-13 12:56:38', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.168.155', 'access', ''),
(1223, '2021-07-13 12:56:41', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.168.155', 'access', ''),
(1224, '2021-07-13 12:56:43', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1225, '2021-07-13 13:04:30', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 21||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 13||text (new)  : GIVE AWAY\r\nAnyone who is the first to write Y in brackets will have 5000frs MTN airtime from MTN ambassador Ni Alenne Menget Ats.YELLOW IS BEST.||link (new)  : https://www.facebook.com/alenne.menget/posts/4716207898411916&||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 23||product_id (new)  : 7||Fichier  (new)  : ||detail (new)  : '),
(1226, '2021-07-13 13:04:36', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.168.155', 'access', ''),
(1227, '2021-07-13 13:04:37', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'access', ''),
(1228, '2021-07-13 13:05:46', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 57||--> fields <-- user_id (new)  : 7||action_id (new)  : 21||kpi_id (new)  : 5||date_j (new)  : 2021-07-13||value (new)  : 77||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1229, '2021-07-13 13:06:15', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 58||--> fields <-- user_id (new)  : 7||action_id (new)  : 21||kpi_id (new)  : 4||date_j (new)  : 2021-07-13||value (new)  : 166||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1230, '2021-07-13 13:06:42', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.168.155', 'insert', '--> keys <-- id : 59||--> fields <-- user_id (new)  : 7||action_id (new)  : 21||kpi_id (new)  : 3||date_j (new)  : 2021-07-13||value (new)  : 18||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1231, '2021-07-13 13:06:53', 'Anïs', 'form_actions', 'Scriptcase', '154.72.168.155', 'update', '--> keys <-- id : 21||--> fields <-- text (old)  : GIVE AWAY\r\nAnyone who is the first to write Y in brackets will have 5000frs MTN airtime from MTN ambassador Ni Alenne Menget Ats.YELLOW IS BEST.||text (new)  : GIVE AWAY\nAnyone who is the first to write Y in brackets will have 5000frs MTN airtime from MTN ambassador Ni Alenne Menget Ats.YELLOW IS BEST.'),
(1232, '2021-07-13 13:07:00', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.155', 'access', ''),
(1233, '2021-07-14 17:41:12', '', 'sec_Login', 'Scriptcase', '154.72.170.4', 'access', ''),
(1234, '2021-07-15 09:15:28', '', 'sec_Login', 'Scriptcase', '154.72.170.4', 'access', ''),
(1235, '2021-07-15 09:15:37', 'Anïs', 'sec_Login', 'User', '154.72.170.4', 'login', 'Connecté avec succès dans le système !'),
(1236, '2021-07-15 09:15:38', 'Anïs', 'menu', 'Scriptcase', '154.72.170.4', 'access', ''),
(1237, '2021-07-15 09:27:12', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1238, '2021-07-15 09:27:45', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1239, '2021-07-15 09:29:13', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 22||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 2||reaction_type_id (new)  : 2||text (new)  : MTN Home: Découvrez l\'internet à domicile moins cher de MTN Cameroon | ECHOSTECH | TELE\'ASU||link (new)  : https://www.youtube.com/watch?v=AhIKWbuDq_o&t=25s||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 18||product_id (new)  : 4||Fichier  (new)  : ||detail (new)  : '),
(1240, '2021-07-15 09:29:37', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1241, '2021-07-15 09:29:49', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1242, '2021-07-15 09:31:18', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 60||--> fields <-- user_id (new)  : 7||action_id (new)  : 22||kpi_id (new)  : 1||date_j (new)  : 2021-07-15||value (new)  : 32||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1243, '2021-07-15 09:31:43', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 61||--> fields <-- user_id (new)  : 7||action_id (new)  : 22||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 0||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1244, '2021-07-15 09:32:04', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 60||--> fields <-- user_id (old)  : ||user_id (new)  : 7||action_id (old)  : ||action_id (new)  : 22'),
(1245, '2021-07-15 09:32:18', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 62||--> fields <-- user_id (new)  : 7||action_id (new)  : 22||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 0||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1246, '2021-07-15 09:32:34', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1247, '2021-07-15 09:43:06', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1248, '2021-07-15 09:51:03', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 23||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 8||text (new)  : Bonjour ici . ✈️✈️✈️✈️||link (new)  : https://www.facebook.com/photo/?fbid=353003396196087&set=a.276310033865424&__cft__[0]=AZXebTfIDIhdo0Tv3hl9Hrx5-VFAtHd3nXFxQ_fsRIhCHySNPidUv8IK_DHnqN3iz0KG6-DV505mof4rk7j9Quq2Tt9eX0g9-XvMdj6k1CEgFat-6pMTInNiQnHrhVTQgv5mWj8DbBh7t3U7GayNOoWg&__tn__=EH-R||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||product_id (new)  : 8||Fichier  (new)  : ||detail (new)  : '),
(1249, '2021-07-15 09:51:17', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1250, '2021-07-15 09:51:21', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1251, '2021-07-15 09:52:10', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 11||--> fields <-- action_id (new)  : 23||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 114940.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1252, '2021-07-15 09:52:47', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 63||--> fields <-- user_id (new)  : 7||action_id (new)  : 23||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 15000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1253, '2021-07-15 09:53:15', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 64||--> fields <-- user_id (new)  : 7||action_id (new)  : 23||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 722||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1254, '2021-07-15 09:53:42', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 65||--> fields <-- user_id (new)  : 7||action_id (new)  : 23||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 15||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1255, '2021-07-15 09:53:55', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1256, '2021-07-15 10:02:57', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1257, '2021-07-15 10:06:56', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 24||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 1||reaction_type_id (new)  : 3||text (new)  : Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic. \r\nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim. \r\nBimoulee d’abord !!!||link (new)  : https://www.facebook.com/photo/?fbid=353588536137573&set=a.276310040532090&__cft__[0]=AZVEPMdc-4BO2pnbEvrf_qRMgUiLNOlTz36JI0D5tzyBxtRKBr4MT23erKmWGFCb5JTOf5mCv-7GsgQ7NnfWVTJ5WGNmTfMWtpua3MTBEtrz1DnmDWbCyKAyZzmOt__ChycAMkYjYQY-betYc_WR8VUL&__tn__=EH-R||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||product_id (new)  : 8||Fichier  (new)  : ||detail (new)  : '),
(1258, '2021-07-15 10:07:21', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1259, '2021-07-15 10:07:23', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1260, '2021-07-15 10:08:22', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 12||--> fields <-- action_id (new)  : 24||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 120517.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1261, '2021-07-15 10:08:47', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 66||--> fields <-- user_id (new)  : 7||action_id (new)  : 24||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 18||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1262, '2021-07-15 10:09:06', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 67||--> fields <-- user_id (new)  : 7||action_id (new)  : 24||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 687||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1263, '2021-07-15 10:09:41', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 68||--> fields <-- user_id (new)  : 7||action_id (new)  : 24||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 22||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1264, '2021-07-15 10:09:46', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 24||--> fields <-- text (old)  : Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic. \r\nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim. \r\nBimoulee d’abord !!!||text (new)  : Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic. \nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim. \nBimoulee d’abord !!!'),
(1265, '2021-07-15 10:09:53', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1266, '2021-07-15 10:11:56', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1267, '2021-07-15 10:13:43', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 25||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 4||reaction_type_id (new)  : 3||text (new)  : Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic.\r\n\r\nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim.\r\n\r\nBimoulee d’abord !!!||link (new)  : https://www.instagram.com/p/CRTICi4llQJ/?utm_source=ig_web_copy_link||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||product_id (new)  : 8||Fichier  (new)  : ||detail (new)  : '),
(1268, '2021-07-15 10:14:06', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1269, '2021-07-15 10:14:09', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1270, '2021-07-15 10:14:34', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 13||--> fields <-- action_id (new)  : 25||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 120517.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1271, '2021-07-15 10:15:17', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 69||--> fields <-- user_id (new)  : 7||action_id (new)  : 25||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 7342||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1272, '2021-07-15 10:15:32', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 70||--> fields <-- user_id (new)  : 7||action_id (new)  : 25||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 110||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1273, '2021-07-15 10:16:01', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 25||--> fields <-- text (old)  : Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic.\r\n\r\nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim.\r\n\r\nBimoulee d’abord !!!||text (new)  : Bien le bonjour à tous les abonnés MTN , si vous n’êtes pas encore abonnés il est temps de s’abonner et de souscrire au forfait MTN Magic.\n\nBien le bonjour aussi à mes amis de Dubaï , je suis déjà là et j’ai faim.\n\nBimoulee d’abord !!!'),
(1274, '2021-07-15 10:16:32', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1275, '2021-07-15 10:17:21', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1276, '2021-07-15 10:18:42', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 26||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 4||reaction_type_id (new)  : 3||text (new)  : ||link (new)  : https://www.instagram.com/p/CRRBEGiFjsS/?utm_source=ig_web_copy_link||status (new)  : 1||tags (new)  : @valerie_ayena ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||product_id (new)  : 8||Fichier  (new)  : ||detail (new)  : '),
(1277, '2021-07-15 10:19:01', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1278, '2021-07-15 10:19:05', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1279, '2021-07-15 10:19:45', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 14||--> fields <-- action_id (new)  : 26||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 114940.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1280, '2021-07-15 10:20:57', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 71||--> fields <-- user_id (new)  : 7||action_id (new)  : 26||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 4091||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1281, '2021-07-15 10:21:27', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 72||--> fields <-- user_id (new)  : 7||action_id (new)  : 26||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 4091||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1282, '2021-07-15 10:21:43', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 71||--> fields <-- user_id (old)  : ||user_id (new)  : 7||action_id (old)  : ||action_id (new)  : 26||value (old)  : 4091||value (new)  : 63'),
(1283, '2021-07-15 10:23:28', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1284, '2021-07-15 10:24:42', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1285, '2021-07-15 10:27:03', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 27||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 1||reaction_type_id (new)  : 2||text (new)  : Ma grande sœur chérie Valerie Ayena a un mapane ce soir , je suis passé me rassurer qu’elle était prêt avant de retourner vérifier que les clients de MTN qui ont tapé *211# et qui ont gagné un séjour à Dubaï sont entrain de bien enjoy le moment. \r\nBimoulee d’abord !!!||link (new)  : https://fb.watch/v/Mru3NO3u/||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||product_id (new)  : 8||Fichier  (new)  : ||detail (new)  : '),
(1286, '2021-07-15 10:27:32', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1287, '2021-07-15 10:27:35', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1288, '2021-07-15 10:29:02', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 73||--> fields <-- user_id (new)  : 7||action_id (new)  : 27||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 2570||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1289, '2021-07-15 10:29:24', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 74||--> fields <-- user_id (new)  : 7||action_id (new)  : 27||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 90||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1290, '2021-07-15 10:29:45', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 75||--> fields <-- user_id (new)  : 7||action_id (new)  : 27||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 6||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1291, '2021-07-15 10:29:54', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 27||--> fields <-- text (old)  : Ma grande sœur chérie Valerie Ayena a un mapane ce soir , je suis passé me rassurer qu’elle était prêt avant de retourner vérifier que les clients de MTN qui ont tapé *211# et qui ont gagné un séjour à Dubaï sont entrain de bien enjoy le moment. \r\nBimoulee d’abord !!!||text (new)  : Ma grande sœur chérie Valerie Ayena a un mapane ce soir , je suis passé me rassurer qu’elle était prêt avant de retourner vérifier que les clients de MTN qui ont tapé *211# et qui ont gagné un séjour à Dubaï sont entrain de bien enjoy le moment. \nBimoulee d’abord !!!'),
(1292, '2021-07-15 10:30:43', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1293, '2021-07-15 10:31:04', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1294, '2021-07-15 10:32:39', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 28||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 4||reaction_type_id (new)  : 2||text (new)  : Ma grande sœur chérie @valerie_ayena est déjà prêt pour le @theemigala de ce soir.\r\n\r\nBonne chance à toi ohh ||link (new)  : https://www.instagram.com/p/CRUCf_5FIsh/?utm_source=ig_web_copy_link||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 21||product_id (new)  : 8||Fichier  (new)  : ||detail (new)  : '),
(1295, '2021-07-15 10:33:01', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1296, '2021-07-15 10:33:03', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1297, '2021-07-15 10:33:31', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 76||--> fields <-- user_id (new)  : 7||action_id (new)  : 28||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 17871||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1298, '2021-07-15 10:33:53', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 77||--> fields <-- user_id (new)  : 7||action_id (new)  : 28||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 45||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ');
INSERT INTO `sc_log` (`id`, `inserted_date`, `username`, `application`, `creator`, `ip_user`, `action`, `description`) VALUES
(1299, '2021-07-15 10:34:04', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 28||--> fields <-- text (old)  : Ma grande sœur chérie @valerie_ayena est déjà prêt pour le @theemigala de ce soir.\r\n\r\nBonne chance à toi ohh ||text (new)  : Ma grande sœur chérie @valerie_ayena est déjà prêt pour le @theemigala de ce soir.\n\nBonne chance à toi ohh '),
(1300, '2021-07-15 10:34:21', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1301, '2021-07-15 10:35:00', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1302, '2021-07-15 10:39:58', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1303, '2021-07-15 10:40:32', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1304, '2021-07-15 10:41:34', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 29||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 1||reaction_type_id (new)  : 3||text (new)  : La journée peut enfin commencer , en route le périple du jour. \r\nJe vais aussi voir le Burj khalifa aujourd’hui  avec mes yeux. '),
(1305, '2021-07-15 10:41:50', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1306, '2021-07-15 10:41:52', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1307, '2021-07-15 10:42:38', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 78||--> fields <-- user_id (new)  : 7||action_id (new)  : 29||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 4320||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1308, '2021-07-15 10:43:02', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 79||--> fields <-- user_id (new)  : 7||action_id (new)  : 29||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 215||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1309, '2021-07-15 10:43:15', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 80||--> fields <-- user_id (new)  : 7||action_id (new)  : 29||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 2||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1310, '2021-07-15 10:43:20', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 29||--> fields <-- text (old)  : La journée peut enfin commencer , en route le périple du jour. \r\nJe vais aussi voir le Burj khalifa aujourd’hui  avec mes yeux. ????????????????\r\nN’hésites pas de taper *211# pour souscrit au forfait mtn Magic et tentez aussi de gagner un voyage pour deux à Dubaï . \r\nJe vais partager avec vous les journées des gagnants. \r\nBimoulee d’abord !!!||text (new)  : La journée peut enfin commencer , en route le périple du jour. \nJe vais aussi voir le Burj khalifa aujourd’hui  avec mes yeux. ????????????????\nN’hésites pas de taper *211# pour souscrit au forfait mtn Magic et tentez aussi de gagner un voyage pour deux à Dubaï . \nJe vais partager avec vous les journées des gagnants. \nBimoulee d’abord !!!'),
(1311, '2021-07-15 10:43:32', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1312, '2021-07-15 10:44:08', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1313, '2021-07-15 10:45:55', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 30||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 2||platform_id (new)  : 4||reaction_type_id (new)  : 3||text (new)  : La journée peut enfin commencer , en route le périple du jour.\r\n\r\nJe vais aussi voir le Burj khalifa aujourd’hui avec mes yeux. '),
(1314, '2021-07-15 10:46:13', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1315, '2021-07-15 10:46:16', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1316, '2021-07-15 10:47:30', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 15||--> fields <-- action_id (new)  : 30||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 124702.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1317, '2021-07-15 10:48:04', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 81||--> fields <-- user_id (new)  : 7||action_id (new)  : 30||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 1596||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1318, '2021-07-15 10:48:20', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 82||--> fields <-- user_id (new)  : 7||action_id (new)  : 30||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 22||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1319, '2021-07-15 10:48:24', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 30||--> fields <-- text (old)  : La journée peut enfin commencer , en route le périple du jour.\r\n\r\nJe vais aussi voir le Burj khalifa aujourd’hui avec mes yeux. ????????????????\r\n\r\nN’hésites pas de taper *211# pour souscrit au forfait mtn Magic et tentez aussi de gagner un voyage pour deux à Dubaï .\r\n\r\nJe vais partager avec vous les journées des gagnants.\r\n\r\nBimoulee d’abord !!!||text (new)  : La journée peut enfin commencer , en route le périple du jour.\n\nJe vais aussi voir le Burj khalifa aujourd’hui avec mes yeux. ????????????????\n\nN’hésites pas de taper *211# pour souscrit au forfait mtn Magic et tentez aussi de gagner un voyage pour deux à Dubaï .\n\nJe vais partager avec vous les journées des gagnants.\n\nBimoulee d’abord !!!'),
(1320, '2021-07-15 10:48:29', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1321, '2021-07-15 10:52:20', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1322, '2021-07-15 10:53:40', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.4', 'access', ''),
(1323, '2021-07-15 10:54:34', 'Anïs', 'form_products', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 10||--> fields <-- name (new)  : MTN MoMo||status (new)  : 1||created_at (new)  : '),
(1324, '2021-07-15 10:54:41', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1325, '2021-07-15 11:04:09', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.170.4', 'access', ''),
(1326, '2021-07-15 11:04:32', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.170.4', 'access', ''),
(1327, '2021-07-15 11:05:44', 'Anïs', 'form_influencers', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 22||--> fields <-- company_id (new)  : 1||category_id (new)  : 1||name (new)  : Francis N\'gannou||full_name (new)  : Francis N\'gannou||email (new)  : Francis N\'gannou@gmail.com||phone (new)  : 677777777||status (new)  : 1||created_at (new)  : ||updated_at (new)  : ||manager (new)  : '),
(1328, '2021-07-15 11:06:53', 'Anïs', 'grid_influencers', 'Scriptcase', '154.72.170.4', 'access', ''),
(1329, '2021-07-15 11:07:32', 'Anïs', 'grid_contracts', 'Scriptcase', '154.72.170.4', 'access', ''),
(1330, '2021-07-15 11:07:43', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.170.4', 'access', ''),
(1331, '2021-07-15 11:08:45', 'Anïs', 'form_contracts_ok', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 24||--> fields <-- start_date (new)  : 2021-05-01||end_date (new)  : 2021-12-31||description (new)  : Contrat entre MTN et Francis N\'gannou||created_at (new)  : ||updated_at (new)  : ||influenceur_id (new)  : 22||status (new)  : 1||scane (new)  : ||kpis (new)  : ||campagne (new)  : ||campaign_contract_kpis (new)  : '),
(1332, '2021-07-15 11:09:01', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1333, '2021-07-15 11:09:19', 'Anïs', 'form_contract_kpis', 'Scriptcase', '154.72.170.4', 'access', ''),
(1334, '2021-07-15 11:09:25', 'Anïs', 'form_contract_campaigns', 'Scriptcase', '154.72.170.4', 'access', ''),
(1335, '2021-07-15 11:09:28', 'Anïs', 'form_campaign_contract_kpis', 'Scriptcase', '154.72.170.4', 'access', ''),
(1336, '2021-07-15 11:12:04', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 31||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 6||platform_id (new)  : 1||reaction_type_id (new)  : 2||text (new)  : I can\'t explain how happy I\'m today for my family, but mostly my mom. A single woman who flawlessly raised 5 kids in an extreme poverty.\r\nToday she\'s shining all over billboards and TV commercials and she just got started. \r\nThank you @mtncameroon for making mama look great as a true Queen that she is.||link (new)  : https://fb.watch/v/39ta1GTRA/||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 24||product_id (new)  : 10||Fichier  (new)  : ||detail (new)  : '),
(1337, '2021-07-15 11:12:16', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1338, '2021-07-15 11:12:18', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1339, '2021-07-15 11:13:28', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 83||--> fields <-- user_id (new)  : 7||action_id (new)  : 31||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 7430||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1340, '2021-07-15 11:13:55', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 84||--> fields <-- user_id (new)  : 7||action_id (new)  : 31||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 308||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1341, '2021-07-15 11:14:25', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 85||--> fields <-- user_id (new)  : 7||action_id (new)  : 31||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 196||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1342, '2021-07-15 11:14:31', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 31||--> fields <-- text (old)  : I can\'t explain how happy I\'m today for my family, but mostly my mom. A single woman who flawlessly raised 5 kids in an extreme poverty.\r\nToday she\'s shining all over billboards and TV commercials and she just got started. \r\nThank you @mtncameroon for making mama look great as a true Queen that she is.||text (new)  : I can\'t explain how happy I\'m today for my family, but mostly my mom. A single woman who flawlessly raised 5 kids in an extreme poverty.\nToday she\'s shining all over billboards and TV commercials and she just got started. \nThank you @mtncameroon for making mama look great as a true Queen that she is.'),
(1343, '2021-07-15 11:15:13', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1344, '2021-07-15 11:16:17', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1345, '2021-07-15 11:22:13', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 32||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 3||text (new)  : UNBREAKABLE '),
(1346, '2021-07-15 11:22:33', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1347, '2021-07-15 11:22:45', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1348, '2021-07-15 11:23:52', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 16||--> fields <-- action_id (new)  : 32||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 132321.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1349, '2021-07-15 11:24:56', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 86||--> fields <-- user_id (new)  : 7||action_id (new)  : 32||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 19000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1350, '2021-07-15 11:25:19', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 87||--> fields <-- user_id (new)  : 7||action_id (new)  : 32||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 293||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1351, '2021-07-15 11:25:42', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 88||--> fields <-- user_id (new)  : 7||action_id (new)  : 32||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 204||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1352, '2021-07-15 11:25:47', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 32||--> fields <-- text (old)  : UNBREAKABLE ????\r\nWe stand, we fight odds, we overcome challenges and most of all, we stand proud.||text (new)  : UNBREAKABLE ????\nWe stand, we fight odds, we overcome challenges and most of all, we stand proud.||product_id (old)  : ||product_id (new)  : NULL'),
(1353, '2021-07-15 11:26:44', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1354, '2021-07-15 11:27:16', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1355, '2021-07-15 11:52:59', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 33||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 3||text (new)  : This moment is something I have been waiting for. It is hard to put this feeling in to words. The belt is back home where it belongs '),
(1356, '2021-07-15 11:53:20', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1357, '2021-07-15 11:53:22', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1358, '2021-07-15 11:56:44', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 17||--> fields <-- action_id (new)  : 33||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 135419.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1359, '2021-07-15 11:56:57', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 18||--> fields <-- action_id (new)  : 33||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 135540.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1360, '2021-07-15 11:57:04', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 33||--> fields <-- product_id (old)  : ||product_id (new)  : NULL'),
(1361, '2021-07-15 11:59:37', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 89||--> fields <-- user_id (new)  : 7||action_id (new)  : 33||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 88000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1362, '2021-07-15 11:59:58', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 90||--> fields <-- user_id (new)  : 7||action_id (new)  : 33||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 5400||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1363, '2021-07-15 12:00:15', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 91||--> fields <-- user_id (new)  : 7||action_id (new)  : 33||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 3900||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1364, '2021-07-15 12:00:22', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 33||--> fields <-- product_id (old)  : ||product_id (new)  : NULL'),
(1365, '2021-07-15 12:00:40', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 19||--> fields <-- action_id (new)  : 33||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 135853.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1366, '2021-07-15 12:00:44', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 33||--> fields <-- product_id (old)  : ||product_id (new)  : NULL'),
(1367, '2021-07-15 12:00:45', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1368, '2021-07-15 12:01:21', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1369, '2021-07-15 12:05:24', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 0||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 0||reaction_type_id (new)  : 3||text (new)  : It was great having the opportunity to talk with the students of the University of Buéa while doing the #unbeatable campaign and presentating the belt with @mtncameroon. I had a awesome moment out there and was in awe by their joy, happiness and excitement '),
(1370, '2021-07-15 12:06:01', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 35||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 3||text (new)  : It was great having the opportunity to talk with the students of the University of Buéa while doing the #unbeatable campaign and presentating the belt with @mtncameroon. I had a awesome moment out there and was in awe by their joy, happiness and excitement '),
(1371, '2021-07-15 12:06:14', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1372, '2021-07-15 12:06:18', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1373, '2021-07-15 12:07:39', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 20||--> fields <-- action_id (new)  : 35||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 140713.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1374, '2021-07-15 12:08:41', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 21||--> fields <-- action_id (new)  : 35||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 140822.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1375, '2021-07-15 12:08:45', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 35||--> fields <-- product_id (old)  : ||product_id (new)  : NULL'),
(1376, '2021-07-15 12:09:25', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 92||--> fields <-- user_id (new)  : 7||action_id (new)  : 35||kpi_id (new)  : 5||date_j (new)  : 2021-07-15||value (new)  : 58000||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1377, '2021-07-15 12:09:51', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 93||--> fields <-- user_id (new)  : 7||action_id (new)  : 35||kpi_id (new)  : 4||date_j (new)  : 2021-07-15||value (new)  : 1600||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1378, '2021-07-15 12:10:39', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 94||--> fields <-- user_id (new)  : 7||action_id (new)  : 35||kpi_id (new)  : 3||date_j (new)  : 2021-07-15||value (new)  : 404||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1379, '2021-07-15 12:10:45', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 35||--> fields <-- product_id (old)  : ||product_id (new)  : NULL'),
(1380, '2021-07-15 12:10:55', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1381, '2021-07-15 12:16:28', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1382, '2021-07-15 12:17:40', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 36||--> fields <-- user_id (new)  : 7||campaign_id (new)  : 4||platform_id (new)  : 1||reaction_type_id (new)  : 3||text (new)  : As I\'m leaving cameroon, I want to take this moment to thank @mtncameroon for all the help and support that they brought me to introduce the belt to the people. It was an unprecedented moment, and a lifetime experience that we witnessed during these campaigns and parades all over the country. \r\nThank you to the CEO, all the supervisors, managers and for all the #unbeatable team who did and #unbeatable job surrounding this historical moment. @ Yaoundé, Cameroon||link (new)  : https://www.facebook.com/photo/?fbid=343685403783342&set=pcb.343685763783306&__cft__[0]=AZV2rBQxsAJeo0IxrtDnE98NS7-gAc1C1Bn1x3WDQg3oVK1-W6wP1b8VBpZr1csBt17cuis7eDyKK-LN-rxwhWFH0GkhPIR2gawptUPAzEydaawHT6ZED5onagPzpj8vf9yBUeIprPSGR5jkVjhZhS84&__tn__=*bH-R||status (new)  : 1||tags (new)  : ||created_at (new)  : ||updated_at (new)  : ||contract_id (new)  : 24||product_id (new)  : NULL||Fichier  (new)  : ||detail (new)  : '),
(1383, '2021-07-15 12:18:00', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'access', ''),
(1384, '2021-07-15 12:18:04', 'Anïs', 'form_actions_details', 'Scriptcase', '154.72.170.4', 'access', ''),
(1385, '2021-07-15 12:23:40', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 22||--> fields <-- action_id (new)  : 36||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 141240.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1386, '2021-07-15 12:23:57', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 23||--> fields <-- action_id (new)  : 36||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 141329.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1387, '2021-07-15 12:24:00', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 24||--> fields <-- action_id (new)  : 36||type (new)  : ||link (new)  : ||status (new)  : ||created_at (new)  : ||updated_at (new)  : '),
(1388, '2021-07-15 12:24:06', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'delete', '--> keys <-- id : 24||--> fields <-- action_id (old)  : ||type (old)  : ||link (old)  : ||status (old)  : ||created_at (old)  : ||updated_at (old)  : '),
(1389, '2021-07-15 12:24:21', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 25||--> fields <-- action_id (new)  : 36||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 141411.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1390, '2021-07-15 12:24:37', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 26||--> fields <-- action_id (new)  : 36||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 141451.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1391, '2021-07-15 12:24:51', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 27||--> fields <-- action_id (new)  : 36||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 141533.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1392, '2021-07-15 12:25:05', 'Anïs', 'form_actions_files', 'Scriptcase', '154.72.170.4', 'insert', '--> keys <-- id : 28||--> fields <-- action_id (new)  : 36||type (new)  : Images||link (new)  : Capture d’écran 2021-07-15 141615.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1393, '2021-07-15 12:25:09', 'Anïs', 'form_actions', 'Scriptcase', '154.72.170.4', 'update', '--> keys <-- id : 36||--> fields <-- text (old)  : As I\'m leaving cameroon, I want to take this moment to thank @mtncameroon for all the help and support that they brought me to introduce the belt to the people. It was an unprecedented moment, and a lifetime experience that we witnessed during these campaigns and parades all over the country. \r\nThank you to the CEO, all the supervisors, managers and for all the #unbeatable team who did and #unbeatable job surrounding this historical moment. @ Yaoundé, Cameroon||text (new)  : As I\'m leaving cameroon, I want to take this moment to thank @mtncameroon for all the help and support that they brought me to introduce the belt to the people. It was an unprecedented moment, and a lifetime experience that we witnessed during these campaigns and parades all over the country. \nThank you to the CEO, all the supervisors, managers and for all the #unbeatable team who did and #unbeatable job surrounding this historical moment. @ Yaoundé, Cameroon||product_id (old)  : ||product_id (new)  : NULL'),
(1394, '2021-07-15 12:25:13', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.170.4', 'access', ''),
(1395, '2021-07-16 11:20:51', '', 'sec_Login', 'Scriptcase', '154.72.168.154', 'access', ''),
(1396, '2021-07-16 11:21:19', 'Anïs', 'sec_Login', 'User', '154.72.168.154', 'login', 'Connecté avec succès dans le système !'),
(1397, '2021-07-16 11:21:20', 'Anïs', 'menu', 'Scriptcase', '154.72.168.154', 'access', ''),
(1398, '2021-07-16 11:21:58', 'Anïs', 'grid_actions', 'Scriptcase', '154.72.168.154', 'access', ''),
(1399, '2021-07-16 11:58:46', '', 'sec_Login', 'Scriptcase', '154.72.168.154', 'access', ''),
(1400, '2021-07-16 11:59:32', 'admin', 'sec_Login', 'User', '154.72.168.154', 'login', 'Connecté avec succès dans le système !'),
(1401, '2021-07-16 11:59:33', 'admin', 'menu', 'Scriptcase', '154.72.168.154', 'access', ''),
(1402, '2021-07-16 11:59:44', 'admin', 'grid_actions', 'Scriptcase', '154.72.168.154', 'access', ''),
(1403, '2021-07-16 12:00:13', 'admin', 'form_actions', 'Scriptcase', '154.72.168.154', 'access', ''),
(1404, '2021-07-16 12:00:55', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.154', 'access', ''),
(1405, '2021-07-16 12:00:57', 'admin', 'form_actions_details', 'Scriptcase', '154.72.168.154', 'access', ''),
(1406, '2021-07-16 12:04:19', 'admin', 'form_actions_files', 'Scriptcase', '154.72.168.154', 'insert', '--> keys <-- id : 29||--> fields <-- action_id (new)  : 43||type (new)  : Images||link (new)  : mtn_news.jpg||status (new)  : 1||created_at (new)  : ||updated_at (new)  : '),
(1407, '2021-07-16 10:36:51', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1408, '2021-07-16 10:36:54', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1409, '2021-07-16 10:36:55', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1410, '2021-07-16 10:37:00', 'admin', 'chart_engagements_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1411, '2021-07-16 10:37:02', 'admin', 'chart_actions_canneau', 'Scriptcase', '127.0.0.1', 'access', ''),
(1412, '2021-07-16 10:37:04', 'admin', 'chart_vues_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1413, '2021-07-16 10:37:06', 'admin', 'chart_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1414, '2021-07-16 10:37:07', 'admin', 'chart_likes_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1415, '2021-07-16 10:37:09', 'admin', 'chart_actions_details_canneaux', 'Scriptcase', '127.0.0.1', 'access', ''),
(1416, '2021-07-16 10:38:31', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1417, '2021-07-16 10:40:00', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1418, '2021-07-16 10:40:01', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1419, '2021-07-16 10:40:01', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1420, '2021-07-16 10:40:06', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1421, '2021-07-16 10:40:09', 'admin', 'grid_contracts', 'Scriptcase', '127.0.0.1', 'access', ''),
(1422, '2021-07-16 10:41:29', 'admin', 'form_contracts_ok', 'Scriptcase', '127.0.0.1', 'access', ''),
(1423, '2021-07-16 10:41:33', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1424, '2021-07-16 10:41:36', 'admin', 'form_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1425, '2021-07-16 10:41:38', 'admin', 'form_contract_campaigns', 'Scriptcase', '127.0.0.1', 'access', ''),
(1426, '2021-07-16 10:50:07', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1427, '2021-07-16 10:50:08', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1428, '2021-07-16 10:50:09', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1429, '2021-07-16 10:50:11', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1430, '2021-07-16 10:50:19', 'admin', 'form_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1431, '2021-07-16 10:50:27', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1432, '2021-07-16 10:50:30', 'admin', 'grid_contracts', 'Scriptcase', '127.0.0.1', 'access', ''),
(1433, '2021-07-16 10:50:33', 'admin', 'form_contracts_ok', 'Scriptcase', '127.0.0.1', 'access', ''),
(1434, '2021-07-16 10:50:33', 'admin', 'form_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1435, '2021-07-16 10:50:33', 'admin', 'form_contract_campaigns', 'Scriptcase', '127.0.0.1', 'access', ''),
(1436, '2021-07-16 10:50:33', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1437, '2021-07-16 10:53:24', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1438, '2021-07-16 10:53:26', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1439, '2021-07-16 10:53:26', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1440, '2021-07-16 10:53:29', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1441, '2021-07-16 10:53:32', 'admin', 'grid_contracts', 'Scriptcase', '127.0.0.1', 'access', ''),
(1442, '2021-07-16 10:53:36', 'admin', 'form_contracts_ok', 'Scriptcase', '127.0.0.1', 'access', ''),
(1443, '2021-07-16 10:53:36', 'admin', 'form_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1444, '2021-07-16 10:53:36', 'admin', 'form_contract_campaigns', 'Scriptcase', '127.0.0.1', 'access', ''),
(1445, '2021-07-16 10:53:36', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1446, '2021-07-16 10:56:09', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1447, '2021-07-16 10:56:11', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1448, '2021-07-16 10:56:11', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1449, '2021-07-16 10:56:15', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1450, '2021-07-16 10:56:17', 'admin', 'grid_contracts', 'Scriptcase', '127.0.0.1', 'access', ''),
(1451, '2021-07-16 10:56:18', 'admin', 'form_contracts_ok', 'Scriptcase', '127.0.0.1', 'access', ''),
(1452, '2021-07-16 10:56:18', 'admin', 'form_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1453, '2021-07-16 10:56:18', 'admin', 'form_contract_campaigns', 'Scriptcase', '127.0.0.1', 'access', ''),
(1454, '2021-07-16 10:56:19', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1455, '2021-07-16 10:57:26', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1456, '2021-07-16 10:57:27', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1457, '2021-07-16 10:57:28', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1458, '2021-07-16 10:57:29', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1459, '2021-07-16 10:57:31', 'admin', 'grid_contracts', 'Scriptcase', '127.0.0.1', 'access', ''),
(1460, '2021-07-16 10:57:32', 'admin', 'form_contracts_ok', 'Scriptcase', '127.0.0.1', 'access', ''),
(1461, '2021-07-16 10:57:33', 'admin', 'form_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1462, '2021-07-16 10:57:33', 'admin', 'form_contract_campaigns', 'Scriptcase', '127.0.0.1', 'access', ''),
(1463, '2021-07-16 10:57:33', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1464, '2021-07-16 10:57:47', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1465, '2021-07-16 10:57:51', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1466, '2021-07-16 10:57:51', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1467, '2021-07-16 10:57:53', 'admin', 'grid_influencers', 'Scriptcase', '127.0.0.1', 'access', ''),
(1468, '2021-07-16 10:57:59', 'admin', 'grid_contracts', 'Scriptcase', '127.0.0.1', 'access', ''),
(1469, '2021-07-16 10:58:02', 'admin', 'form_contracts_ok', 'Scriptcase', '127.0.0.1', 'access', ''),
(1470, '2021-07-16 10:58:02', 'admin', 'form_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1471, '2021-07-16 10:58:02', 'admin', 'form_contract_campaigns', 'Scriptcase', '127.0.0.1', 'access', ''),
(1472, '2021-07-16 10:58:02', 'admin', 'form_campaign_contract_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1473, '2021-07-16 10:58:55', 'admin', 'form_kpis', 'Scriptcase', '127.0.0.1', 'access', ''),
(1474, '2021-07-16 11:06:06', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1475, '2021-07-16 11:06:09', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1476, '2021-07-16 11:06:09', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1477, '2021-07-16 11:06:13', 'admin', 'grid_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1478, '2021-07-16 11:06:15', 'admin', 'form_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1479, '2021-07-16 11:06:18', 'admin', 'form_actions_files', 'Scriptcase', '127.0.0.1', 'access', ''),
(1480, '2021-07-16 11:06:20', 'admin', 'form_actions_details', 'Scriptcase', '127.0.0.1', 'access', ''),
(1481, '2021-07-21 14:41:09', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1482, '2021-07-21 14:43:13', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateurwill'),
(1483, '2021-07-21 14:43:28', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateurwill'),
(1484, '2021-07-21 14:44:13', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1485, '2021-07-21 14:44:22', 'will', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1486, '2021-07-21 14:46:07', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1487, '2021-07-21 14:46:18', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1488, '2021-07-21 14:46:18', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1489, '2021-07-21 14:56:05', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1490, '2021-07-21 14:56:17', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1491, '2021-07-21 15:01:43', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1492, '2021-07-21 15:01:46', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1493, '2021-07-21 15:01:57', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1494, '2021-07-21 15:01:57', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1495, '2021-07-21 15:02:02', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1496, '2021-07-21 15:02:05', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1497, '2021-07-21 15:05:01', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1498, '2021-07-21 15:05:37', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1499, '2021-07-21 15:05:37', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1500, '2021-07-21 15:05:41', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1501, '2021-07-21 15:05:44', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1502, '2021-07-21 15:16:22', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1503, '2021-07-21 15:16:24', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1504, '2021-07-21 15:16:35', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1505, '2021-07-21 15:16:35', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1506, '2021-07-22 05:38:56', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1507, '2021-07-22 05:39:07', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1508, '2021-07-22 05:39:39', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1509, '2021-07-22 05:39:41', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1510, '2021-07-22 05:39:56', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1511, '2021-07-22 05:40:26', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1512, '2021-07-22 05:40:34', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1513, '2021-07-22 05:40:39', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1514, '2021-07-22 05:46:42', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1515, '2021-07-22 05:46:43', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1516, '2021-07-22 05:46:47', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1517, '2021-07-22 05:46:50', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1518, '2021-07-22 05:47:17', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 0||login : test||--> fields <-- company_id (new)  : NULL||type (new)  : ||name (new)  : test Will||full_name (new)  : K||email (new)  : mkwilson24@gmail.com||phone (new)  : 677715404||password (new)  : $2y$10$M6/BkfRTzALIKNDUlorUGe1yOX48Xeo/gGhkGfA8wFDmHvXx4EARC||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : 1'),
(1519, '2021-07-22 06:28:32', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1520, '2021-07-22 06:28:35', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1521, '2021-07-22 06:28:38', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1522, '2021-07-22 06:28:40', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1523, '2021-07-22 06:28:45', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1524, '2021-07-22 06:28:47', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1525, '2021-07-22 06:30:13', 'admin', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1526, '2021-07-22 06:30:49', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1527, '2021-07-22 06:30:51', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1528, '2021-07-22 06:31:00', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1529, '2021-07-22 06:31:00', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1530, '2021-07-22 06:31:05', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1531, '2021-07-22 06:36:57', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1532, '2021-07-22 06:36:59', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1533, '2021-07-22 06:37:07', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1534, '2021-07-22 06:37:07', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1535, '2021-07-22 06:37:11', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1536, '2021-07-22 06:37:11', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1537, '2021-07-22 06:37:14', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1538, '2021-07-22 06:37:14', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1539, '2021-07-22 07:06:17', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1540, '2021-07-22 07:06:21', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1541, '2021-07-22 07:06:34', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1542, '2021-07-22 07:07:50', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1543, '2021-07-22 07:07:51', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1544, '2021-07-22 07:08:06', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1545, '2021-07-22 07:08:13', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1546, '2021-07-22 07:08:21', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1547, '2021-07-22 07:08:21', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1548, '2021-07-22 07:08:26', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1549, '2021-07-22 07:12:02', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1550, '2021-07-22 07:12:05', 'admin', 'sec_sync_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1551, '2021-07-22 07:12:15', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1552, '2021-07-22 07:12:54', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1553, '2021-07-22 07:13:26', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1554, '2021-07-22 07:13:26', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1555, '2021-07-22 07:13:31', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1556, '2021-07-22 07:13:33', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1557, '2021-07-22 07:13:33', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1558, '2021-07-22 07:13:36', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1559, '2021-07-22 07:15:33', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1560, '2021-07-22 07:15:38', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1561, '2021-07-22 07:15:40', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1562, '2021-07-22 07:15:48', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1563, '2021-07-22 07:15:48', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1564, '2021-07-22 07:15:54', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1565, '2021-07-22 07:15:55', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1566, '2021-07-22 07:16:08', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1567, '2021-07-22 07:16:08', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1568, '2021-07-22 07:31:08', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1569, '2021-07-22 07:31:13', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1570, '2021-07-22 07:31:13', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1571, '2021-07-22 07:31:18', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1572, '2021-07-22 07:31:18', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1573, '2021-07-22 07:31:20', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1574, '2021-07-22 07:31:20', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1575, '2021-07-22 07:35:49', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1576, '2021-07-22 07:36:10', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1577, '2021-07-22 07:36:11', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1578, '2021-07-22 07:48:40', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1579, '2021-07-22 07:48:45', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1580, '2021-07-22 07:48:46', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1581, '2021-07-22 07:49:10', 'admin', 'form_companies', 'Scriptcase', '127.0.0.1', 'access', ''),
(1582, '2021-07-22 07:49:13', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1583, '2021-07-22 07:49:15', 'admin', 'sec_grid_sec_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1584, '2021-07-22 07:49:17', 'admin', 'sec_grid_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1585, '2021-07-22 07:49:19', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1586, '2021-07-22 07:49:22', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1587, '2021-07-22 07:49:30', 'admin', 'sec_sync_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1588, '2021-07-22 07:49:32', 'admin', 'sec_change_pswd', 'Scriptcase', '127.0.0.1', 'access', ''),
(1589, '2021-07-22 07:49:41', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1590, '2021-07-22 07:49:46', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1591, '2021-07-22 07:50:31', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1592, '2021-07-22 07:50:31', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1593, '2021-07-22 07:56:49', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1594, '2021-07-22 07:56:51', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1595, '2021-07-22 07:57:00', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1596, '2021-07-22 07:57:00', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1597, '2021-07-22 07:57:15', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1598, '2021-07-22 07:57:18', 'admin', 'sec_grid_sec_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1599, '2021-07-22 07:57:20', 'admin', 'sec_change_pswd', 'Scriptcase', '127.0.0.1', 'access', ''),
(1600, '2021-07-22 08:02:33', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1601, '2021-07-22 08:03:04', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1602, '2021-07-22 08:13:22', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1603, '2021-07-22 08:13:41', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1604, '2021-07-22 08:16:47', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1605, '2021-07-22 08:16:51', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1606, '2021-07-22 08:16:57', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1607, '2021-07-22 08:17:38', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1608, '2021-07-22 08:17:41', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1609, '2021-07-22 08:17:46', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1610, '2021-07-22 08:17:49', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1611, '2021-07-22 08:18:44', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1612, '2021-07-22 08:18:47', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1613, '2021-07-22 08:20:46', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1614, '2021-07-22 08:20:53', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1615, '2021-07-22 08:22:38', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1616, '2021-07-22 08:22:55', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1617, '2021-07-22 08:23:10', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1618, '2021-07-22 08:23:52', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1619, '2021-07-22 08:23:52', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1620, '2021-07-22 08:24:05', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1621, '2021-07-22 08:24:09', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1622, '2021-07-22 08:24:09', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1623, '2021-07-22 08:24:14', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1624, '2021-07-22 08:24:17', 'admin', 'sec_grid_sec_users_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1625, '2021-07-22 08:24:18', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1626, '2021-07-22 08:24:35', 'admin', 'sec_search_sec_groups', 'Scriptcase', '127.0.0.1', 'access', ''),
(1627, '2021-07-22 08:24:35', 'admin', 'sec_form_sec_groups_apps', 'Scriptcase', '127.0.0.1', 'access', ''),
(1628, '2021-07-22 08:24:41', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', '');
INSERT INTO `sc_log` (`id`, `inserted_date`, `username`, `application`, `creator`, `ip_user`, `action`, `description`) VALUES
(1629, '2021-07-22 08:28:09', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1630, '2021-07-22 08:30:03', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1631, '2021-07-22 08:30:03', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1632, '2021-07-22 08:30:10', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1633, '2021-07-22 08:30:12', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1634, '2021-07-22 08:32:20', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 0||login : test||--> fields <-- password (new)  : $2y$10$lr8YPE3OCHujx7F5K2uHwexwIQZKu5vCOY116s244uP5KeZ.gdPUS||name (new)  : test||email (new)  : mkwilson2422@gmail.com||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||company_id (new)  : 1||type (new)  : ||full_name (new)  : Marck Tankeu||phone (new)  : 0233501300||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : 1'),
(1635, '2021-07-22 08:33:43', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 0||login : admin233||--> fields <-- password (new)  : $2y$10$3gCYDXQbN87kQ/OF1G1kUOY3Aj5TbtKb7GOdIwJk6gfbC70wYkULm||name (new)  : Wyllie marck||email (new)  : mkwilson245662@gmail.com||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||company_id (new)  : 1||type (new)  : ||full_name (new)  : Wyllie marck Kapseu Tankeu||phone (new)  : 0233501300||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : '),
(1636, '2021-07-22 08:42:11', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1637, '2021-07-22 08:42:18', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1638, '2021-07-22 08:42:18', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1639, '2021-07-22 08:53:12', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1640, '2021-07-22 08:53:19', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1641, '2021-07-22 08:53:19', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1642, '2021-07-22 08:53:23', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1643, '2021-07-22 08:53:33', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1644, '2021-07-22 08:53:57', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 0||login : test||--> fields <-- password (new)  : $2y$10$qo.VSbVQzH8sevUagZfFI.P.ChcC/4X9glCEp1ItSo2sVKM0n9CTq||name (new)  : test||email (new)  : test@gmail.com||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||company_id (new)  : 1||full_name (new)  : Marck Tankeu||phone (new)  : 0233501300||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : 1'),
(1645, '2021-07-22 08:55:36', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1646, '2021-07-22 08:55:43', 'test', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1647, '2021-07-22 08:55:43', 'test', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1648, '2021-07-22 12:15:08', 'test', 'grid_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1649, '2021-07-22 12:15:09', 'test', 'chart_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1650, '2021-07-22 12:15:11', 'test', 'chart_vues_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1651, '2021-07-22 12:15:12', 'test', 'chart_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1652, '2021-07-22 12:15:12', 'test', 'chart_engagements_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1653, '2021-07-22 12:15:14', 'test', 'chart_actions_canneau', 'Scriptcase', '127.0.0.1', 'access', ''),
(1654, '2021-07-22 12:15:15', 'test', 'chart_engagements_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1655, '2021-07-22 12:15:15', 'test', 'chart_actions_details_canneaux', 'Scriptcase', '127.0.0.1', 'access', ''),
(1656, '2021-07-22 12:15:17', 'test', 'chart_actions_canneau', 'Scriptcase', '127.0.0.1', 'access', ''),
(1657, '2021-07-22 12:15:17', 'test', 'chart_actions_details_canneaux', 'Scriptcase', '127.0.0.1', 'access', ''),
(1658, '2021-07-22 12:15:18', 'test', 'chart_likes_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1659, '2021-07-22 12:15:19', 'test', 'chart_likes_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1660, '2021-07-22 12:15:19', 'test', 'chart_vues_actions', 'Scriptcase', '127.0.0.1', 'access', ''),
(1661, '2021-07-22 12:15:21', 'test', 'grid_messages', 'Scriptcase', '127.0.0.1', 'access', ''),
(1662, '2021-07-22 12:15:25', 'test', 'form_messages', 'Scriptcase', '127.0.0.1', 'access', ''),
(1663, '2021-07-22 12:15:41', 'test', 'form_products', 'Scriptcase', '127.0.0.1', 'access', ''),
(1664, '2021-07-22 12:24:45', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1665, '2021-07-22 12:24:46', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1666, '2021-07-22 12:24:57', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1667, '2021-07-22 12:24:58', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1668, '2021-07-22 12:25:02', 'admin', 'form_products', 'Scriptcase', '127.0.0.1', 'access', ''),
(1669, '2021-07-22 12:27:47', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1670, '2021-07-22 12:27:54', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1671, '2021-07-22 12:27:55', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1672, '2021-07-22 12:27:59', 'admin', 'form_products', 'Scriptcase', '127.0.0.1', 'access', ''),
(1673, '2021-07-22 12:28:03', 'admin', 'form_companies', 'Scriptcase', '127.0.0.1', 'access', ''),
(1674, '2021-07-22 12:28:04', 'admin', 'form_campaigns', 'Scriptcase', '127.0.0.1', 'access', ''),
(1675, '2021-07-22 17:08:19', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1676, '2021-07-22 17:08:21', '', 'sec_Login', 'User', '127.0.0.1', 'login Fail', 'Quelqu\'un a essayé de se connecter avec cet utilisateuradmin'),
(1677, '2021-07-22 17:08:27', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1678, '2021-07-22 17:08:28', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1679, '2021-07-22 17:08:36', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1680, '2021-07-22 17:09:18', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1681, '2021-07-22 17:09:26', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1682, '2021-07-22 17:09:26', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1683, '2021-07-22 17:09:39', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1684, '2021-07-22 17:09:42', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1685, '2021-07-22 17:10:36', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 0||login : testUser||--> fields <-- password (new)  : $2y$10$zkg8EafE9nU6J1ntRED2KOdT/p8wKoWjJbxJDVjJoUtMsTsZYjTUW||name (new)  : testUser||email (new)  : testUser@testUser.cm||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||company_id (new)  : 1||full_name (new)  : testUser||phone (new)  : 693060210||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : 1'),
(1686, '2021-07-22 17:11:10', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1687, '2021-07-22 17:11:18', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1688, '2021-07-22 17:11:24', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1689, '2021-07-22 17:11:30', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1690, '2021-07-22 17:11:35', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1691, '2021-07-22 17:13:16', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1692, '2021-07-22 17:13:23', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1693, '2021-07-22 17:13:23', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1694, '2021-07-22 17:13:28', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1695, '2021-07-22 17:13:30', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1696, '2021-07-22 17:13:59', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'insert', '--> keys <-- id : 0||login : testuset||--> fields <-- password (new)  : $2y$10$mFJAdH1LFU3VxU7kbcHIVOm1NXNA1Ns3Xju7GrWTq8IqLTB94WYFa||name (new)  : testuset||email (new)  : testuset@testuset.cm||active (new)  : Y||activation_code (new)  : ||priv_admin (new)  : ||company_id (new)  : 1||full_name (new)  : testuset||phone (new)  : 693060210||status (new)  : 1||created_at (new)  : null||updated_at (new)  : null||groups (new)  : 1'),
(1697, '2021-07-22 17:16:50', '', 'sec_Login', 'Scriptcase', '127.0.0.1', 'access', ''),
(1698, '2021-07-22 17:16:56', 'admin', 'sec_Login', 'User', '127.0.0.1', 'login', 'Connecté avec succès dans le système !'),
(1699, '2021-07-22 17:16:56', 'admin', 'menu', 'Scriptcase', '127.0.0.1', 'access', ''),
(1700, '2021-07-22 17:17:00', 'admin', 'sec_grid_sec_users', 'Scriptcase', '127.0.0.1', 'access', ''),
(1701, '2021-07-22 17:17:04', 'admin', 'sec_form_edit_users', 'Scriptcase', '127.0.0.1', 'access', '');

-- --------------------------------------------------------

--
-- Structure de la table `sec_apps`
--

CREATE TABLE `sec_apps` (
  `app_name` varchar(128) NOT NULL,
  `app_type` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sec_apps`
--

INSERT INTO `sec_apps` (`app_name`, `app_type`, `description`) VALUES
('chart_actions', 'chart', ''),
('chart_actions_canneau', 'chart', ''),
('chart_actions_details_canneaux', 'chart', ''),
('chart_comments_actions', 'chart', ''),
('chart_engagements_actions', 'chart', ''),
('chart_engegement_actions', 'chart', ''),
('chart_engegement_campagne', 'chart', ''),
('chart_engegement_kpi', 'chart', ''),
('chart_engegement_products', 'chart', ''),
('chart_likes_actions', 'chart', ''),
('chart_repartition_influenseur_actions', 'chart', ''),
('chart_vues_actions', 'chart', ''),
('dashboard_global', 'container', ''),
('dashboard_infl', 'container', ''),
('form_actions', 'form', ''),
('form_actions_details', 'form', ''),
('form_actions_files', 'form', ''),
('form_actions_types', 'form', ''),
('form_campaigns', 'form', ''),
('form_campaign_contract_kpis', 'form', ''),
('form_categories', 'form', ''),
('form_companies', 'form', ''),
('form_contracts', 'form', ''),
('form_contracts_ok', 'form', ''),
('form_contract_campaigns', 'form', ''),
('form_contract_kpis', 'form', ''),
('form_groups', 'form', ''),
('form_group_user', 'form', ''),
('form_influencers', 'form', ''),
('form_influencer_managers', 'form', ''),
('form_kpis', 'form', ''),
('form_messages', 'form', ''),
('form_message_groupe_push', 'form', ''),
('form_platforms', 'form', ''),
('form_products', 'form', ''),
('grid_actions', 'cons', ''),
('grid_actions_details', 'cons', ''),
('grid_actions_details_last', 'cons', ''),
('grid_actions_files', 'cons', ''),
('grid_actions_files_clik', 'cons', ''),
('grid_contracts', 'cons', ''),
('grid_groups', 'cons', ''),
('grid_influencers', 'cons', ''),
('grid_messages', 'cons', ''),
('grid_message_groupe_push', 'cons', ''),
('grid_message_user', 'cons', ''),
('grid_vue_engagement_influenceurs', 'cons', ''),
('herder_dashboard', 'blank', ''),
('images_galery', 'blank', ''),
('lecteur_Iframe', 'blank', ''),
('menu', 'menu', ''),
('sec_change_pswd', 'contr', 'Security Application'),
('sec_form_add_users', 'form', 'Security Application'),
('sec_form_edit_users', 'form', 'Security Application'),
('sec_form_sec_apps', 'form', 'Security Application'),
('sec_form_sec_groups', 'form', 'Security Application'),
('sec_form_sec_groups_apps', 'form', 'Security Application'),
('sec_grid_sec_apps', 'cons', 'Security Application'),
('sec_grid_sec_groups', 'cons', 'Security Application'),
('sec_grid_sec_users', 'cons', 'Security Application'),
('sec_grid_sec_users_groups', 'form', 'Security Application'),
('sec_Login', 'contr', 'Security Application'),
('sec_retrieve_pswd', 'contr', 'Security Application'),
('sec_search_sec_groups', 'filter', 'Security Application'),
('sec_sync_apps', 'contr', 'Security Application'),
('zes_Login_x', 'contr', 'Login - Initial Application');

-- --------------------------------------------------------

--
-- Structure de la table `sec_groups`
--

CREATE TABLE `sec_groups` (
  `group_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `code` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sec_groups`
--

INSERT INTO `sec_groups` (`group_id`, `description`, `code`) VALUES
(1, 'Administrateur', 'ADMIN'),
(2, 'Company Admin ', 'COMPANYAD'),
(3, 'influencer Manager', 'MANAGER');

-- --------------------------------------------------------

--
-- Structure de la table `sec_groups_apps`
--

CREATE TABLE `sec_groups_apps` (
  `group_id` int(11) NOT NULL,
  `app_name` varchar(128) NOT NULL,
  `priv_access` varchar(1) DEFAULT NULL,
  `priv_insert` varchar(1) DEFAULT NULL,
  `priv_delete` varchar(1) DEFAULT NULL,
  `priv_update` varchar(1) DEFAULT NULL,
  `priv_export` varchar(1) DEFAULT NULL,
  `priv_print` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sec_groups_apps`
--

INSERT INTO `sec_groups_apps` (`group_id`, `app_name`, `priv_access`, `priv_insert`, `priv_delete`, `priv_update`, `priv_export`, `priv_print`) VALUES
(1, 'chart_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_actions_canneau', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_actions_details_canneaux', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_comments_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_engagements_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_engegement_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_engegement_campagne', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_engegement_kpi', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_engegement_products', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_likes_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_repartition_influenseur_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'chart_vues_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'dashboard_global', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'dashboard_infl', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'form_actions', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_actions_details', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_actions_files', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_actions_types', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_campaigns', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_campaign_contract_kpis', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_categories', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_companies', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_contracts', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_contracts_ok', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_contract_campaigns', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_contract_kpis', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_groups', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_group_user', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_influencers', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_influencer_managers', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_kpis', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_messages', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_message_groupe_push', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_platforms', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'form_products', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'grid_actions', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_actions_details', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_actions_details_last', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_actions_files', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_actions_files_clik', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_contracts', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_groups', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_influencers', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_messages', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_message_groupe_push', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_message_user', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'grid_vue_engagement_influenceurs', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'herder_dashboard', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'images_galery', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'lecteur_Iframe', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'menu', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'sec_change_pswd', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'sec_form_add_users', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'sec_form_edit_users', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'sec_form_sec_apps', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'sec_form_sec_groups', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'sec_form_sec_groups_apps', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'sec_grid_sec_apps', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'sec_grid_sec_groups', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'sec_grid_sec_users', 'Y', 'N', 'N', 'N', 'Y', 'Y'),
(1, 'sec_grid_sec_users_groups', 'Y', 'Y', 'Y', 'Y', 'N', 'N'),
(1, 'sec_Login', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'sec_retrieve_pswd', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'sec_search_sec_groups', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'sec_sync_apps', 'Y', 'N', 'N', 'N', 'N', 'N'),
(1, 'zes_Login_x', 'Y', 'N', 'N', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Structure de la table `sec_users_groups`
--

CREATE TABLE `sec_users_groups` (
  `login` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sec_users_groups`
--

INSERT INTO `sec_users_groups` (`login`, `group_id`) VALUES
('admin', 1),
('test', 1),
('testuset', 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `activation_code` varchar(45) DEFAULT NULL,
  `priv_admin` varchar(1) DEFAULT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `email`, `active`, `activation_code`, `priv_admin`, `company_id`, `full_name`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$GEklhEDsngUAm9.YMIWZj.Z1glLP6zYWZ457WInxsR8rXV3Y00uJy', 'admin ', 'admin@admin.com', 'Y', 'ras', 'Y', 1, 'admin test', '666666666', 1, '2021-06-30 09:09:23', '2021-06-30 09:09:23'),
(7, 'Anïs', '$2y$10$GEklhEDsngUAm9.YMIWZj.Z1glLP6zYWZ457WInxsR8rXV3Y00uJy', 'Anaïs Ndédi', 'andedi@opensolutions-it.com', 'Y', '', '', 1, 'Anaïs Ndédi', '666666666', 1, NULL, NULL),
(9, 'navarro21', '$2y$10$GEklhEDsngUAm9.YMIWZj.Z1glLP6zYWZ457WInxsR8rXV3Y00uJy', 'Navaro ', 'vnavarro@opensolutions-it.com', 'Y', '', '', 1, 'Navaro ', '659476961', 1, NULL, NULL),
(11, 'will', '$2y$10$GEklhEDsngUAm9.YMIWZj.Z1glLP6zYWZ457WInxsR8rXV3Y00uJy', 'will', 'will2@will.cm', 'Y', NULL, NULL, 1, 'will', '677715404', 1, '2021-07-13 19:46:48', '2021-07-13 19:46:48'),
(18, 'test', '$2y$10$qo.VSbVQzH8sevUagZfFI.P.ChcC/4X9glCEp1ItSo2sVKM0n9CTq', 'test', 'test@gmail.com', 'Y', '', '', 1, 'Marck Tankeu', '0233501300', 1, '2021-07-22 11:53:57', '2021-07-22 11:53:57'),
(20, 'testuset', '$2y$10$mFJAdH1LFU3VxU7kbcHIVOm1NXNA1Ns3Xju7GrWTq8IqLTB94WYFa', 'testuset', 'testuset@testuset.cm', 'Y', '', '', 1, 'testuset', '693060210', 1, '2021-07-22 20:13:59', '2021-07-22 20:13:59');

-- --------------------------------------------------------

--
-- Structure de la table `versionning`
--

CREATE TABLE `versionning` (
  `id` int(11) NOT NULL,
  `code` varchar(30) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `vesion_date` date DEFAULT NULL,
  `actif` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(45) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `versionning`
--

INSERT INTO `versionning` (`id`, `code`, `label`, `note`, `vesion_date`, `actif`, `created_at`, `created_by`, `updated_at`, `update_by`, `link`) VALUES
(1, '1.0.1', 'Version non identifiée', '', NULL, 1, '2017-12-11 11:35:55', 'admin', '2021-07-21 13:46:48', 'admin', 'https://infl.fieldlive.pro/mobileAppSocialTracking/app-release.apk ');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_engagement_influenceurs`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_engagement_influenceurs` (
`value` double
,`influenceur_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_max_actions_details`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_max_actions_details` (
`id` int(10) unsigned
,`user_id` int(10) unsigned
,`action_id` int(10) unsigned
,`kpi_id` int(10) unsigned
,`date_j` date
,`value` double
,`status` tinyint(1)
,`created_at` timestamp
,`text` longtext
,`platform_id` int(10) unsigned
,`reaction_type_id` int(10) unsigned
,`campaign_id` int(10) unsigned
,`contract_id` int(11)
,`description` text
,`influenceur_id` int(10) unsigned
,`product_id` int(11)
,`link` varchar(255)
,`action_status` tinyint(1)
,`tags` text
);

-- --------------------------------------------------------

--
-- Structure de la vue `vue_engagement_influenceurs`
--
DROP TABLE IF EXISTS `vue_engagement_influenceurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_engagement_influenceurs`  AS  select sum(`vue_max_actions_details`.`value`) AS `value`,`vue_max_actions_details`.`influenceur_id` AS `influenceur_id` from `vue_max_actions_details` group by `vue_max_actions_details`.`influenceur_id` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_max_actions_details`
--
DROP TABLE IF EXISTS `vue_max_actions_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_max_actions_details`  AS  (select `actions_details`.`id` AS `id`,`actions_details`.`user_id` AS `user_id`,`actions`.`id` AS `action_id`,`actions_details`.`kpi_id` AS `kpi_id`,`actions_details`.`date_j` AS `date_j`,`actions_details`.`value` AS `value`,`actions_details`.`status` AS `status`,`actions_details`.`created_at` AS `created_at`,`actions`.`text` AS `text`,`actions`.`platform_id` AS `platform_id`,`actions`.`reaction_type_id` AS `reaction_type_id`,`actions`.`campaign_id` AS `campaign_id`,`actions`.`contract_id` AS `contract_id`,`contracts`.`description` AS `description`,`contracts`.`influenceur_id` AS `influenceur_id`,`actions`.`product_id` AS `product_id`,`actions`.`link` AS `link`,`actions`.`status` AS `action_status`,`actions`.`tags` AS `tags` from (((`actions_details` join (select max(`actions_details`.`id`) AS `idt` from `actions_details` group by `actions_details`.`action_id`,`actions_details`.`kpi_id`) `t` on((`actions_details`.`id` = `t`.`idt`))) join `actions` on((`actions_details`.`action_id` = `actions`.`id`))) join `contracts` on((`actions`.`contract_id` = `contracts`.`id`)))) ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `FK_Reactions_Users_idx` (`user_id`),
  ADD KEY `FK_Reactions_Campaigns_idx` (`campaign_id`),
  ADD KEY `FK_Reactions_Platforms_idx` (`platform_id`),
  ADD KEY `FK_Reactions_ReactionsTypes_idx` (`reaction_type_id`);

--
-- Index pour la table `actions_details`
--
ALTER TABLE `actions_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_actions_details_users1_idx` (`user_id`),
  ADD KEY `fk_actions_details_actions1_idx` (`action_id`),
  ADD KEY `fk_actions_details_kpis1_idx` (`kpi_id`);

--
-- Index pour la table `actions_files`
--
ALTER TABLE `actions_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_actions_files_actions1_idx` (`action_id`);

--
-- Index pour la table `actions_types`
--
ALTER TABLE `actions_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Index pour la table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_campaigns_companies1_idx` (`company_id`);

--
-- Index pour la table `campaign_contract_kpis`
--
ALTER TABLE `campaign_contract_kpis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_campaign_contract_kpis_kpis1_idx` (`kpi_id`),
  ADD KEY `fk_campaign_contract_kpis_campaign_contract1_idx` (`campaign_contract_id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Index pour la table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Index pour la table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_contracts_influenceurs1_idx` (`influenceur_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Index pour la table `contract_campaigns`
--
ALTER TABLE `contract_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_kpi_contracts_contracts1_idx` (`contract_id`),
  ADD KEY `fk_campaign_contract_kpis_campaigns1_idx` (`campaign_id`);

--
-- Index pour la table `contract_kpis`
--
ALTER TABLE `contract_kpis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_contract_kpis_kpis1_idx` (`kpi_id`),
  ADD KEY `fk_contract_kpis_contracts1_idx` (`contract_id`);

--
-- Index pour la table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_groups_companies1_idx` (`company_id`);

--
-- Index pour la table `group_user`
--
ALTER TABLE `group_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `FK_GroupUser_Users_idx` (`influencer_id`),
  ADD KEY `FK_GroupUser_Groups_idx` (`group_id`);

--
-- Index pour la table `influencers`
--
ALTER TABLE `influencers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_influenceurs_companies1_idx` (`company_id`),
  ADD KEY `fk_influencers_categories1_idx` (`category_id`);

--
-- Index pour la table `influencer_managers`
--
ALTER TABLE `influencer_managers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_influencer_managers_users1_idx` (`user_id`),
  ADD KEY `fk_influencer_managers_influenceurs1_idx` (`influenceur_id`);

--
-- Index pour la table `kpis`
--
ALTER TABLE `kpis`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_kpis_companies1_idx` (`companies_id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `PK_Messages_Campaigns_idx` (`campaign_id`);

--
-- Index pour la table `message_groupe_push`
--
ALTER TABLE `message_groupe_push`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message_user`
--
ALTER TABLE `message_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `FK_MessageUser_Messages_idx` (`message_id`),
  ADD KEY `FK_MessageUser_Users_idx` (`user_id`);

--
-- Index pour la table `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- Index pour la table `sc_log`
--
ALTER TABLE `sc_log`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sec_apps`
--
ALTER TABLE `sec_apps`
  ADD PRIMARY KEY (`app_name`);

--
-- Index pour la table `sec_groups`
--
ALTER TABLE `sec_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Index pour la table `sec_groups_apps`
--
ALTER TABLE `sec_groups_apps`
  ADD PRIMARY KEY (`group_id`,`app_name`),
  ADD KEY `sec_groups_apps_ibfk_2` (`app_name`);

--
-- Index pour la table `sec_users_groups`
--
ALTER TABLE `sec_users_groups`
  ADD PRIMARY KEY (`login`,`group_id`),
  ADD KEY `sec_users_groups_ibfk_2` (`group_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`),
  ADD KEY `fk_users_companies1_idx` (`company_id`);

--
-- Index pour la table `versionning`
--
ALTER TABLE `versionning`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT pour la table `actions_details`
--
ALTER TABLE `actions_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT pour la table `actions_files`
--
ALTER TABLE `actions_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `actions_types`
--
ALTER TABLE `actions_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `campaign_contract_kpis`
--
ALTER TABLE `campaign_contract_kpis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `contract_campaigns`
--
ALTER TABLE `contract_campaigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `contract_kpis`
--
ALTER TABLE `contract_kpis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `group_user`
--
ALTER TABLE `group_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `influencers`
--
ALTER TABLE `influencers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `influencer_managers`
--
ALTER TABLE `influencer_managers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `kpis`
--
ALTER TABLE `kpis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `message_groupe_push`
--
ALTER TABLE `message_groupe_push`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `message_user`
--
ALTER TABLE `message_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `platforms`
--
ALTER TABLE `platforms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `sc_log`
--
ALTER TABLE `sc_log`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1702;

--
-- AUTO_INCREMENT pour la table `sec_groups`
--
ALTER TABLE `sec_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `versionning`
--
ALTER TABLE `versionning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `FK_Reactions_Campaigns` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Reactions_Platforms` FOREIGN KEY (`platform_id`) REFERENCES `platforms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Reactions_ReactionsTypes` FOREIGN KEY (`reaction_type_id`) REFERENCES `actions_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Reactions_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `actions_details`
--
ALTER TABLE `actions_details`
  ADD CONSTRAINT `fk_actions_details_actions1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_actions_details_kpis1` FOREIGN KEY (`kpi_id`) REFERENCES `kpis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_actions_details_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `actions_files`
--
ALTER TABLE `actions_files`
  ADD CONSTRAINT `fk_actions_files_actions1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `campaigns`
--
ALTER TABLE `campaigns`
  ADD CONSTRAINT `fk_campaigns_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `campaign_contract_kpis`
--
ALTER TABLE `campaign_contract_kpis`
  ADD CONSTRAINT `fk_campaign_contract_kpis_campaign_contract1` FOREIGN KEY (`campaign_contract_id`) REFERENCES `contract_campaigns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_campaign_contract_kpis_kpis1` FOREIGN KEY (`kpi_id`) REFERENCES `kpis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `contracts`
--
ALTER TABLE `contracts`
  ADD CONSTRAINT `fk_contracts_influenceurs1` FOREIGN KEY (`influenceur_id`) REFERENCES `influencers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `contract_campaigns`
--
ALTER TABLE `contract_campaigns`
  ADD CONSTRAINT `fk_campaign_contract_kpis_campaigns1` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kpi_contracts_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `contract_kpis`
--
ALTER TABLE `contract_kpis`
  ADD CONSTRAINT `fk_contract_kpis_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contract_kpis_kpis1` FOREIGN KEY (`kpi_id`) REFERENCES `kpis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `fk_groups_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `group_user`
--
ALTER TABLE `group_user`
  ADD CONSTRAINT `FK_GroupUser_Groups` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GroupUser_Influencers` FOREIGN KEY (`influencer_id`) REFERENCES `influencers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `influencers`
--
ALTER TABLE `influencers`
  ADD CONSTRAINT `fk_influencers_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_influenceurs_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `influencer_managers`
--
ALTER TABLE `influencer_managers`
  ADD CONSTRAINT `fk_influencer_managers_influenceurs1` FOREIGN KEY (`influenceur_id`) REFERENCES `influencers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_influencer_managers_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `kpis`
--
ALTER TABLE `kpis`
  ADD CONSTRAINT `fk_kpis_companies1` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `PK_Messages_Campaigns` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `message_user`
--
ALTER TABLE `message_user`
  ADD CONSTRAINT `FK_MessageUser_Messages` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_MessageUser_Users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sec_groups_apps`
--
ALTER TABLE `sec_groups_apps`
  ADD CONSTRAINT `sec_groups_apps_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `sec_groups` (`group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sec_groups_apps_ibfk_2` FOREIGN KEY (`app_name`) REFERENCES `sec_apps` (`app_name`) ON DELETE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
